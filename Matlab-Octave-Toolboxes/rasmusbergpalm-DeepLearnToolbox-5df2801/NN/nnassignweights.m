% ## Copyright (C) 2016 Adil
% ## 
% ## This program is free software; you can redistribute it and/or modify it
% ## under the terms of the GNU General Public License as published by
% ## the Free Software Foundation; either version 3 of the License, or
% ## (at your option) any later version.
% ## 
% ## This program is distributed in the hope that it will be useful,
% ## but WITHOUT ANY WARRANTY; without even the implied warranty of
% ## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% ## GNU General Public License for more details.
% ## 
% ## You should have received a copy of the GNU General Public License
% ## along with this program.  If not, see <http://www.gnu.org/licenses/>.
% 
% ## -*- texinfo -*- 
% ## @deftypefn {Function File} {@var{NET} =} nnassignweights (@var{NET}, @var{weights})
% ## This function basically assigns values to the weights of a neural network structure.
% ## Some code has been taken from nnsetup.
% ##
% ## Inputs:
% ##   NET: - A neural network structure created by the nnsetup function.
% ##   weights: - A vector of possible weights for the network
% ## Outputs:
% ##   NET: The Adapted network structure with new values for weights.
% ## @seealso{}
% ## @end deftypefn
% 
% ## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
% ## Created: 2016-06-16

function [nn] = nnassignweights (nn, weights)
tmp=0;
for i = 2 : nn.n   
   % weights and weight momentum
   [numrows numcols]=size(nn.W{i-1});
   %The following line should be self-explanatory. It basically reshapes a row vector of weights in a matrix
   nn.W{i - 1} = reshape(weights(tmp+1:tmp+(numrows*numcols)), numrows, numcols);
   tmp=tmp+(numrows*numcols);
end

end
