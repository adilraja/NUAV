% ## Copyright (C) 2016 Adil
% ## 
% ## This program is free software; you can redistribute it and/or modify it
% ## under the terms of the GNU General Public License as published by
% ## the Free Software Foundation; either version 3 of the License, or
% ## (at your option) any later version.
% ## 
% ## This program is distributed in the hope that it will be useful,
% ## but WITHOUT ANY WARRANTY; without even the implied warranty of
% ## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% ## GNU General Public License for more details.
% ## 
% ## You should have received a copy of the GNU General Public License
% ## along with this program.  If not, see <http://www.gnu.org/licenses/>.
% 
% ## -*- texinfo -*- 
% ## @deftypefn {Function File} {@var{numweights} =} nnnumweights (@var{nn})
% ## This function returns the number of weights and biases of a network strucure. It works for the network structure 
% ## provided with the deep learning neural network package.
% ##
% ## Inputs: NET :- Neural network structure provided by the nnsetup function.
% ## Outputs: numweights : - Total number of weights and biases for that network.
% ## @seealso{}
% ## @end deftypefn
% 
% ## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
% ## Created: 2016-06-16

function numweights = nnnumweights (nn)
  numweights=0;
  for i = 2 : nn.n   
        % Number of weights 
        [numrows numcols]=size(nn.W{i - 1});
        numweights=numweights+numrows*numcols;   
  end

end
