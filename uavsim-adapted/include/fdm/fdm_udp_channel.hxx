/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#ifndef _FDM_UDP_CHANNEL_HXX_
#define _FDM_UDP_CHANNEL_HXX_

#include "../common.hxx"

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#include "net_ctrls.hxx"
#include "net_fdm.hxx"

class FDMUDPChannel {

private:

	int sock; //socket handle
	struct sockaddr_in server, client;
	struct hostent *server2;//To get hostbyname. Adil

	int printErrorMessage(char const*);
public:

	//Constructor
	FDMUDPChannel(unsigned int, unsigned int, in_addr_t);
	FDMUDPChannel(unsigned int, unsigned int, in_addr_t, int);//Constructor added by Adil.

	//Destructor
	~FDMUDPChannel();

	int senddata(FGNetCtrls);//Initially the return type was void. I changed it to int to get some error codes. Adil.
	int sendFGNetFDMDataTCP(FGNetFDM);//Added by Adil
	int sendFGNetFDMDataUDP(FGNetFDM net_fdm);//Added by Adil

	FGNetFDM* getdata();
	
	FGNetCtrls* getFGNetCtrlsData();//Added by Adil
	
	int getSocketHandle();

	void closeSocket();
	void shutdownSocket();//Added by Adil to close TCP socket.
	
	//Double version
	inline static void htond(double &x) {	
		int * double_overlay;
		int holding_buffer;

		double_overlay = (int *) &x;
		holding_buffer = double_overlay[0];

		double_overlay[0] = htonl(double_overlay[1]);	
		double_overlay[1] = htonl(holding_buffer);

		return;
	}

	// Float version
	inline static void htonf(float &x) {
		if (sgIsLittleEndian() ) {
			int *Float_Overlay;
			int Holding_Buffer;

			Float_Overlay = (int *) &x;
			Holding_Buffer = Float_Overlay[0];

			Float_Overlay[0] = htonl(Holding_Buffer);
		} else {
			return;
		}
	}
	
	void htonFGNetCtrls(FGNetCtrls *net_ctrls);
	void htonFGNetFDM(FGNetFDM *net_fdm);

};

#endif

