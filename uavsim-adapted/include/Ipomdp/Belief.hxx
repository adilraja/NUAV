/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#ifndef BELIEF_HXX_
#define BELIEF_HXX_
#include"../common.hxx"
#include <vector>
#include <fstream>
class Belief{
private:
	vector<double> belief;
	void Tokenize(const string& str, vector<string>& tokens, const string& delimiters = " ")
	{
	  // Skip delimiters at beginning.
	  string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	  // Find first "non-delimiter".
	  string::size_type pos     = str.find_first_of(delimiters, lastPos);

	  while (string::npos != pos || string::npos != lastPos)
	  {
	      // Found a token, add it to the vector.
	      tokens.push_back(str.substr(lastPos, pos - lastPos));
	      // Skip delimiters.  Note the "not_of"
	      lastPos = str.find_first_not_of(delimiters, pos);
	      // Find next "non-delimiter"
	       pos = str.find_first_of(delimiters, lastPos);
	   }
	}
public:
	Belief();
	Belief(string);
	Belief(vector<double> NewBelief);

	vector<double> getBelief();
	void setBelief(vector<double> NewBelief);
	//vector<double> updateBelief(IpomdpObservation obs, IpomdpAction ac, bool isStatic);
	//vector<double> updateBeliefUsingPosition(int* pos);
	void printBelief();
};

#endif /* BELIEF_HXX_ */
