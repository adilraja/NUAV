/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#ifndef GRID_2D_HXX_
#define GRID_2D_HXX_


#include <string>
#include <math.h>
#include <vector>

#include"../unmannedC172.hxx"

class Grid2D{
	private: 
		int no_of_rows, no_of_cols;
		int current_row, current_col; //Start from 0.

		double init_lat,init_lon; //In radians. Coordinates from which grid starts
		double grid_size;
		vector<double> center_lats;
		vector<double> center_lons;
		double hover_leeway;
		double action_time;


	public:
		Grid2D(int NoRows, int NoCols, double InitLat, double InitLong, double gridSize, double hvr_leeway, double acttime);
		int* getCurrentGridLocation();

		void setCurrentGridLocation(int* curLoc){
			current_row=curLoc[0];
			current_col=curLoc[1];
			return;
		}

		int* getGridSize();

		double* getInitialCoordiantes(){
			double* init = new double[2];
			init[0] = init_lat;
			init[1] = init_lon;
			return init;
		}

		double* getCenterCoordinates(int row, int col);

		int* getGridLocation(double*); //Returns GridLocation of a given coordinate. Lat,Lon

		void hover(int, char **, UC172Control*, FDMUDPChannel*, FGNetCtrls*, FGNetFDM *,  MultiplayerUDPChannel*, char*);
		void moveNorth(int, char**, UC172Control*, FDMUDPChannel *, FGNetCtrls *, FGNetFDM *,  MultiplayerUDPChannel*, char*);
		void moveSouth(int, char**, UC172Control*, FDMUDPChannel *, FGNetCtrls *, FGNetFDM *,  MultiplayerUDPChannel*, char*);
		void moveEast(int, char**, UC172Control*, FDMUDPChannel *, FGNetCtrls *, FGNetFDM *,  MultiplayerUDPChannel*, char*);
		void moveWest(int, char**, UC172Control*, FDMUDPChannel *, FGNetCtrls *, FGNetFDM *,  MultiplayerUDPChannel*, char*);
};


#endif /*GRID_2D_HXX_*/
