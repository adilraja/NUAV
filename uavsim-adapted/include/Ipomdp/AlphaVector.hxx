/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#ifndef ALPHAVECTORS_HXX_
#define ALPHAVECTORS_HXX_
#include "../common.hxx"

#include <fstream>
#include <vector>
#include <limits>


typedef struct alphavect{
	int act;
	vector<double> intercepts;

} alphavectors;

class AlphaVectors{
	private:
		vector<alphavectors> alphvects;
		int nodecount;

		void Tokenize(const string& str, vector<string>& tokens, const string& delimiters = " ")
		{
		    // Skip delimiters at beginning.
		    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
		    // Find first "non-delimiter".
		    string::size_type pos     = str.find_first_of(delimiters, lastPos);

		    while (string::npos != pos || string::npos != lastPos)
	 	   {
		        // Found a token, add it to the vector.
		        tokens.push_back(str.substr(lastPos, pos - lastPos));
	  	      // Skip delimiters.  Note the "not_of"
	  	      lastPos = str.find_first_not_of(delimiters, pos);
	  	      // Find next "non-delimiter"
	 	       pos = str.find_first_of(delimiters, lastPos);
	 	   }
		}

	public:
		AlphaVectors();
		AlphaVectors(string);
		~AlphaVectors();

		vector<int> getOptimalAction (vector<double> pbelief);
		vector<int> getOptimalPolicy (vector<double> belief);
		void printAlphaVectors();
};

#endif /* ALPHAVECTORS_HXX_ */

