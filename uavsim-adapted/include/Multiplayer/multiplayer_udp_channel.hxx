/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#ifndef _MULTIPLAYER_UDP_CHANNEL_HXX_
#define _MULTIPLAYER_UDP_CHANNEL_HXX_

#include "../common.hxx"

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <plib/netSocket.h>
#include <map>
#include<fstream>

#include "mpmessages.hxx"
#include "tiny_xdr.hxx"

#include "../fdm/net_fdm.hxx"

class MultiplayerUDPChannel {

private:

	int sock; //socket handle
	struct sockaddr_in server, client;

        void SendMyPosition(const FGExternalMotionData& motionInfo, char*);	
	void printErrorMessage(char *msg);
	void processPositionMessage(const char *Msg);
	void processPositionMessage(const char *Msg,bool);	//Added to enable writing position to file only occasionally.
	void fillMsgHdr(T_MsgHdr *MsgHdr, int iMsgId, unsigned _len = 0u);
	void fillMsgHdr(char*, T_MsgHdr *MsgHdr, int iMsgId, unsigned _len = 0u);
	
	netSocket *mSocket, *multiplayerSocket;
	netAddress mServer,multiplayerServer;
	
	char* currentGridPosition; 

	typedef std::map<unsigned, SGSharedPtr<SGPropertyNode> > PropertyMap;
        PropertyMap mPropertyMap;

	ofstream finalLocationFile;
	
public:

	//Constructor
	MultiplayerUDPChannel(unsigned int);
	MultiplayerUDPChannel(unsigned int, char*); //s_port, out ip address

	//Destructor
	~MultiplayerUDPChannel();

	void fowardData(char* callsignToWatch);	//Misnomer. Don't go with literal meaning. This Function Receives other agent's data
	
        void processFDM(FGNetFDM, char*);	//This function sends data to other agents
	void SendTextMessage(const string &MsgText);

	void closeSocket();
	
	//Double version
	inline static void htond(double &x) {

		int * double_overlay;
		int holding_buffer;

		double_overlay = (int *) &x;
		holding_buffer = double_overlay[0];

		double_overlay[0] = htonl(double_overlay[1]);
		double_overlay[1] = htonl(holding_buffer);

		return;
	}

	// Float version
	inline static void htonf(float &x) {
		if (sgIsLittleEndian() ) {
			int *Float_Overlay;
			int Holding_Buffer;

			Float_Overlay = (int *) &x;
			Holding_Buffer = Float_Overlay[0];

			Float_Overlay[0] = htonl(Holding_Buffer);
		} else {
			return;
		}
	}

};

#endif

