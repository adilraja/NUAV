/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#ifndef _AIRVEHICLE_HXX_
#define _AIRVEHICLE_HXX_

#include "common.hxx"

#include "fdm/net_ctrls.hxx"
#include "fdm/net_fdm.hxx"

class AVControl {

protected:

  //FGNetCtrls *ctrls;

public:
  
  AVControl();
  ~AVControl();

  void initGeneralParams(FGNetCtrls*);

  virtual void takeoff(FGNetFDM, FGNetCtrls*) = 0; 
  virtual void fly_straight(FGNetFDM, FGNetCtrls*) = 0;  
  virtual void turn_left(float)=0;
  virtual void turn_right(float)=0;
  virtual void descend_by(float)=0;
  virtual void ascend_by(float)=0;
  
  FGNetCtrls* getCtrlParams();

};


#endif
