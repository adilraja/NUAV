## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} set_fgfs_properties (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-19

function set_fgfs_properties (ipAddress="127.0.0.1", portNum=22224, altitude=2500, lattitude_deg=32, longitude_deg=55)

  client = socket(AF_INET, SOCK_STREAM, 0);
  server_info = struct("addr", ipAddress, "port", portNum);
  rc = connect(client, server_info);
  send(rc, strcat("set /position/altitude-ft ", num2str(altitude), "\r\n");
  disp(recv(client,1000));
  send(rc, strcat("set /position/latitude-deg ", num2str(latitude_deg), "\r\n");
  disp(recv(client,1000));
  send(rc, strcat("set /position/longitude-deg ", num2str(longitude_deg), "\r\n");
  disp(recv(client,1000));
  disconnect(rc);
endfunction
