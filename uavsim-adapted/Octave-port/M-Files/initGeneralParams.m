## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} initGeneralParams (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-01

function netCtrls = initGeneralParams (netCtrls)

netCtrls.version= 27;

%Aero Controls
	netCtrls.aileron= 0;
	netCtrls.elevator= 0;
	netCtrls.rudder=0;
	netCtrls.aileron_trim= 0;
	netCtrls.elevator_trim=0;
	netCtrls.rudder_trim= 0;
  
  % Aero control faults
    netCtrls.flaps_power=1;% true = power available
   	netCtrls.flap_motor_ok=1;
    
% Landing Gear
    netCtrls.gear_handle= 0; % true=gear handle down; false= gear handle up

% Switches
    netCtrls.master_avionics=1;
% value tank to tank specified by
                                             % int value
   netCtrls.cross_feed=1;% false = off, true = on

   %nav and Comm
   	netCtrls.comm_1=0;
    	netCtrls.comm_2=0;
    	netCtrls.nav_1=0;
    	netCtrls.nav_2= 0;

    	%wind and turbulance
    	netCtrls.wind_speed_kt=0;
    	netCtrls.wind_dir_deg=0;
    	netCtrls.turbulence_norm=0;

    	% temp and pressure
    	netCtrls.temp_c=25;
      netCtrls.press_inhg=29.92;%FGFS simply crashes without this value. Adil

    	%other information about environment
    	netCtrls.hground= 0;% ground elevation (meters)
    	netCtrls.magvar= 0; % local magnetic variation in degs.

    	%hazards
    	netCtrls.icing=0;% icing status could me much
                                         % more complex but I'm
                                         % starting simple here.

    		%simulation control
    	netCtrls.speedup=1;  % integer speedup multiplier
    	netCtrls.freeze= 0;		% 0=normal
				         % 0x01=master
				         % 0x02=position
				         % 0x04=fuel
endfunction
