## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} simpleTakeoff (@var{input1}, @var{input2})
## The simpleTakeoff algorithm of GaTAC
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-23

function [retval] = simpleTakeoff (fg_ip_address="127.0.0.1", FGIncomingPort=23222, FGOutgoingPort=23223)
  takeoffStatus=false;
  netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort,fg_ip_address);
  netCtrls=initGeneralParams(netCtrls);
  netCtrls=initC172Params(netCtrls);
  while(takeoffStatus==false)
    net_fdm=getMyFlightData(FGIncomingPort, FGOutgoingPort,fg_ip_address);
    if(net_fdm.vcas<75)
      for(i=1:netCtrls.num_engines)
        netCtrls.throttle(i)=netCtrls.throttle(i)+0.03;
      endfor
    elseif(net_fdm.vcas>80)
      for(i=1:netCtrls.num_engines)
        netCtrls.throttle(i)=netCtrls.throttle(i)-0.03;
      endfor
    endif
          disp("The throttle is: "), disp(netCtrls.throttle(1));

    if(net_fdm.agl<1.75)
      if(net_fdm.vcas<50)
  %      netCtrls.rudder=(5.2-net_fdm.psi)*8;
      else
   %       netCtrls.rudder=(5.2-net_fdm.psi)*4;
      endif
    else
     % netCtrls.rudder=(5.2-net_fdm.psi)*20;
    endif
    if(net_fdm.agl>1.75)
      netCtrls.ailerons=rand(1,1)/5;
    endif
    if(net_fdm.agl>20)
      netCtrls.elevator=(net_fdm.climb_rate-12)/100;
    endif
    if(net_fdm.agl>=30)
      takeoffStatus=true;
    endif
    netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address, netCtrls,0);
    pause(0.015);
  endwhile
endfunction
