## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} initC172Params (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-01

function netCtrls = initC172Params (netCtrls)
netCtrls.version= 27;
netCtrls.num_engines=2;
for(i=1:netCtrls.num_engines)
  netCtrls.master_bat(i)=1;
  netCtrls.master_alt(i)=1;
  netCtrls.magnetos(i)=1;
  netCtrls.starter_power(i)=1;
  netCtrls.throttle(i)=1;
  netCtrls.mixture(i)=1;
  netCtrls.condition(i)=1;
  netCtrls.fuel_pump_power(i)=1;
  netCtrls.prop_advance(i)=1;
  netCtrls.feed_tank_to(i)=1;
  netCtrls.reverse(i)=1;
  
  netCtrls.engine_ok(i)=1;
  netCtrls.mag_right_ok(i)=1;
  netCtrls.mag_left_ok(i)=1;
  netCtrls.spark_plugs_ok(i)=1;
  netCtrls.oil_press_status(i)=1;
  netCtrls.fuel_pump_ok(i)=1;
endfor

netCtrls.num_tanks=1;
for(i=1:netCtrls.num_tanks)
  netCtrls.fuel_selector(i)=1;
  netCtrls.xfer_pump(i)=1;
endfor

netCtrls.brake_left=0;
netCtrls.brake_right=0;
netCtrls.copilot_brake_left=0;
netCtrls.copilot_brake_right=0;
netCtrls.brake_parking=0;

endfunction
