## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} runSendMyFlightDataInALoop (@var{input1}, @var{input2})
##This runs sendMyFlightData.oct in a loop for testing it
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-23

function [retval] = runSendMyFlightDataInALoop (input1, input2)

netCtrls=sendMyFlightData(22222,22223,"192.168.0.28");
netCtrls=initGeneralParams(netCtrls);
netCtrls=initC172Params(netCtrls);
netCtrls.throttle(1)=20;
%netCtrls.aileron=0.3;
netCtrls.elevator=0.0;
%netCtrls.rudder=0.3;
max_throttle=500;
min_elevator=-0.7;
max_elevator=0.5;

for(i=1:10000)
  netCtrls=sendMyFlightData(22222,22223,"192.168.0.28",netCtrls,0);
  netCtrls.throttle(1)+=20;
 % netCtrls.aileron*=-1;
 if(i>500)
  netCtrls.elevator-=0.001;
 endif
  %netCtrls.rudder*=-1;
  if(netCtrls.throttle(1)>max_throttle)
    netCtrls.throttle(1)=max_throttle;
  endif
  if(netCtrls.elevator>max_elevator)
    netCtrls.elevator-=0.001;
  endif
  
  if(netCtrls.elevator<min_elevator)
    netCtrls.elevator+=0.001;
  endif
    
    
  pause(0.15);
endfor
endfunction
