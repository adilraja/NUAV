## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} swapBytesForSendMyFlightData (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-22

function [retval] = swapBytesForSendMyFlightData (netCtrls)
    
    netCtrls.version = swapbytes(uint32(netCtrls.version));
    
    netCtrls.aileron = swapbytes(double(netCtrls.aileron));
    netCtrls.elevator = swapbytes(double(netCtrls.elevator));
    netCtrls.rudder = swapbytes(double(netCtrls.rudder));
    netCtrls.aileron_trim = swapbytes(double(netCtrls.aileron_trim));
    netCtrls.elevator_trim = swapbytes(double(netCtrls.elevator_trim));
    netCtrls.rudder_trim = swapbytes(double(netCtrls.rudder_trim));
    netCtrls.flaps = swapbytes(double(netCtrls.flaps));
    netCtrls.spoilers = swapbytes(double(netCtrls.spoilers));
    netCtrls.speedbrake = swapbytes(double(netCtrls.speedbrake));
    
    netCtrls.flaps_power = swapbytes(uint32(netCtrls.flaps_power));
    netCtrls.flap_motor_ok = swapbytes(uint32(netCtrls.flap_motor_ok));
    
    netCtrls.num_engines =  swapbytes(uint32(netCtrls.num_engines));
    netCtrls.master_bat =swapbytes(uint32(netCtrls.master_bat));
    netCtrls.master_alt = swapbytes(uint32(netCtrls.master_alt));
    netCtrls.magnetos = swapbytes(uint32(netCtrls.magnetos));
    netCtrls.starter_power = swapbytes(uint32(netCtrls.starter_power));
    netCtrls.throttle = swapbytes(double(netCtrls.throttle));
    netCtrls.mixture = swapbytes(double(netCtrls.mixture));
    netCtrls.condition = swapbytes(double(netCtrls.condition));
    netCtrls.fuel_pump_power = swapbytes(uint32(netCtrls.fuel_pump_power));
    netCtrls.prop_advance = swapbytes(double(netCtrls.prop_advance));
    netCtrls.feed_tank_to = swapbytes(uint32(netCtrls.feed_tank_to));
    netCtrls.reverse = swapbytes(uint32(netCtrls.reverse));
    
    netCtrls.engine_ok = swapbytes(uint32(netCtrls.engine_ok));
    netCtrls.mag_left_ok = swapbytes(uint32(netCtrls.mag_left_ok));
    netCtrls.mag_right_ok = swapbytes(uint32(netCtrls.mag_right_ok));
    netCtrls.spark_plugs_ok = swapbytes(uint32(netCtrls.spark_plugs_ok));
    netCtrls.oil_press_status = swapbytes(uint32(netCtrls.oil_press_status));
    netCtrls.fuel_pump_ok = swapbytes(uint32(netCtrls.fuel_pump_ok));
    
    netCtrls.num_tanks =  swapbytes(uint32(netCtrls.num_tanks));
    netCtrls.fuel_selector = swapbytes(uint32(netCtrls.fuel_selector));
    netCtrls.xfer_pump = swapbytes(uint32(netCtrls.xfer_pump));
    
    netCtrls.cross_feed = swapbytes(uint32(netCtrls.cross_feed));
    
    netCtrls.brake_left = swapbytes(double(netCtrls.brake_left));
    netCtrls.brake_right = swapbytes(double(netCtrls.brake_right));
    netCtrls.copilot_brake_left = swapbytes(double(netCtrls.copilot_brake_left));
    netCtrls.copilot_brake_right = swapbytes(double(netCtrls.copilot_brake_right));
    netCtrls.brake_parking = swapbytes(double(netCtrls.brake_parking));
    
    netCtrls.gear_handle = swapbytes(uint32(netCtrls.gear_handle));
    
    netCtrls.master_avionics = swapbytes(uint32(netCtrls.master_avionics));
    
    netCtrls.comm_1 = swapbytes(double(netCtrls.comm_1));
    netCtrls.comm_2 = swapbytes(double(netCtrls.comm_2));
    netCtrls.nav_1 = swapbytes(double(netCtrls.nav_1));
    netCtrls.nav_2 = swapbytes(double(netCtrls.nav_2));
    
    netCtrls.wind_speed_kt = swapbytes(double(netCtrls.wind_speed_kt));
    netCtrls.wind_dir_deg = swapbytes(double(netCtrls.wind_dir_deg));
    netCtrls.turbulence_norm = swapbytes(double(netCtrls.turbulence_norm));
    
    netCtrls.temp_c = swapbytes(double(netCtrls.temp_c));
    netCtrls.press_inhg = swapbytes(double(netCtrls.press_inhg));
    
    netCtrls.hground = swapbytes(double(netCtrls.hground));
    netCtrls.magvar = swapbytes(double(netCtrls.magvar));
    
    netCtrls.icing = swapbytes(uint32(netCtrls.icing));
    
    netCtrls.speedup = swapbytes(uint32(netCtrls.speedup));
    
    netCtrls.freeze = swapbytes(uint32(netCtrls.freeze));
    
   % netCtrls.reserved= swapbytes(uint32(netCtrls.reserved));
    retval=netCtrls;
endfunction
