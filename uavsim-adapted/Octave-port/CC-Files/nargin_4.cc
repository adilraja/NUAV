//netCtrls = args(3).scalar_map_value();//Args(3) was already 				supposed to be a struct. So assign it to another struct.
		//Aero Controls

		/*ctrls->aileron = netCtrls.contents("aileron").double_value();
		ctrls->elevator = netCtrls.contents("elevator").double_value();
		ctrls->rudder = netCtrls.contents("rudder").double_value();
		ctrls->aileron_trim = netCtrls.contents("aileron_trim").double_value();
		ctrls->elevator_trim = netCtrls.contents("elevator_trim").double_value();
		ctrls->rudder_trim = netCtrls.contents("rudder_trim").double_value();

		// Aero control faults
    		ctrls->flaps_power=netCtrls.contents("flaps_power").int_value();                 // true = power available
   		ctrls->flap_motor_ok=netCtrls.contents("flap_motor_ok").int_value();

    		// Engine controls
   ctrls->num_engines=netCtrls.contents("num_engines").int_value();// number of valid engines*/

/*dim_vector vd(1, ctrls->num_engines);
uint32NDArray tmp_master_bat(vd), tmp_master_alt(vd), tmp_magnetos(vd), tmp_starter_power(vd), tmp_fuel_pump_power(vd);
NDArray tmp_throttle(vd), tmp_mixture(vd), tmp_condition(vd),  tmp_prop_advance(vd);

tmp_master_bat=netCtrls.contents("master_bat").array_value();
tmp_master_alt=netCtrls.contents("master_alt").array_value();
tmp_magnetos=netCtrls.contents("magnetos").array_value();
tmp_starter_power=netCtrls.contents("starter_power").array_value();

tmp_throttle=netCtrls.contents("throttle").array_value();
tmp_mixture=netCtrls.contents("mixture").array_value();
tmp_condition=netCtrls.contents("condition").array_value();
tmp_fuel_pump_power=netCtrls.contents("fuel_pump_power").array_value();
tmp_prop_advance=netCtrls.contents("prop_advance").array_value();

for(int i=0; i<ctrls->num_engines;++i){
   ctrls->master_bat[i]=tmp_master_bat(i);
   ctrls->master_alt[i]=tmp_master_alt(i);
   ctrls->magnetos[i]=tmp_magnetos(i);
   ctrls->starter_power[i]=tmp_starter_power(i);// true = starter power
   ctrls->throttle[i]=tmp_throttle(i);     //  0 ... 1
   ctrls->mixture[i]=tmp_mixture(i);      //  0 ... 1
   ctrls->condition[i]=tmp_condition(i);    //  0 ... 1
   ctrls->fuel_pump_power[i]=tmp_fuel_pump_power(i);// true = on
   ctrls->prop_advance[i]=tmp_prop_advance(i); //  0 ... 1
}//end for loop

dim_vector vd1(1,4);//1 rows, 4 columns
uint32NDArray tmp_feed_tank_to(vd1), tmp_reverse(vd1);

tmp_feed_tank_to=netCtrls.contents("feed_tank_to").array_value();
tmp_reverse=netCtrls.contents("reverse").array_value();

for(int i=0;i<4;++i){
   ctrls->feed_tank_to[i]=tmp_feed_tank_to(i);
   ctrls->reverse[i]=tmp_reverse(i);
}//end for loop

dim_vector vd2(1, ctrls->num_engines);
uint32NDArray tmp_engine_ok, tmp_mag_left_ok, tmp_mag_right_ok, tmp_spark_plugs_ok, tmp_oil_press_status, tmp_fuel_pump_ok;

tmp_engine_ok=netCtrls.contents("engine_ok").array_value();
tmp_mag_left_ok=netCtrls.contents("mag_left_ok").array_value();
tmp_mag_right_ok=netCtrls.contents("mag_right_ok").array_value();
tmp_spark_plugs_ok=netCtrls.contents("spark_plug_ok").array_value();
tmp_oil_press_status=netCtrls.contents("oil_press_status").array_value();
tmp_fuel_pump_ok==netCtrls.contents("fuel_pump_ok").array_value();

for(int i=0;i<ctrls->num_engines;++i){
    	// Engine faults
   ctrls->engine_ok[i]=tmp_engine_ok(i);
   ctrls->mag_left_ok[i]=tmp_mag_left_ok(i);
   ctrls->mag_right_ok[i]=tmp_mag_right_ok(i);
   ctrls->spark_plugs_ok[i]=tmp_spark_plugs_ok(i);  // false = fouled plugs
   ctrls->oil_press_status[i]=tmp_oil_press_status(i);// 0 = normal, 1 = low, 2 = full fail
   ctrls->fuel_pump_ok[i]=tmp_fuel_pump_ok(i);
}//end for loop
	// Fuel management
    	ctrls->num_tanks=netCtrls.contents("num_tanks").int_value();                      // number of valid tanks

dim_vector vd3(1, ctrls->num_engines);
uint32NDArray tmp_fuel_selector(vd3);
tmp_fuel_selector=netCtrls.contents("fuel_selector").array_value();
for(int i=0;i<ctrls->num_engines;++i){
   ctrls->fuel_selector[i]=tmp_fuel_selector(i);// false = off, true = on
}//end for loop

dim_vector vd4(1, 5);//1 rows, 5 columns
uint32NDArray tmp_xfer_pump(vd4);
tmp_xfer_pump=netCtrls.contents("xfer_pump").array_value();
for(int i=0;i<5;++i){
	ctrls->xfer_pump[i]=tmp_xfer_pump(i);// specifies transfer from array
}//end for loop
                                             // value tank to tank specified by
                                             // int value
    	ctrls->cross_feed=netCtrls.contents("cross_feed").int_value();// false = off, true = on

   	// Brake controls
   	ctrls->brake_left=netCtrls.contents("brake_left").double_value();
    	ctrls->brake_right=netCtrls.contents("brake_right").double_value();
    	ctrls->copilot_brake_left=netCtrls.contents("copilot_brake").double_value();
    	ctrls->copilot_brake_right=netCtrls.contents("copilot_brake").double_value();
    	ctrls->brake_parking=netCtrls.contents("brake_parking").double_value();
    
    	// Landing Gear
    	ctrls->gear_handle=netCtrls.contents("brake_left").int_value(); // true=gear handle down; false= gear handle up

    	// Switches
    	ctrls->master_avionics=netCtrls.contents("master_avionics").int_value();
    
        // nav and Comm
   	ctrls->comm_1=netCtrls.contents("comm_1").double_value();
    	ctrls->comm_2=netCtrls.contents("comm_2").double_value();
    	ctrls->nav_1=netCtrls.contents("nav_1").double_value();
    	ctrls->nav_2=netCtrls.contents("nav_2").double_value();

    	// wind and turbulance
    	ctrls->wind_speed_kt=netCtrls.contents("wind_speed_kt").double_value();
    	ctrls->wind_dir_deg=netCtrls.contents("wind_dir_deg").double_value();
    	ctrls->turbulence_norm=netCtrls.contents("turbulence_norm").double_value();

    	// temp and pressure
    	ctrls->temp_c=netCtrls.contents("temp_c").double_value();
    	ctrls->press_inhg=netCtrls.contents("press_inhg").double_value();

    	// other information about environment
    	ctrls->hground=netCtrls.contents("hground").double_value();// ground elevation (meters)
    	ctrls->magvar=netCtrls.contents("magvar").double_value(); // local magnetic variation in degs.

    	// hazards
    	ctrls->icing=netCtrls.contents("icing").int_value();// icing status could me much
                                         // more complex but I'm
                                         // starting simple here.

    		// simulation control
    	ctrls->speedup=netCtrls.contents("speedup").int_value();        // integer speedup multiplier
    	ctrls->freeze=netCtrls.contents("freeze").int_value();		// 0=normal
				         // 0x01=master
				         // 0x02=position
				         // 0x04=fuel

    		// --- New since FlightGear 0.9.10 (FG_NET_CTRLS_VERSION = 27)

    		// --- Add new variables just before this line.

    		//uint32_t reserved[RESERVED_SPACE];	 // 100 bytes reserved for future use.*/
