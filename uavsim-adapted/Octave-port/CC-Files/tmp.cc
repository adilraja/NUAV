#include <octave/oct.h>
#include <octave/ov-struct.h>

#include "../../include/fdm/fdm_udp_channel.hxx"
#include "../../include/fdm/net_ctrls.hxx"

DEFUN_DLD (tmp, args, nargout,
           "This is an implementation of the getMyFlightData function of Main.cxx of GaTAC. The original function does not have any input arguments. However, this implementation shall take in the socket port number and IP address.")
{
  octave_value_list retval;	//create object to store return values

  //octave_value retval;
  int nargin = args.length ();

  octave_stdout << "getData has "
                << nargin << " input arguments and "
                << nargout << " output arguments.\n";

	FGNetFDM *fdm;

//	char *FDMIncomingPort = NULL;				//Receive FDM data from flightgear
//	char *CtrlOutgoingPort = NULL;				//Send control data to flightgear
//	char *IPAddress=NULL;

//Lets use octave variables instead of above three C variables.

	if(nargin!=4 && !args(0).is_real_scalar() && !args(1).is_real_scalar() && !args(2).is_string()){//Right now there should be exactly four input arguments: Source port number, destination port number, destination IPAddress, and a struct that will hold the FDM data.
		printf("Right now there should be exactly three input arguments: Source port number, destination port number, destination IPAddress, and a struct that will hold the FDM data.\n");
	}
	octave_scalar_map st;
   	st.assign("version",1);    // increment when data values change
    	st.assign("padding",2);    // padding

    	
  	st.assign("selected", 12);
  	retval(0) = octave_value(st);

return retval;
}
