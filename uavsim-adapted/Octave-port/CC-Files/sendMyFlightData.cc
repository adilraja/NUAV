#include <octave/oct.h>
#include <octave/parse.h>
#include <octave/ov-struct.h>

#include "../../include/fdm/fdm_udp_channel.hxx"
#include "../../include/fdm/net_ctrls.hxx"

DEFUN_DLD (sendMyFlightData, args, nargout,
           "This is an implementation of the sendMyFlightData function of Main.cxx of GaTAC. This takes 3 or 5 input arguments. When it takes 3 arbuments it return a netCtrls struct element that holds initial values for control variables. When it takes 5 arguments, its fourth argument is a netCtrls struct passed to it that copies data off to an FGNetCtrls object and sends that over to a FlightGear instance. The fifth argument is supposed to be a boolean flag teer or not the values sent to it were byte swapped before being sent to it. The first 3 arguments are FGIncoming port, CtrlsOutgoingPort and the IPAddress of the FlightGear instance.")
{
	int nargin = args.length ();
	octave_value retval;
 	
	if(nargin<3){
		octave_stdout << "Not enough input arguments.\n";
		return octave_value(-1);
	}

	if(nargin>5 || !args(0).is_real_scalar() || !args(1).is_real_scalar() || !args(2).is_string()){
//	Right now there should be exactly four input arguments: Source port number, destination port number, destination IPAddress, and a struct that will hold the Net-ctrls data.
	octave_stdout << "Right now there should be exactly five input arguments: Source port number, destination port number, destination IPAddress, a struct that will hold the Net-Ctrls data and a byte swapping flag.\n";
	return octave_value(-1);
	}

	
	octave_scalar_map netCtrls;
	

	//octave_function *flyFast;//We do not need something like this.
	//octave_value_list output = feval (flyFast);//Also this.

	if(nargin==5 && args(0).is_real_scalar() && args(1).is_real_scalar() && args(2).is_string() && args(4).is_real_scalar())//We already supplied a struct containing control values.
	{
		FGNetCtrls *ctrls = new FGNetCtrls();

	int FDMIncomingPort = floor(args(0).int_value());	//Convert to integer, just in case.
	int CtrlOutgoingPort = floor(args(1).int_value());	//Convert to integer, just in case.
	std::string IPAddress = args(2).string_value();
	const char * IPAddress2 = IPAddress.c_str();		//cast to char *
	int loop_max=4;//No byte swapping
	int byte_swapping_flag=args(4).int_value();
	
	
	FDMUDPChannel net(FDMIncomingPort, CtrlOutgoingPort, inet_addr(IPAddress2));
	if(net.getSocketHandle()==-1){ //Meaning that there is an error.
		return octave_value(-1);
	}

		//code for this written in nargin_4.cc
		octave_stdout << "We are working with the user supplied ctrls and sending them away to the remote fgfs host after copying them to the FGNetCtrls object.\n";

		netCtrls = args(3).scalar_map_value();//Args(3) was already 				supposed to be a struct. So assign it to another struct.

		
		//Assign the version
		ctrls->version = netCtrls.contents("version").int_value();
		
		//Aero Controls

		ctrls->aileron = netCtrls.contents("aileron").double_value();
		ctrls->elevator = netCtrls.contents("elevator").double_value();
		ctrls->rudder = netCtrls.contents("rudder").double_value();
		ctrls->aileron_trim = netCtrls.contents("aileron_trim").double_value();
		ctrls->elevator_trim = netCtrls.contents("elevator_trim").double_value();
		ctrls->rudder_trim = netCtrls.contents("rudder_trim").double_value();
		ctrls->flaps = netCtrls.contents("flaps").double_value();
		ctrls->spoilers = netCtrls.contents("spoilers").double_value();
		ctrls->speedbrake = netCtrls.contents("speedbrake").double_value();

		// Aero control faults
    		ctrls->flaps_power=netCtrls.contents("flaps_power").int_value();                 // true = power available
   		ctrls->flap_motor_ok=netCtrls.contents("flap_motor_ok").int_value();

    		// Engine controls
   ctrls->num_engines=netCtrls.contents("num_engines").int_value();// number of valid engines

if(byte_swapping_flag==1){//Byte swapping has been done
	loop_max=ctrls->num_engines>>24;//Right shift by 24 bits to compensate for byte swapping.
}
else{//No byte swapping has been done
	loop_max=ctrls->num_engines;
}

octave_stdout << "Loop max is: " << loop_max<<"\n";

dim_vector vd(1, loop_max);
uint32NDArray tmp_master_bat(vd), tmp_master_alt(vd), tmp_magnetos(vd), tmp_starter_power(vd), tmp_fuel_pump_power(vd);
NDArray tmp_throttle(vd), tmp_mixture(vd), tmp_condition(vd),  tmp_prop_advance(vd);

tmp_master_bat=netCtrls.contents("master_bat").array_value();
tmp_master_alt=netCtrls.contents("master_alt").array_value();
tmp_magnetos=netCtrls.contents("magnetos").array_value();
tmp_starter_power=netCtrls.contents("starter_power").array_value();

tmp_throttle=netCtrls.contents("throttle").array_value();
tmp_mixture=netCtrls.contents("mixture").array_value();
tmp_condition=netCtrls.contents("condition").array_value();
tmp_fuel_pump_power=netCtrls.contents("fuel_pump_power").array_value();
tmp_prop_advance=netCtrls.contents("prop_advance").array_value();





for(int i=0; i<loop_max;i++){
   ctrls->master_bat[i]=tmp_master_bat(i+1);
   ctrls->master_alt[i]=tmp_master_alt(i+1);
   ctrls->magnetos[i]=tmp_magnetos(i+1);
   ctrls->starter_power[i]=tmp_starter_power(i+1);// true = starter power
   ctrls->throttle[i]=tmp_throttle(i+1);     //  0 ... 1
   ctrls->mixture[i]=tmp_mixture(i+1);      //  0 ... 1
   ctrls->condition[i]=tmp_condition(i+1);    //  0 ... 1
   ctrls->fuel_pump_power[i]=tmp_fuel_pump_power(i+1);// true = on
   ctrls->prop_advance[i]=tmp_prop_advance(i+1); //  0 ... 1
}//end for loop

dim_vector vd1(1,loop_max);//1 rows, 4 columns
uint32NDArray tmp_feed_tank_to(vd1), tmp_reverse(vd1);

tmp_feed_tank_to=netCtrls.contents("feed_tank_to").array_value();
tmp_reverse=netCtrls.contents("reverse").array_value();

for(int i=0;i<loop_max;i++){
   ctrls->feed_tank_to[i]=tmp_feed_tank_to(i+1);
   ctrls->reverse[i]=tmp_reverse(i);
}//end for loop

dim_vector vd2(1, loop_max);//num_engines
uint32NDArray tmp_engine_ok(vd2), tmp_mag_left_ok(vd2), tmp_mag_right_ok(vd2), tmp_spark_plugs_ok(vd2), tmp_oil_press_status(vd2), tmp_fuel_pump_ok(vd2);

tmp_engine_ok=netCtrls.contents("engine_ok").array_value();
tmp_mag_left_ok=netCtrls.contents("mag_left_ok").array_value();
tmp_mag_right_ok=netCtrls.contents("mag_right_ok").array_value();
tmp_spark_plugs_ok=netCtrls.contents("spark_plugs_ok").array_value();
tmp_oil_press_status=netCtrls.contents("oil_press_status").array_value();
tmp_fuel_pump_ok=netCtrls.contents("fuel_pump_ok").array_value();

//int loop_max=ctrls->num_engines>>24;//Right shift by 24 bits to compensate for byte swapping.
for(int i=0;i<loop_max;i++){//iterate over netCtrls.num_engines
    	// Engine faults
   ctrls->engine_ok[i]=tmp_engine_ok(i+1);
   ctrls->mag_left_ok[i]=tmp_mag_left_ok(i+1);
   ctrls->mag_right_ok[i]=tmp_mag_right_ok(i+1);
   ctrls->spark_plugs_ok[i]=tmp_spark_plugs_ok(i+1);  // false = fouled plugs
   ctrls->oil_press_status[i]=tmp_oil_press_status(i+1);// 0 = normal, 1 = low, 2 = full fail
   ctrls->fuel_pump_ok[i]=tmp_fuel_pump_ok(i+1);
}//end for loop
	// Fuel management
    	ctrls->num_tanks=netCtrls.contents("num_tanks").int_value();                      // number of valid tanks

dim_vector vd3(1, loop_max);
uint32NDArray tmp_fuel_selector(vd3);
tmp_fuel_selector=netCtrls.contents("fuel_selector").array_value();

//int loop_max=ctrls->num_engines>>24;//Right shift by 24 bits to compensate for byte swapping.
for(int i=0;i<loop_max;i++){
   ctrls->fuel_selector[i]=tmp_fuel_selector(i+1);// false = off, true = on
}//end for loop

dim_vector vd4(1, 5);//1 rows, 5 columns
uint32NDArray tmp_xfer_pump(vd4);
tmp_xfer_pump=netCtrls.contents("xfer_pump").array_value();

//int loop_max=ctrls->num_engines>>24;//Right shift by 24 bits to compensate for byte swapping.

for(int i=0;i<loop_max;i++){
	ctrls->xfer_pump[i]=tmp_xfer_pump(i+1);// specifies transfer from array
}//end for loop
                                             // value tank to tank specified by
                                             // int value
    	ctrls->cross_feed=netCtrls.contents("cross_feed").int_value();// false = off, true = on

   	// Brake controls
   	ctrls->brake_left=netCtrls.contents("brake_left").double_value();
    	ctrls->brake_right=netCtrls.contents("brake_right").double_value();
    	ctrls->copilot_brake_left=netCtrls.contents("copilot_brake_left").double_value();
    	ctrls->copilot_brake_right=netCtrls.contents("copilot_brake_right").double_value();
    	ctrls->brake_parking=netCtrls.contents("brake_parking").double_value();
    
    	// Landing Gear
    	ctrls->gear_handle=netCtrls.contents("gear_handle").int_value(); // true=gear handle down; false= gear handle up

    	// Switches
    	ctrls->master_avionics=netCtrls.contents("master_avionics").int_value();
    
        // nav and Comm
   	ctrls->comm_1=netCtrls.contents("comm_1").double_value();
    	ctrls->comm_2=netCtrls.contents("comm_2").double_value();
    	ctrls->nav_1=netCtrls.contents("nav_1").double_value();
    	ctrls->nav_2=netCtrls.contents("nav_2").double_value();

    	// wind and turbulance
    	ctrls->wind_speed_kt=netCtrls.contents("wind_speed_kt").double_value();
    	ctrls->wind_dir_deg=netCtrls.contents("wind_dir_deg").double_value();
    	ctrls->turbulence_norm=netCtrls.contents("turbulence_norm").double_value();

    	// temp and pressure
    	ctrls->temp_c=netCtrls.contents("temp_c").double_value();
    	ctrls->press_inhg=netCtrls.contents("press_inhg").double_value();

    	// other information about environment
    	ctrls->hground=netCtrls.contents("hground").double_value();// ground elevation (meters)
    	ctrls->magvar=netCtrls.contents("magvar").double_value(); // local magnetic variation in degs.

    	// hazards
    	ctrls->icing=netCtrls.contents("icing").int_value();// icing status could me much
                                         // more complex but I'm
                                         // starting simple here.

    		// simulation control
    	ctrls->speedup=netCtrls.contents("speedup").int_value();        // integer speedup multiplier
    	ctrls->freeze=netCtrls.contents("freeze").int_value();		// 0=normal
				         // 0x01=master
				         // 0x02=position
				         // 0x04=fuel

    		// --- New since FlightGear 0.9.10 (FG_NET_CTRLS_VERSION = 27)

    		// --- Add new variables just before this line.

    		//uint32_t reserved[RESERVED_SPACE];	 // 100 bytes reserved for future use.
	for(int i=0;i<25;i++){
		ctrls->reserved[i]=0;
	}
	octave_stdout << "The size of FGNetCtrls object is: "<< sizeof(FGNetCtrls)<<" bytes\n";
	net.senddata(*ctrls);//Send controls Data to FGFS	
	//delete(&ctrls);
	net.closeSocket();
	//delete(&net);
	retval= octave_value(netCtrls);//Return the netCtrls structure back to the caller.
	}
	else if(nargin==3 && args(0).is_real_scalar() && args(1).is_real_scalar() && args(2).is_string()){
	//A struct contianing netCtrls values has not been supplied as an input argument. So please populate that. ex. Code for this written in nargin_3.cc.
	octave_stdout << "A struct contianing netCtrls values has not been supplied as an input argument. So that is being created and populated.\n";
	//Set the version to 27
	netCtrls.assign("version", octave_value(27));

	//Aero Controls
	netCtrls.assign("aileron", octave_value(0));
	netCtrls.assign("elevator", octave_value(0));
	netCtrls.assign("rudder", octave_value(0));
	netCtrls.assign("aileron_trim", octave_value(0));
	netCtrls.assign("elevator_trim", octave_value(0));
	netCtrls.assign("rudder_trim", octave_value(0));
	netCtrls.assign("flaps", octave_value(0));
	netCtrls.assign("spoilers", octave_value(0));
	netCtrls.assign("speedbrake", octave_value(0));
	
	// Aero control faults
    	netCtrls.assign("flaps_power", octave_value(0));// true = power available
   	netCtrls.assign("flap_motor_ok", octave_value(0));

    	// Engine controls
   	netCtrls.assign("num_engines", octave_value(4));// number of valid engines

	octave_value_list newargs;
	newargs(0)=octave_value(1);//newargs contain the number of rows and columns of the zeros function.
	newargs(1)=octave_value(4);
	octave_value_list zeros_list=feval("zeros", newargs);//feval returns an octave_value_list
	octave_value zeros_value=zeros_list(0).array_value();//We need to provide an octave_value to assign.

	netCtrls.assign("master_bat", zeros_value);////
	netCtrls.assign("master_alt", zeros_value);
	netCtrls.assign("magnetos", zeros_value);
	netCtrls.assign("starter_power", zeros_value);
	netCtrls.assign("throttle", zeros_value);
	netCtrls.assign("mixture", zeros_value);
	netCtrls.assign("condition", zeros_value);
	netCtrls.assign("fuel_pump_power", zeros_value);
	netCtrls.assign("prop_advance", zeros_value);


	netCtrls.assign("feed_tank_to", zeros_value);
	netCtrls.assign("reverse", zeros_value);


	netCtrls.assign("engine_ok", zeros_value);
	netCtrls.assign("mag_left_ok", zeros_value);
	netCtrls.assign("mag_right_ok", zeros_value);
	netCtrls.assign("spark_plugs_ok", zeros_value);
	netCtrls.assign("oil_press_status", zeros_value);
	netCtrls.assign("fuel_pump_ok", zeros_value);

	// Fuel management
    	netCtrls.assign("num_tanks", octave_value(8));// number of valid tanks
newargs(1)=octave_value(8);
zeros_list=feval("zeros", newargs);//feval returns an octave_value_list
zeros_value=zeros_list(0).array_value();//We need to provide an octave_value to assign.

netCtrls.assign("fuel_selector", zeros_value);

	newargs(1)=octave_value(5);
	zeros_list=feval("zeros", newargs);//feval returns an octave_value_list
	zeros_value=zeros_list(0).array_value();//We need to provide an octave_value to assign.
	netCtrls.assign("xfer_pump", zeros_value);

// value tank to tank specified by
                                             // int value
    	netCtrls.assign("cross_feed", octave_value(0));// false = off, true = on

   	// Brake controls
   	netCtrls.assign("brake_left", octave_value(0));
    	netCtrls.assign("brake_right", octave_value(0));
    	netCtrls.assign("copilot_brake_left", octave_value(0));
    	netCtrls.assign("copilot_brake_right", octave_value(0));
    	netCtrls.assign("brake_parking", octave_value(0));
    
    	// Landing Gear
    	netCtrls.assign("gear_handle", octave_value(0)); // true=gear handle down; false= gear handle up

    	// Switches
    	netCtrls.assign("master_avionics", octave_value(0));
    
        // nav and Comm
   	netCtrls.assign("comm_1", octave_value(0));
    	netCtrls.assign("comm_2", octave_value(0));
    	netCtrls.assign("nav_1", octave_value(0));
    	netCtrls.assign("nav_2", octave_value(0));

    	// wind and turbulance
    	netCtrls.assign("wind_speed_kt", octave_value(0));
    	netCtrls.assign("wind_dir_deg", octave_value(0));
    	netCtrls.assign("turbulence_norm", octave_value(0));

    	// temp and pressure
    	netCtrls.assign("temp_c", octave_value(0));
    	netCtrls.assign("press_inhg", octave_value(0));

    	// other information about environment
    	netCtrls.assign("hground", octave_value(0));// ground elevation (meters)
    	netCtrls.assign("magvar", octave_value(0)); // local magnetic variation in degs.

    	// hazards
    	netCtrls.assign("icing", octave_value(0));// icing status could me much
                                         // more complex but I'm
                                         // starting simple here.

    		// simulation control
    	netCtrls.assign("speedup", octave_value(0));        // integer speedup multiplier
    	netCtrls.assign("freeze", octave_value(0));		// 0=normal
				         // 0x01=master
				         // 0x02=position
				         // 0x04=fuel

    		// --- New since FlightGear 0.9.10 (FG_NET_CTRLS_VERSION = 27)

    		// --- Add new variables just before this line.

    		//uint32_t reserved[RESERVED_SPACE];	 // 100 bytes reserved for future use.
	newargs(1)=octave_value(25);

	zeros_list=feval("zeros", newargs);//feval returns an octave_value_list
	zeros_value=zeros_list(0).array_value();//We need to provide an octave_value to assign.
	netCtrls.assign("reserved", zeros_value);	
	
	//No data is to be sent to fgfs
	retval= octave_value(netCtrls);

}
else{
	octave_stdout << "Not the right kind of input arguments\n";
	retval= octave_value(-1);//Just in case. Perhaps an error occured
}
	
	
	return retval;
	
}
