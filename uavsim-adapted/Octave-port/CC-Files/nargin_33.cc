/*else if(nargin==3){//This code assumes that the data can be fetched from FGNetCtrls object which is largely not the case. However, we put it here for future use.
	//A struct contianing netCtrls values has not been supplied as an input argument. So please populate that. ex.

//Aero Controls
	netCtrls.assign("aileron", octave_value(ctrls->aileron));
	netCtrls.assign("elevator", octave_value(ctrls->elevator));
	netCtrls.assign("rudder", octave_value(ctrls->rudder));
	netCtrls.assign("aileron_trim", octave_value(ctrls->aileron_trim));
	netCtrls.assign("elevator_trim", octave_value(ctrls->elevator_trim));
	netCtrls.assign("rudder_trim", octave_value(ctrls->rudder_trim));
	
	// Aero control faults
    	netCtrls.assign("flaps_power", octave_value(ctrls->flaps));// true = power available
   	netCtrls.assign("flap_motor_ok", octave_value(ctrls->flap_motor_ok));

    	// Engine controls
   	netCtrls.assign("num_engines", octave_value(ctrls->num_engines));// number of valid engines

	dim_vector dv(1, ctrls->num_engines);//1 row, ctrls->num_engines columns	
	int32NDArray tmp_master_bat(dv), tmp_master_alt(dv), tmp_magnetos(dv), tmp_starter_power(dv);
	NDArray tmp_throttle(dv), tmp_mixture(dv), tmp_condition(dv), tmp_fuel_pump_power(dv), tmp_prop_advance(dv);

for(int i=0; i<ctrls->num_engines;++i){
   tmp_master_bat(i)=ctrls->master_bat[i];
   tmp_master_alt(i)=ctrls->master_alt[i];
   tmp_magnetos(i)=ctrls->magnetos[i];
   tmp_starter_power(i)=ctrls->starter_power[i];// true = starter power
   tmp_throttle(i)=ctrls->throttle[i];     //  0 ... 1
   tmp_mixture(i)=ctrls->mixture[i];      //  0 ... 1
   tmp_condition(i)=ctrls->condition[i];    //  0 ... 1
   tmp_fuel_pump_power(i)=ctrls->fuel_pump_power[i];// true = on
   tmp_prop_advance(i)=ctrls->prop_advance[i]; //  0 ... 1
}//end for loop
	netCtrls.assign("master_bat", tmp_master_bat);
	netCtrls.assign("master_alt", tmp_master_alt);
	netCtrls.assign("magnetos", tmp_magnetos);
	netCtrls.assign("starter_power", tmp_starter_power);
	netCtrls.assign("throttle", tmp_throttle);
	netCtrls.assign("mixture", tmp_mixture);
	netCtrls.assign("condition", tmp_condition);
	netCtrls.assign("fuel_pump_power", tmp_fuel_pump_power);
	netCtrls.assign("prop_advance", tmp_prop_advance);

dim_vector dv1(1, 4);//1 row, 4 columns
int32NDArray tmp_feed_tank_to(dv1), tmp_reverse(dv1);
for(int i=0;i<4;++i){
   tmp_feed_tank_to(i)=ctrls->feed_tank_to[i];
   tmp_reverse(i)=ctrls->reverse[i];
}//end for loop
	netCtrls.assign("feed_tank_to", tmp_feed_tank_to);
	netCtrls.assign("reverse", tmp_reverse);

dim_vector dv2(1, ctrls->num_engines);//1 rows, ctrls->num_engines columns
int32NDArray tmp_engine_ok(dv2), tmp_mag_left_ok(dv2), tmp_mag_right_ok(dv2), tmp_spark_plugs_ok(dv2), tmp_oil_press_status(dv2), tmp_fuel_pump_ok(dv2);

for(int i=0;i<ctrls->num_engines;++i){
    	// Engine faults
   tmp_engine_ok(i)=ctrls->engine_ok[i];
   tmp_mag_left_ok(i)=ctrls->mag_left_ok[i];
   tmp_mag_right_ok(i)=ctrls->mag_right_ok[i];
   tmp_spark_plugs_ok(i)=ctrls->spark_plugs_ok[i];  // false = fouled plugs
   tmp_oil_press_status(i)=ctrls->oil_press_status[i];// 0 = normal, 1 = low, 2 = full fail
   tmp_fuel_pump_ok(i)=ctrls->fuel_pump_ok[i];
}//end for loop
	netCtrls.assign("engine_ok", tmp_engine_ok);
	netCtrls.assign("mag_left_ok", tmp_mag_left_ok);
	netCtrls.assign("mag_right_ok", tmp_mag_right_ok);
	netCtrls.assign("spark_plugs_ok", tmp_spark_plugs_ok);
	netCtrls.assign("oil_press_status", tmp_oil_press_status);
	netCtrls.assign("fuel_pump_ok", tmp_fuel_pump_ok);

	// Fuel management
    	netCtrls.assign("num_tanks", octave_value(ctrls->num_tanks));// number of valid tanks
dim_vector dv3(1, ctrls->num_tanks);//1 rows, ctrls->num_vectors columns
int32NDArray tmp_fuel_selector(dv3);
for(int i=0;i<ctrls->num_tanks;++i){
   tmp_fuel_selector(i)=ctrls->fuel_selector[i];// false = off, true = on
}//end for loop
netCtrls.assign("fuel_selector", tmp_fuel_selector);

dim_vector dv4(1, 5);//1 rows, 5 columns
int32NDArray tmp_xfer_pump(dv4);
for(int i=0;i<5;++i){
	tmp_xfer_pump(i)=ctrls->xfer_pump[i];// specifies transfer from array
}//end for loop
	netCtrls.assign("xfer_pump", tmp_xfer_pump);

// value tank to tank specified by
                                             // int value
    	netCtrls.assign("cross_feed", octave_value(ctrls->cross_feed));// false = off, true = on

   	// Brake controls
   	netCtrls.assign("brake_left", octave_value(ctrls->brake_left));
    	netCtrls.assign("brake_right", octave_value(ctrls->brake_right));
    	netCtrls.assign("copilot_brake_left", octave_value(ctrls->copilot_brake_left));
    	netCtrls.assign("copilot_brake_right", octave_value(ctrls->copilot_brake_right));
    	netCtrls.assign("brake_parking", octave_value(ctrls->brake_parking));
    
    	// Landing Gear
    	netCtrls.assign("gear_handle", octave_value(ctrls->gear_handle)); // true=gear handle down; false= gear handle up

    	// Switches
    	netCtrls.assign("master_avionics", octave_value(ctrls->master_avionics));
    
        // nav and Comm
   	netCtrls.assign("comm_1", octave_value(ctrls->comm_1));
    	netCtrls.assign("comm_2", octave_value(ctrls->comm_2));
    	netCtrls.assign("nav_1", octave_value(ctrls->nav_1));
    	netCtrls.assign("nav_2", octave_value(ctrls->nav_2));

    	// wind and turbulance
    	netCtrls.assign("wind_speed_kt", octave_value(ctrls->wind_speed_kt));
    	netCtrls.assign("wind_dir_deg", octave_value(ctrls->wind_dir_deg));
    	netCtrls.assign("turbulence_norm", octave_value(ctrls->turbulence_norm));

    	// temp and pressure
    	netCtrls.assign("temp_c", octave_value(ctrls->temp_c));
    	netCtrls.assign("press_inhg", octave_value(ctrls->press_inhg));

    	// other information about environment
    	netCtrls.assign("hground", octave_value(ctrls->hground));// ground elevation (meters)
    	netCtrls.assign("magvar", octave_value(ctrls->magvar)); // local magnetic variation in degs.

    	// hazards
    	netCtrls.assign("icing", octave_value(ctrls->icing));// icing status could me much
                                         // more complex but I'm
                                         // starting simple here.

    		// simulation control
    	netCtrls.assign("speedup", octave_value(ctrls->speedup));        // integer speedup multiplier
    	netCtrls.assign("freeze", octave_value(ctrls->freeze));		// 0=normal
				         // 0x01=master
				         // 0x02=position
				         // 0x04=fuel

    		// --- New since FlightGear 0.9.10 (FG_NET_CTRLS_VERSION = 27)

    		// --- Add new variables just before this line.

    		//uint32_t reserved[RESERVED_SPACE];	 // 100 bytes reserved for future use.
}*/
