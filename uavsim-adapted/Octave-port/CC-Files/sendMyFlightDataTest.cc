#include <octave/oct.h>
#include <octave/parse.h>
#include <octave/ov-struct.h>

#include "../../include/fdm/fdm_udp_channel.hxx"
#include "../../include/fdm/net_ctrls.hxx"

DEFUN_DLD (sendMyFlightDataTest, args, nargout,
           "This is an implementation of the sendMyFlightData function of Main.cxx of GaTAC. ")
{
	int nargin = args.length ();
	octave_value retval;
 	FGNetCtrls *ctrls = new FGNetCtrls();

	if(nargin<3||!args(0).is_real_scalar()||!args(1).is_real_scalar()||!args(2).is_string()){
	//Right now there should be exactly four input arguments: Source port number, destination port number, destination IPAddress, and a struct that will hold the Net-ctrls data.
	octave_stdout << "Right now there should be exactly three input arguments: Source port number, destination port number, destination IPAddress, and a struct that will hold the Net-Ctrls data.\n";
	return octave_value(-1);
	}

	int FDMIncomingPort = floor(args(0).int_value());	//Convert to integer, just in case.
	int CtrlOutgoingPort = floor(args(1).int_value());	//Convert to integer, just in case.
	std::string IPAddress = args(2).string_value();
	const char * IPAddress2 = IPAddress.c_str();		//cast to char *
	octave_scalar_map netCtrls;
	

	octave_stdout << "here\n";

	//FDMUDPChannel net(FDMIncomingPort, CtrlOutgoingPort, inet_addr(IPAddress2));



	return octave_value(-1);
}

