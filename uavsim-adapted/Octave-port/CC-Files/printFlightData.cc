#include <octave/oct.h>
#include <netinet/in.h>

#include "../../include/fdm/fdm_udp_channel.hxx"
#include "../../include/fdm/net_ctrls.hxx"

DEFUN_DLD (printFlightData, args, nargout,
           "This is an implementation of the printFlightData function of Main.cxx. The original function takes two input arguments: FGNetFDM fdm, FGNetCtrls ctrls. However, this implementation shall take in the socket port number and IP address. ALso, the original printFlightData has two input arguments: FGNetFDM fdm, and FGNetCtrls ctrls. This oct-file takes in those values in octave format and then initializes the constructors with them.")
{
	octave_value retval;
	int nargin = args.length ();

	if (nargin != 1)
   		 print_usage ();
  	else if (nargout != 0)
    		error ("paramdemo: OUTPUT argument required");
	else{
		FGNetFDM fdm=args(0).array_value();
		FGNetCtrls ctrls=args(1).array_value();

	  	octave_stdout << "getData has "
                << nargin << " input arguments and "
                << nargout << " output arguments.\n";
	  	fprintf(stdout,
	
		"speed: %-3.3f \
	 	[agl: %-3.3f pitch: %-3.3f elevator: %-3.3f climb: %-3.3f] \
	 	[yaw: %-3.3f rudder: %-3.3f roll: %-3.3f aileron: %-3.3f]\n",
		fdm.vcas,
		fdm.agl, fdm.theta, ctrls.elevator, fdm.climb_rate,
		fdm.psi, ctrls.rudder, fdm.phi, ctrls.aileron);
	
		cout << "me:   ";
		
		cout << "Pos(" 
			<< fdm.latitude << "," 
			<< fdm.longitude << "," 
			<< fdm.altitude << ") ";
		
		cout << "Grid(" << OpGrid.getCurrentGridLocation()[0]<<","<<OpGrid.getCurrentGridLocation()[1] << ") "; 
		
		cout << "Orient("
			<< fdm.psi << ","
			<< fdm.theta << ","
			<< fdm.phi << ")"<<endl;
}
  return octave_value_list ();
}
