#include <octave/oct.h>
#include <netinet/in.h>

#include "../../include/fdm/fdm_udp_channel.hxx"
#include "../../include/fdm/net_ctrls.hxx"

DEFUN_DLD (getMyFlightData, args, nargout,
           "This is an implementation of the getMyFlightData function of Main.cxx. The original function does not have any input arguments. However, this implementation shall take in the socket port number and IP address.")
{
  int nargin = args.length ();

  octave_stdout << "getData has "
                << nargin << " input arguments and "
                << nargout << " output arguments.\n";

	FGNetFDM *fdm;
	FGNetCtrls *ctrls = new FGNetCtrls();

	char *FDMIncomingPort = NULL;				//Receive FDM data from flightgear
	char *CtrlOutgoingPort = NULL;				//Send control data to flightgear
	char *IPAddress=NULL;

	FDMUDPChannel net(atoi(FDMIncomingPort), atoi(CtrlOutgoingPort), inet_addr(IPAddress));

	while (1) {
		// Receive FDM information from the client
		fdm = net.getdata();
		//printFlightData(*fdm, *ctrls);
	}

  return octave_value_list ();
}
