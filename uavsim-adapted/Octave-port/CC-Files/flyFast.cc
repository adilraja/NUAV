#include <octave/oct.h>
#include <octave/parse.h>
#include <octave/ov-struct.h>

#include "../../include/fdm/fdm_udp_channel.hxx"
#include "../../include/fdm/net_ctrls.hxx"

DEFUN_DLD (flyFast, args, nargout,
           "")
{
	octave_value_list retval;
	octave_function *getMyFlightData;
	octave_value input = feval(getMyFlightData);
	
	FGNetCtrls  *net_ctrls = new FGNetCtrls;
	octave_scalar_map ctrls_data;

	net_ctrls->aileron = 0.5;
	net_ctrls->elevator = 1.23;			// -1 ... 1
	net_ctrls->rudder = 1.23;			// -1 ... 1
	net_ctrls->aileron_trim = 1.23;			// -1 ... 1
	net_ctrls->elevator_trim = 1.23;		// -1 ... 1
	net_ctrls->rudder_trim = 1.23;
							
	ctrls_data.assign("aileron",octave_value(net_ctrls->aileron));
	ctrls_data.assign("elevator",octave_value(net_ctrls->elevator));
	ctrls_data.assign("rudder",octave_value(net_ctrls->rudder));
	ctrls_data.assign("aileron_trim",octave_value(net_ctrls->aileron_trim));
	ctrls_data.assign("elevator_trim",octave_value(net_ctrls->elevator_trim));
	ctrls_data.assign("rudder_trim",octave_value(net_ctrls->rudder_trim

	return octave_value(ctrls_data);
}
