#include <octave/oct.h>
#include <octave/ov-struct.h>

#include "../../include/fdm/fdm_udp_channel.hxx"
#include "../../include/fdm/net_ctrls.hxx"

DEFUN_DLD (sendFGNetFDMData, args, nargout,
           "This is an implementation of the getMyFlightData function of Main.cxx of GaTAC. The original function does not have any input arguments. However, this implementation shall take in the socket port number and IP address.")
{
  //octave_value retval;	//create object to store return values retval is a scalar of type octave_value. It is going to store a struct (fdm_data)

  //octave_value retval;
  int nargin = args.length ();

  octave_stdout << "sendFGNetFDMData has "
                << nargin << " input arguments and "
                << nargout << " output arguments.\n";


//	char *FDMIncomingPort = NULL;				//Receive FDM data from flightgear
//	char *CtrlOutgoingPort = NULL;				//Send control data to flightgear
//	char *IPAddress=NULL;

//Lets use octave variables instead of above three C variables.

	if(nargin!=5 || !args(0).is_real_scalar() || !args(1).is_real_scalar() || !args(2).is_string() || !args(4).is_real_scalar()){//Right now there should be exactly three input arguments: port number for incoming fdm data (int), port number for outgoing ctrls data, and a destination IPAddress (string).
		octave_stdout << "Right now there should be exactly 5 input arguments: port number for incoming fdm data (int), port number for outgoing net_fdm data, a destination IPAddress (string), an octave struct containing net_fdm data and an integer flag telling whether to send data with UDP or TCP: 0 for UDP and 1 for TCP.\n";
	return octave_value(-1);
		//exit(0);
	}
	FGNetFDM *fdm=new FGNetFDM();
	int FDMIncomingPort = floor(args(0).int_value());//Convert to integer, just in case.
	int CtrlOutgoingPort=floor(args(1).int_value());//Convert to integer, just in case.
	std::string IPAddress = args(2).string_value();
	const char * IPAddress2 = IPAddress.c_str();//cast to char *
	octave_scalar_map fdm_data=args(3).scalar_map_value();//create a C map equivalent to an octave struct that is going to hold values for the FGNetFDM
	int tcpudpflag=args(4).int_value();

	//FDMUDPChannel net(atoi(FDMIncomingPort), atoi(CtrlOutgoingPort), inet_addr(IPAddress));
	//Slightly change this. We do not necessarily need atoi(). We can pass integers directly.
	FDMUDPChannel* net;
	if(tcpudpflag==0){
		net = new FDMUDPChannel(FDMIncomingPort, CtrlOutgoingPort, inet_addr(IPAddress2));
		if(net->getSocketHandle()==-1){ //Meaning that there is an error.
			return octave_value(-1);
		}
	}
	else if(tcpudpflag==1){
		net = new FDMUDPChannel(FDMIncomingPort, CtrlOutgoingPort, inet_addr(IPAddress2), 1);
		if(net->getSocketHandle()==-1){ //Meaning that there is an error.
			return octave_value(-1);
		}
	}
	else{
		octave_stdout<<"Not a valid value for TCP/UDP Flag. Enter Either a zero or a one next time.\n";
	return octave_value(-1);
	}

		// Receive FDM information from the client. This should not be in a loop.
		
		//printFlightData(*fdm, *ctrls);

    fdm->version=fdm_data.contents("version").int_value();    // increment when data values change
    fdm->padding=fdm_data.contents("padding").int_value();    // padding

    // Positions
    fdm->longitude=fdm_data.contents("longitude").double_value();    // geodetic (radians)
    fdm->latitude=fdm_data.contents("latitude").double_value();    // geodetic (radians)
    fdm->altitude=fdm_data.contents("altitude").double_value();    // above sea level (meters)
    fdm->agl=fdm_data.contents("agl").float_value();      // above ground level (meters)
    fdm->phi=fdm_data.contents("phi").float_value();     // roll (radians)
    fdm->theta=fdm_data.contents("theta").float_value();;    // pitch (radians)
    fdm->psi=fdm_data.contents("psi").float_value();      // yaw or true heading (radians)
    fdm->alpha=fdm_data.contents("alpha").float_value();                // angle of attack (radians)
    fdm->beta=fdm_data.contents("beta").float_value();                 // side slip angle (radians)

    // Velocities
    fdm->phidot=fdm_data.contents("phidot").float_value();;    // roll rate (radians/sec)
    fdm->thetadot=fdm_data.contents("thetadot").float_value();;    // pitch rate (radians/sec)
    fdm->psidot=fdm_data.contents("psidot").float_value();;    // yaw rate (radians/sec)
    fdm->vcas=fdm_data.contents("vcas").float_value();;            // calibrated airspeed
    fdm->climb_rate=fdm_data.contents("climb_rate").float_value();;    // feet per second
    fdm->v_north=fdm_data.contents("v_north").float_value();              // north velocity in local/body frame, fps
    fdm->v_east=fdm_data.contents("v_east").float_value();               // east velocity in local/body frame, fps
    fdm->v_down=fdm_data.contents("v_down").float_value();;               // down/vertical velocity in local/body frame, fps
    fdm->v_wind_body_north=fdm_data.contents("v_wind_body_north").float_value();    // north velocity in local/body frame
                                // relative to local airmass, fps
    fdm->v_wind_body_east=fdm_data.contents("v_wind_body_east").float_value();     // east velocity in local/body frame
                                // relative to local airmass, fps
    fdm->v_wind_body_down=fdm_data.contents("v_wind_body_down").float_value();     // down/vertical velocity in local/body

    // Accelerations
    fdm->A_X_pilot=fdm_data.contents("A_X_pilot").float_value();;    // X accel in body frame ft/sec^2
    fdm->A_Y_pilot==fdm_data.contents("A_Y_pilot").float_value();    // Y accel in body frame ft/sec^2
    fdm->A_Z_pilot=fdm_data.contents("A_Z_pilot").float_value();    // Z accel in body frame ft/sec^2

    // Stall
    fdm->stall_warning=fdm_data.contents("stall_warning").float_value();        // 0.0 - 1.0 indicating the amount of stall
    fdm->slip_deg=fdm_data.contents("slip_deg").float_value();    // slip ball deflection

    // Pressure

    // Engine status
    fdm->num_engines=fdm_data.contents("num_engines").int_value();       // Number of valid engines
	
	dim_vector dv (1, fdm->num_engines);  // 1 rows, fdm->num_engines columns
	
	uint32NDArray tmp_eng_state(dv);
	NDArray tmp_rpm(dv), tmp_fuel_flow(dv), tmp_fuel_px(dv), tmp_egt(dv), tmp_cht(dv), tmp_mp_osi(dv), tmp_tit(dv), tmp_oil_temp(dv), tmp_oil_px(dv);
	tmp_eng_state=fdm_data.contents("eng_state").array_value();
	tmp_rpm=fdm_data.contents("rpm").array_value();
	tmp_fuel_flow=fdm_data.contents("fuel_flow").array_value();
	tmp_fuel_px=fdm_data.contents("fuel_px").array_value();
	tmp_egt=fdm_data.contents("egt").array_value();
	tmp_cht=fdm_data.contents("cht").array_value();
	tmp_mp_osi=fdm_data.contents("mp_osi").array_value();
	tmp_tit=fdm_data.contents("tit").array_value();
	tmp_oil_temp=fdm_data.contents("oil_temp").array_value();
	tmp_oil_px=fdm_data.contents("oil_px").array_value();
    for (int i = 0; i < fdm->num_engines; i++) {
		fdm->eng_state[i]=tmp_eng_state(i+1);
		fdm->rpm[i]=tmp_rpm(i+1);
		fdm->fuel_flow[i]=tmp_fuel_flow(i+1);
		fdm->fuel_px[i]=tmp_fuel_px(i+1);
		fdm->egt[i]=tmp_egt(i+1);
		fdm->cht[i]=tmp_cht(i+1);
		fdm->mp_osi[i]=tmp_mp_osi(i+1);
		fdm->tit[i]=tmp_tit(i+1);
		fdm->oil_temp[i]=tmp_oil_temp(i+1);
		fdm->oil_px[i]=tmp_oil_px(i+1);
	}

//Now assign these vectors to fdm_data
    /*fdm_data.assign("eng_state",tmp_eng_state);// Engine state (off, cranking, running)
    fdm_data.assign("rpm",tmp_rpm);       // Engine RPM rev/min
    fdm_data.assign("fuel_flow", tmp_fuel_flow); // Fuel flow gallons/hr
    fdm_data.assign("fuel_px",tmp_fuel_flow);   // Fuel pressure psi
    fdm_data.assign("egt",tmp_egt);       // Exhuast gas temp deg F
    fdm_data.assign("cht", tmp_cht);       // Cylinder head temp deg F
    fdm_data.assign("mp_osi", tmp_mp_osi);    // Manifold pressure
    fdm_data.assign("tit", tmp_tit);       // Turbine Inlet Temperature
    fdm_data.assign("oil_temp", tmp_oil_temp);  // Oil temp deg F
    fdm_data.assign("oil_px", tmp_oil_px);    // Oil pressure psi*/

    // Consumables

    fdm->num_tanks=fdm_data.contents("num_tanks").int_value();    // Max number of fuel tanks
	dim_vector dv1(1, fdm->num_tanks);//1 rows, fdm->num_tanks columns
	NDArray tmp_fuel_quantity(dv1);
	tmp_fuel_quantity=fdm_data.contents("fuel_quantity").array_value();
	for (int i = 0; i < fdm->num_tanks; ++i) {
		fdm->fuel_quantity[i]=tmp_fuel_quantity(i+1);
	}
   // fdm_data.assign("fuel_quantity", tmp_fuel_quantity);

    // Gear status
    fdm->num_wheels=fdm_data.contents("num_wheels").int_value();

	dim_vector dv2(1, fdm->num_wheels);//1 rows, fdm->num_wheels columns
	uint32NDArray tmp_wow(dv2);
	NDArray tmp_gear_pos(dv2), tmp_gear_steer(dv2), tmp_gear_compression(dv2);
	tmp_wow=fdm_data.contents("wow").array_value();
	tmp_gear_pos=fdm_data.contents("gear_pos").array_value();
	tmp_gear_steer=fdm_data.contents("gear_steer").array_value();
	tmp_gear_compression=fdm_data.contents("gear_compression").array_value();

	for (int i = 0; i < fdm->num_wheels; i++) {
		fdm->wow[i]=tmp_wow(i+1);
		fdm->gear_pos[i]=tmp_gear_pos(i+1);
		fdm->gear_steer[i]=tmp_gear_steer(i+1);
		fdm->gear_compression[i]=tmp_gear_compression(i+1);
	}
   /* fdm_data.assign("wow",tmp_wow);
    fdm_data.assign("gear_pos", tmp_gear_pos);
    fdm_data.assign("gear_steer", tmp_gear_steer);
    fdm_data.assign("gear_compression", tmp_gear_compression);*/

    // Environment
    fdm->cur_time=fdm_data.contents("cur_time").int_value();           // current unix time
                                 // FIXME: make this uint64_t before 2038
    fdm->warp=fdm_data.contents("warp").int_value();                // offset in seconds to unix time
    fdm->visibility=fdm_data.contents("visibility").float_value();            // visibility in meters (for env. effects)

    // Control surface positions (normalized values)
    fdm->elevator=fdm_data.contents("elevator").float_value();
    fdm->elevator_trim_tab=fdm_data.contents("elevator_trim_tab").float_value();
    fdm->left_flap=fdm_data.contents("left_flap").float_value();
    fdm->right_flap=fdm_data.contents("right_flap").float_value();
    fdm->left_aileron=fdm_data.contents("left_aileron").float_value();
    fdm->right_aileron=fdm_data.contents("right_aileron").float_value();
    fdm->rudder=fdm_data.contents("rudder").float_value();
    fdm->nose_wheel=fdm_data.contents("nose_wheel").float_value();
    fdm->speedbrake=fdm_data.contents("speedbrake").float_value();
    fdm->spoilers=fdm_data.contents("spoilers").float_value();

	//delete(&net);

octave_stdout << "Trying to send some FGNetFDM data!\n";
	if(tcpudpflag==0){
		net->sendFGNetFDMDataUDP(*fdm);
		net->closeSocket();
	}
	else{
		net->sendFGNetFDMDataTCP(*fdm);
		net->shutdownSocket();
	}
octave_stdout << "Sent some FGNetFDM data!\n";
	delete net;
		
  return octave_value(fdm_data);
}
