//Aero Controls
	/*netCtrls.assign("aileron", octave_value(0));
	netCtrls.assign("elevator", octave_value(0));
	netCtrls.assign("rudder", octave_value(0));
	netCtrls.assign("aileron_trim", octave_value(0));
	netCtrls.assign("elevator_trim", octave_value(0));
	netCtrls.assign("rudder_trim", octave_value(0));
	
	// Aero control faults
    	netCtrls.assign("flaps_power", octave_value(0));// true = power available
   	netCtrls.assign("flap_motor_ok", octave_value(0));*/

    	// Engine controls
   	//netCtrls.assign("num_engines", octave_value(4));// number of valid engines

	/*dim_vector dv(1, FG_MAX_ENGINES);//1 row, ctrls->num_engines columns	
	int32NDArray tmp_master_bat(dv), tmp_master_alt(dv), tmp_magnetos(dv), tmp_starter_power(dv);
	NDArray tmp_throttle(dv), tmp_mixture(dv), tmp_condition(dv), tmp_fuel_pump_power(dv), tmp_prop_advance(dv);

for(int i=0; i<ctrls->num_engines;++i){
   tmp_master_bat(i)=ctrls->master_bat[i];
   tmp_master_alt(i)=ctrls->master_alt[i];
   tmp_magnetos(i)=ctrls->magnetos[i];
   tmp_starter_power(i)=ctrls->starter_power[i];// true = starter power
   tmp_throttle(i)=ctrls->throttle[i];     //  0 ... 1
   tmp_mixture(i)=ctrls->mixture[i];      //  0 ... 1
   tmp_condition(i)=ctrls->condition[i];    //  0 ... 1
   tmp_fuel_pump_power(i)=ctrls->fuel_pump_power[i];// true = on
   tmp_prop_advance(i)=ctrls->prop_advance[i]; //  0 ... 1
}//end for loop*/
	
	//octave_value_list newargs;
	//newargs(0)=octave_value(1);//newargs contain the number of rows and columns of the zeros function.
	//newargs(1)=octave_value(4);

	/*netCtrls.assign("master_bat", feval("zeros", newargs));
	netCtrls.assign("master_alt", feval("zeros", newargs));
	netCtrls.assign("magnetos", feval("zeros", newargs));
	netCtrls.assign("starter_power", feval("zeros", newargs));
	netCtrls.assign("throttle", feval("zeros", newargs));
	netCtrls.assign("mixture", feval("zeros", newargs));
	netCtrls.assign("condition", feval("zeros", newargs));
	netCtrls.assign("fuel_pump_power", feval("zeros", newargs));
	netCtrls.assign("prop_advance", feval("zeros", newargs));*/

/*dim_vector dv1(1, 4);//1 row, 4 columns
int32NDArray tmp_feed_tank_to(dv1), tmp_reverse(dv1);
for(int i=0;i<4;++i){
   tmp_feed_tank_to(i)=ctrls->feed_tank_to[i];
   tmp_reverse(i)=ctrls->reverse[i];
}//end for loop*/
	/*netCtrls.assign("feed_tank_to", feval("zeros", newargs));
	netCtrls.assign("reverse", feval("zeros", newargs));*/

/*dim_vector dv2(1, ctrls->num_engines);//1 rows, ctrls->num_engines columns
int32NDArray tmp_engine_ok(dv2), tmp_mag_left_ok(dv2), tmp_mag_right_ok(dv2), tmp_spark_plugs_ok(dv2), tmp_oil_press_status(dv2), tmp_fuel_pump_ok(dv2);

for(int i=0;i<ctrls->num_engines;++i){
    	// Engine faults
   tmp_engine_ok(i)=ctrls->engine_ok[i];
   tmp_mag_left_ok(i)=ctrls->mag_left_ok[i];
   tmp_mag_right_ok(i)=ctrls->mag_right_ok[i];
   tmp_spark_plugs_ok(i)=ctrls->spark_plugs_ok[i];  // false = fouled plugs
   tmp_oil_press_status(i)=ctrls->oil_press_status[i];// 0 = normal, 1 = low, 2 = full fail
   tmp_fuel_pump_ok(i)=ctrls->fuel_pump_ok[i];
}//end for loop*/
	/*netCtrls.assign("engine_ok", feval("zeros", newargs));
	netCtrls.assign("mag_left_ok", feval("zeros", newargs));
	netCtrls.assign("mag_right_ok", feval("zeros", newargs));
	netCtrls.assign("spark_plugs_ok", feval("zeros", newargs));
	netCtrls.assign("oil_press_status", feval("zeros", newargs));
	netCtrls.assign("fuel_pump_ok", feval("zeros", newargs));

	// Fuel management
    	netCtrls.assign("num_tanks", octave_value(8));// number of valid tanks*/
/*dim_vector dv3(1, ctrls->num_tanks);//1 rows, ctrls->num_vectors columns
int32NDArray tmp_fuel_selector(dv3);
for(int i=0;i<ctrls->num_tanks;++i){
   tmp_fuel_selector(i)=ctrls->fuel_selector[i];// false = off, true = on
}//end for loop*/
/*newargs(1)=octave_value(8);
netCtrls.assign("fuel_selector", feval("zeros", newargs));*/

/*dim_vector dv4(1, 5);//1 rows, 5 columns
int32NDArray tmp_xfer_pump(dv4);
for(int i=0;i<5;++i){
	tmp_xfer_pump(i)=ctrls->xfer_pump[i];// specifies transfer from array
}//end for loop*/

	/*newargs(1)=octave_value(5);
	netCtrls.assign("xfer_pump", feval("zeros", newargs));*/

// value tank to tank specified by
                                             // int value
    	//netCtrls.assign("cross_feed", octave_value(0));// false = off, true = on

   	// Brake controls
   	/*netCtrls.assign("brake_left", octave_value(0));
    	netCtrls.assign("brake_right", octave_value(0));
    	netCtrls.assign("copilot_brake_left", octave_value(0));
    	netCtrls.assign("copilot_brake_right", octave_value(0));
    	netCtrls.assign("brake_parking", octave_value(0));*/
    
    	// Landing Gear
    	//netCtrls.assign("gear_handle", octave_value(0)); // true=gear handle down; false= gear handle up

    	// Switches
    	/*netCtrls.assign("master_avionics", octave_value(0));
    
        // nav and Comm
   	netCtrls.assign("comm_1", octave_value(0));
    	netCtrls.assign("comm_2", octave_value(0));
    	netCtrls.assign("nav_1", octave_value(0));
    	netCtrls.assign("nav_2", octave_value(0));*/

    	// wind and turbulance
    	/*netCtrls.assign("wind_speed_kt", octave_value(0));
    	netCtrls.assign("wind_dir_deg", octave_value(0));
    	netCtrls.assign("turbulence_norm", octave_value(0));*/

    	// temp and pressure
    	/*netCtrls.assign("temp_c", octave_value(0));
    	netCtrls.assign("press_inhg", octave_value(0));*/

    	// other information about environment
    	/*netCtrls.assign("hground", octave_value(0));// ground elevation (meters)
    	netCtrls.assign("magvar", octave_value(0)); // local magnetic variation in degs.*/

    	// hazards
    	//netCtrls.assign("icing", octave_value(ctrls->icing));// icing status could me much
                                         // more complex but I'm
                                         // starting simple here.

    		// simulation control
    	//netCtrls.assign("speedup", octave_value(0));        // integer speedup multiplier
    	//netCtrls.assign("freeze", octave_value(0));		// 0=normal
				         // 0x01=master
				         // 0x02=position
				         // 0x04=fuel

    		// --- New since FlightGear 0.9.10 (FG_NET_CTRLS_VERSION = 27)

    		// --- Add new variables just before this line.

    		//uint32_t reserved[RESERVED_SPACE];	 // 100 bytes reserved for future use.

