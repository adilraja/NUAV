#include <octave/oct.h>
#include <octave/parse.h>
#include <octave/ov-struct.h>

#include "../../include/fdm/fdm_udp_channel.hxx"
#include "../../include/fdm/net_ctrls.hxx"

DEFUN_DLD (getMyFGNetCtrlsData, args, nargout,
           "This implementation is inspired from the getMyFlightData function of Main.cxx of GaTAC. The original function does not have any input arguments. However, this implementation shall take in the socket port number and IP address.")
{
  //octave_value retval;	//create object to store return values retval is a scalar of type octave_value. It is going to store a struct (net_ctrls_data)

  //octave_value retval;
  int nargin = args.length ();

  octave_stdout << "getMyFlightData has "
                << nargin << " input arguments and "
                << nargout << " output arguments.\n";

	FGNetCtrls *net_ctrls;

//	char *FDMIncomingPort = NULL;				//Receive FDM data from flightgear
//	char *CtrlOutgoingPort = NULL;				//Send control data to flightgear
//	char *IPAddress=NULL;

//Lets use octave variables instead of above three C variables.

	if(nargin!=3 || !args(0).is_real_scalar() || !args(1).is_real_scalar() || !args(2).is_string()){//Right now there should be exactly three input arguments: port number for incoming fdm data (int), port number for outgoing ctrls data, and a destination IPAddress (string).
		octave_stdout << "Right now there should be exactly 3 three input arguments: port number for incoming fdm data (int), port number for outgoing ctrls data, and a destination IPAddress (string).\n";
	return octave_value(-1);
		//exit(0);
	}
	int FDMIncomingPort = floor(args(0).int_value());//Convert to integer, just in case.
	int CtrlOutgoingPort=floor(args(1).int_value());//Convert to integer, just in case.
	std::string IPAddress = args(2).string_value();
	const char * IPAddress2 = IPAddress.c_str();//cast to char *
	octave_scalar_map net_ctrls_data;//create a C map equivalent to an octave struct that is going to hold values for the FGNetFDM

	//FDMUDPChannel net(atoi(FDMIncomingPort), atoi(CtrlOutgoingPort), inet_addr(IPAddress));
	//Slightly change this. We do not necessarily need atoi(). We can pass integers directly.
	FDMUDPChannel net(FDMIncomingPort, CtrlOutgoingPort, inet_addr(IPAddress2));
	if(net.getSocketHandle()==-1){ //Meaning that there is an error.
		return octave_value(-1);
	}

		// Receive FDM information from the client. This should not be in a loop.
	octave_stdout << "Trying to get some data!\n";
	if((net_ctrls = net.getFGNetCtrlsData())==NULL){
		octave_stdout << "Could not get anything! An error must have occured\n";
		return octave_value(-1);//-1 means an error occured.
	}
	else
		octave_stdout << "Got some data!\n";
		
		//printFlightData(*fdm, *ctrls);

    net_ctrls_data.assign("version",octave_value(net_ctrls->version));    // increment when data values change
    
    // Engine status
    net_ctrls_data.assign("num_engines",octave_value(net_ctrls->num_engines));       // Number of valid engines
	
	

    // Consumables
    net_ctrls_data.assign("num_tanks",octave_value(net_ctrls->num_tanks));    // Max number of fuel tanks
	
    // Control surface positions (normalized values)
    net_ctrls_data.assign("elevator",octave_value(net_ctrls->elevator));
    net_ctrls_data.assign("rudder",octave_value(net_ctrls->rudder));
    net_ctrls_data.assign("speedbrake",octave_value(net_ctrls->speedbrake));
    net_ctrls_data.assign("spoilers",octave_value(net_ctrls->spoilers));

	//delete(&net);

//Aero Controls
	net_ctrls_data.assign("aileron", octave_value(net_ctrls->aileron));
	net_ctrls_data.assign("elevator", octave_value(net_ctrls->elevator));
	net_ctrls_data.assign("rudder", octave_value(net_ctrls->rudder));
	net_ctrls_data.assign("aileron_trim", octave_value(net_ctrls->aileron_trim));
	net_ctrls_data.assign("elevator_trim", octave_value(net_ctrls->elevator_trim));
	net_ctrls_data.assign("rudder_trim", octave_value(net_ctrls->rudder_trim));
	net_ctrls_data.assign("flaps", octave_value(net_ctrls->flaps));
	net_ctrls_data.assign("spoilers", octave_value(net_ctrls->spoilers));
	net_ctrls_data.assign("speedbrake", octave_value(net_ctrls->speedbrake));
	
	// Aero control faults
    	net_ctrls_data.assign("flaps_power", octave_value(net_ctrls->flaps_power));// true = power available
   	net_ctrls_data.assign("flap_motor_ok", octave_value(net_ctrls->flap_motor_ok));

    	// Engine controls
   	net_ctrls_data.assign("num_engines", octave_value(net_ctrls->num_engines));// number of valid engines

	/*

	net_ctrls_data.assign("master_bat", zeros_value);////
	net_ctrls_data.assign("master_alt", zeros_value);
	net_ctrls_data.assign("magnetos", zeros_value);
	net_ctrls_data.assign("starter_power", zeros_value);
	net_ctrls_data.assign("throttle", zeros_value);
	net_ctrls_data.assign("mixture", zeros_value);
	net_ctrls_data.assign("condition", zeros_value);
	net_ctrls_data.assign("fuel_pump_power", zeros_value);
	net_ctrls_data.assign("prop_advance", zeros_value);


	net_ctrls_data.assign("feed_tank_to", zeros_value);
	net_ctrls_data.assign("reverse", zeros_value);


	net_ctrls_data.assign("engine_ok", zeros_value);
	net_ctrls_data.assign("mag_left_ok", zeros_value);
	net_ctrls_data.assign("mag_right_ok", zeros_value);
	net_ctrls_data.assign("spark_plugs_ok", zeros_value);
	net_ctrls_data.assign("oil_press_status", zeros_value);
	net_ctrls_data.assign("fuel_pump_ok", zeros_value);*/

	// Fuel management
    	net_ctrls_data.assign("num_tanks", octave_value(net_ctrls->num_tanks));// number of valid tanks

//net_ctrls_data.assign("fuel_selector", zeros_value);

	
// value tank to tank specified by
                                             // int value
    	net_ctrls_data.assign("cross_feed", octave_value(net_ctrls->cross_feed));// false = off, true = on

   	// Brake controls
   	net_ctrls_data.assign("brake_left", octave_value(net_ctrls->brake_left));
    	net_ctrls_data.assign("brake_right", octave_value(net_ctrls->brake_right));
    	net_ctrls_data.assign("copilot_brake_left", octave_value(net_ctrls->copilot_brake_left));
    	net_ctrls_data.assign("copilot_brake_right", octave_value(net_ctrls->copilot_brake_right));
    	net_ctrls_data.assign("brake_parking", octave_value(net_ctrls->brake_parking));
    
    	// Landing Gear
    	net_ctrls_data.assign("gear_handle", octave_value(net_ctrls->gear_handle)); // true=gear handle down; false= gear handle up

    	// Switches
    	net_ctrls_data.assign("master_avionics", octave_value(net_ctrls->master_avionics));
    
        // nav and Comm
   	net_ctrls_data.assign("comm_1", octave_value(net_ctrls->comm_1));
    	net_ctrls_data.assign("comm_2", octave_value(net_ctrls->comm_2));
    	net_ctrls_data.assign("nav_1", octave_value(net_ctrls->nav_1));
    	net_ctrls_data.assign("nav_2", octave_value(net_ctrls->nav_2));

    	// wind and turbulance
    	net_ctrls_data.assign("wind_speed_kt", octave_value(net_ctrls->wind_speed_kt));
    	net_ctrls_data.assign("wind_dir_deg", octave_value(net_ctrls->wind_dir_deg));
    	net_ctrls_data.assign("turbulence_norm", octave_value(net_ctrls->turbulence_norm));

    	// temp and pressure
    	net_ctrls_data.assign("temp_c", octave_value(net_ctrls->temp_c));
    	net_ctrls_data.assign("press_inhg", octave_value(net_ctrls->press_inhg));

    	// other information about environment
    	net_ctrls_data.assign("hground", octave_value(net_ctrls->hground));// ground elevation (meters)
    	net_ctrls_data.assign("magvar", octave_value(net_ctrls->magvar)); // local magnetic variation in degs.

    	// hazards
    	net_ctrls_data.assign("icing", octave_value(net_ctrls->icing));// icing status could me much
                                         // more complex but I'm
                                         // starting simple here.

    		// simulation control
    	net_ctrls_data.assign("speedup", octave_value(net_ctrls->speedup));        // integer speedup multiplier
    	net_ctrls_data.assign("freeze", octave_value(net_ctrls->freeze));		// 0=normal
				         // 0x01=master
				         // 0x02=position
				         // 0x04=fuel

    		// --- New since FlightGear 0.9.10 (FG_NET_CTRLS_VERSION = 27)

    		// --- Add new variables just before this line.

    		//uint32_t reserved[RESERVED_SPACE];	 // 100 bytes reserved for future use.
	octave_value_list newargs;
	
	newargs(0)=octave_value(1);
	newargs(1)=octave_value(25);

	octave_value_list zeros_list=feval("zeros", newargs);//feval returns an octave_value_list
	octave_value zeros_value=zeros_list(0).array_value();//We need to provide an octave_value to assign.
	net_ctrls_data.assign("reserved", zeros_value);

  return octave_value(net_ctrls_data);
}
