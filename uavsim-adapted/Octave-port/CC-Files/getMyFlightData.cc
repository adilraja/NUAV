#include <octave/oct.h>
#include <octave/ov-struct.h>

#include "../../include/fdm/fdm_udp_channel.hxx"
#include "../../include/fdm/net_ctrls.hxx"

DEFUN_DLD (getMyFlightData, args, nargout,
           "This is an implementation of the getMyFlightData function of Main.cxx of GaTAC. The original function does not have any input arguments. However, this implementation shall take in the socket port number and IP address.")
{
  //octave_value retval;	//create object to store return values retval is a scalar of type octave_value. It is going to store a struct (fdm_data)

  //octave_value retval;
  int nargin = args.length ();

  octave_stdout << "getMyFlightData has "
                << nargin << " input arguments and "
                << nargout << " output arguments.\n";

	FGNetFDM *fdm;

//	char *FDMIncomingPort = NULL;				//Receive FDM data from flightgear
//	char *CtrlOutgoingPort = NULL;				//Send control data to flightgear
//	char *IPAddress=NULL;

//Lets use octave variables instead of above three C variables.

	if(nargin!=3 || !args(0).is_real_scalar() || !args(1).is_real_scalar() || !args(2).is_string()){//Right now there should be exactly three input arguments: port number for incoming fdm data (int), port number for outgoing ctrls data, and a destination IPAddress (string).
		octave_stdout << "Right now there should be exactly 3 three input arguments: port number for incoming fdm data (int), port number for outgoing ctrls data, and a destination IPAddress (string).\n";
	return octave_value(-1);
		//exit(0);
	}
	int FDMIncomingPort = floor(args(0).int_value());//Convert to integer, just in case.
	int CtrlOutgoingPort=floor(args(1).int_value());//Convert to integer, just in case.
	std::string IPAddress = args(2).string_value();
	const char * IPAddress2 = IPAddress.c_str();//cast to char *
	octave_scalar_map fdm_data;//create a C map equivalent to an octave struct that is going to hold values for the FGNetFDM

	//FDMUDPChannel net(atoi(FDMIncomingPort), atoi(CtrlOutgoingPort), inet_addr(IPAddress));
	//Slightly change this. We do not necessarily need atoi(). We can pass integers directly.
	FDMUDPChannel net(FDMIncomingPort, CtrlOutgoingPort, inet_addr(IPAddress2));
	if(net.getSocketHandle()==-1){ //Meaning that there is an error.
		return octave_value(-1);
	}

		// Receive FDM information from the client. This should not be in a loop.
	octave_stdout << "Trying to get some data!\n";
	if((fdm = net.getdata())==NULL){
		octave_stdout << "Could not get anything! An error must have occured\n";
		return octave_value(-1);//-1 means an error occured.
	}
	else
		octave_stdout << "Got some data!\n";
		
		//printFlightData(*fdm, *ctrls);

    fdm_data.assign("version",octave_value(fdm->version));    // increment when data values change
    fdm_data.assign("padding",octave_value(fdm->padding));    // padding

    // Positions
    fdm_data.assign("longitude",octave_value(fdm->longitude));    // geodetic (radians)
    fdm_data.assign("latitude",octave_value(fdm->latitude));    // geodetic (radians)
    fdm_data.assign("altitude",octave_value(fdm->altitude));    // above sea level (meters)
    fdm_data.assign("agl",octave_value(fdm->agl));      // above ground level (meters)
    fdm_data.assign("phi",octave_value(fdm->phi));      // roll (radians)
    fdm_data.assign("theta",octave_value(fdm->theta));    // pitch (radians)
    fdm_data.assign("psi",octave_value(fdm->psi));      // yaw or true heading (radians)
    fdm_data.assign("alpha",octave_value(fdm->alpha));                // angle of attack (radians)
    fdm_data.assign("beta",octave_value(fdm->beta));                 // side slip angle (radians)

    // Velocities
    fdm_data.assign("phidot",octave_value(fdm->phidot));    // roll rate (radians/sec)
    fdm_data.assign("thetadot",octave_value(fdm->thetadot));    // pitch rate (radians/sec)
    fdm_data.assign("psidot",octave_value(fdm->psidot));    // yaw rate (radians/sec)
    fdm_data.assign("vcas",octave_value(fdm->vcas));            // calibrated airspeed
    fdm_data.assign("climb_rate",octave_value(fdm->climb_rate));    // feet per second
    fdm_data.assign("v_north",octave_value(fdm->v_north));              // north velocity in local/body frame, fps
    fdm_data.assign("v_east",octave_value(fdm->v_east));               // east velocity in local/body frame, fps
    fdm_data.assign("v_down",octave_value(fdm->v_down));               // down/vertical velocity in local/body frame, fps
    fdm_data.assign("v_wind_body_north",octave_value(fdm->v_wind_body_north));    // north velocity in local/body frame
                                // relative to local airmass, fps
    fdm_data.assign("v_wind_body_east",octave_value(fdm->v_wind_body_east));     // east velocity in local/body frame
                                // relative to local airmass, fps
    fdm_data.assign("v_wind_body_down",octave_value(fdm->v_wind_body_down));     // down/vertical velocity in local/body

    // Accelerations
    fdm_data.assign("A_X_pilot",octave_value(fdm->A_X_pilot));    // X accel in body frame ft/sec^2
    fdm_data.assign("A_Y_pilot",octave_value(fdm->A_Y_pilot));    // Y accel in body frame ft/sec^2
    fdm_data.assign("A_Z_pilot",octave_value(fdm->A_Z_pilot));    // Z accel in body frame ft/sec^2

    // Stall
    fdm_data.assign("stall_warning",octave_value(fdm->stall_warning));        // 0.0 - 1.0 indicating the amount of stall
    fdm_data.assign("slip_deg",octave_value(fdm->slip_deg));    // slip ball deflection

    // Pressure

    // Engine status
    fdm_data.assign("num_engines",octave_value(fdm->num_engines));       // Number of valid engines
	
	dim_vector dv (1, fdm->num_engines);  // 1 rows, fdm->num_engines columns
	
	uint32NDArray tmp_eng_state(dv);
	NDArray tmp_rpm(dv), tmp_fuel_flow(dv), tmp_fuel_px(dv), tmp_egt(dv), tmp_cht(dv), tmp_mp_osi(dv), tmp_tit(dv), tmp_oil_temp(dv), tmp_oil_px(dv);
    for (int i = 0; i < fdm->num_engines; ++i) {
		tmp_eng_state(i)=octave_value(fdm->eng_state[i]).int_value();
		tmp_rpm(i)=octave_value(fdm->rpm[i]).float_value();
		tmp_fuel_flow(i)=octave_value(fdm->fuel_flow[i]).float_value();
		tmp_fuel_px(i)=octave_value(fdm->fuel_px[i]).float_value();
		tmp_egt(i)=octave_value(fdm->egt[i]).float_value();
		tmp_cht(i)=octave_value(fdm->cht[i]).float_value();
		tmp_mp_osi(i)=octave_value(fdm->mp_osi[i]).float_value();
		tmp_tit(i)=octave_value(fdm->tit[i]).float_value();
		tmp_oil_temp(i)=octave_value(fdm->oil_temp[i]).float_value();
		tmp_oil_px(i)=octave_value(fdm->oil_px[i]).float_value();
	}
//Now assign these vectors to fdm_data
    fdm_data.assign("eng_state",tmp_eng_state);// Engine state (off, cranking, running)
    fdm_data.assign("rpm",tmp_rpm);       // Engine RPM rev/min
    fdm_data.assign("fuel_flow", tmp_fuel_flow); // Fuel flow gallons/hr
    fdm_data.assign("fuel_px",tmp_fuel_flow);   // Fuel pressure psi
    fdm_data.assign("egt",tmp_egt);       // Exhuast gas temp deg F
    fdm_data.assign("cht", tmp_cht);       // Cylinder head temp deg F
    fdm_data.assign("mp_osi", tmp_mp_osi);    // Manifold pressure
    fdm_data.assign("tit", tmp_tit);       // Turbine Inlet Temperature
    fdm_data.assign("oil_temp", tmp_oil_temp);  // Oil temp deg F
    fdm_data.assign("oil_px", tmp_oil_px);    // Oil pressure psi

    // Consumables
    fdm_data.assign("num_tanks",octave_value(fdm->num_tanks));    // Max number of fuel tanks
	dim_vector dv1(1, fdm->num_tanks);//1 rows, fdm->num_tanks columns
	NDArray tmp_fuel_quantity(dv1);
	for (int i = 0; i < fdm->num_tanks; ++i) {
		tmp_fuel_quantity(i)=octave_value(fdm->fuel_quantity[i]).float_value();
	}
    fdm_data.assign("fuel_quantity", tmp_fuel_quantity);

    // Gear status
    fdm_data.assign("num_wheels",octave_value(fdm->num_wheels));
	dim_vector dv2(1, fdm->num_wheels);//1 rows, fdm->num_wheels columns
	uint32NDArray tmp_wow(dv2);
	NDArray tmp_gear_pos(dv2), tmp_gear_steer(dv2), tmp_gear_compression(dv2);
	for (int i = 0; i < fdm->num_wheels; ++i) {
		tmp_wow(i)= octave_value(fdm->wow[i]).int_value();
		tmp_gear_pos(i)=octave_value(fdm->gear_pos[i]).float_value();
		tmp_gear_steer(i)=octave_value(fdm->gear_steer[i]).float_value();
		tmp_gear_compression(i)=octave_value(fdm->gear_compression[i]).float_value();
	}
    fdm_data.assign("wow",tmp_wow);
    fdm_data.assign("gear_pos", tmp_gear_pos);
    fdm_data.assign("gear_steer", tmp_gear_steer);
    fdm_data.assign("gear_compression", tmp_gear_compression);

    // Environment
    fdm_data.assign("cur_time",octave_value(fdm->cur_time));           // current unix time
                                 // FIXME: make this uint64_t before 2038
    fdm_data.assign("warp",octave_value(fdm->warp));                // offset in seconds to unix time
    fdm_data.assign("visibility",octave_value(fdm->visibility));            // visibility in meters (for env. effects)

    // Control surface positions (normalized values)
    fdm_data.assign("elevator",octave_value(fdm->elevator));
    fdm_data.assign("elevator_trim_tab",octave_value(fdm->elevator_trim_tab));
    fdm_data.assign("left_flap",octave_value(fdm->left_flap));
    fdm_data.assign("right_flap",octave_value(fdm->right_flap));
    fdm_data.assign("left_aileron",octave_value(fdm->left_aileron));
    fdm_data.assign("right_aileron",octave_value(fdm->right_aileron));
    fdm_data.assign("rudder",octave_value(fdm->rudder));
    fdm_data.assign("nose_wheel",octave_value(fdm->nose_wheel));
    fdm_data.assign("speedbrake",octave_value(fdm->speedbrake));
    fdm_data.assign("spoilers",octave_value(fdm->spoilers));

	//delete(&net);

  return octave_value(fdm_data);
}
