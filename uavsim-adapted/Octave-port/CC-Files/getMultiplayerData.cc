#include <octave/oct.h>
#include <octave/ov-struct.h>

#include "../../include/Multiplayer/multiplayer_udp_channel.hxx"
#include "../../include/fdm/net_ctrls.hxx"

DEFUN_DLD (getMultiplayerData, args, nargout,
           "This is an implementation of the getMultiplayerData function of Main.cxx of GaTAC. The original function does not have any input arguments. However, this implementation shall take in the socket port number, and callsigntowatch IP address.")
{
  //octave_value retval;	//create object to store return values retval is a scalar of type octave_value. It is going to store a struct (fdm_data)

  //octave_value retval;
  int nargin = args.length ();

  octave_stdout << "getMultiplayerData has "
                << nargin << " input arguments and "
                << nargout << " output arguments.\n";

//	char *FDMIncomingPort = NULL;				//Receive FDM data from flightgear
//	char *CtrlOutgoingPort = NULL;				//Send control data to flightgear
//	char *IPAddress=NULL;

//Lets use octave variables instead of above three C variables.

if(nargin==3 && args(0).is_real_scalar() && args(1).is_string() && args(2).is_string()){
	int MultiplayerIncomingPort=floor(args(0).int_value());
	std::string IPAddress = args(1).string_value();
	const char * IPAddress2 = IPAddress.c_str();//cast to char *
	std::string CallsignToWatch=args(2).string_value();
	char *CallsignToWatch2 = new char[CallsignToWatch.length()+1];//
	strcpy(CallsignToWatch2, CallsignToWatch.c_str());

	//FDMUDPChannel net(atoi(FDMIncomingPort), atoi(CtrlOutgoingPort), inet_addr(IPAddress));
	//Slightly change this. We do not necessarily need atoi(). We can pass integers directly.
	MultiplayerUDPChannel net(MultiplayerIncomingPort);

	cout << "multi-in socket initialized" << endl;
	int bytes;
	//while(1) {

		net.fowardData(CallsignToWatch2);

		//signal(SIGINT, terminate);
	//}
}//endif
}
