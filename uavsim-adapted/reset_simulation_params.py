import sys
import telnetlib

PROMPT = ':~$'
fg = telnetlib.Telnet();
fg.open(sys.argv[1],sys.argv[2]);
# longitude and latitude should be of the same airport otherwise it will hang in the air . Reason It had to download the scenery from teragear
fg.write("set /position/altitude-ft %s\r\n" % sys.argv[3]);
print fg.read_until('(double)')
fg.write("set /position/latitude-deg %s\r\n" % sys.argv[4]);
print fg.read_until('(double)')
fg.write("set /position/longitude-deg %s\r\n" % sys.argv[5]);
print fg.read_until('(double)')

fg.close()
