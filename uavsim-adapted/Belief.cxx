/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include"include/Ipomdp/Belief.hxx"


Belief::Belief(string filename){
	ifstream inputfile(filename.c_str());
	string belStr;
	if(inputfile.is_open()){
		getline(inputfile, belStr);
		vector <string> tokens;
		Tokenize(belStr, tokens);
		for(int i=0; i<tokens.size(); i++){
			belief.push_back(atof(tokens[i].c_str()));
		}
		inputfile.close();
	}
	else{
		cout<<"couldn't open belief vector file";
		exit(0);
	}
}

Belief::Belief(vector<double> NewBelief){
	for(int i=0; i<NewBelief.size();i++){
			belief.push_back(NewBelief[i]);
	}
}

vector<double> Belief::getBelief(){
	return belief;
}

void Belief::setBelief(vector<double> NewBelief){
	for(int i = 0; i<NewBelief.size();i++){
		belief[i]=NewBelief[i];
	}
}



void Belief::printBelief(){
	cout<<"\n";
	for(int i = 0; i<belief.size();i++){
		cout<<belief[i]<<" ";
	}
	cout<<"\n";
}
//int main(){
//	Belief bel;
//	vector <double> x;
//	x = bel.getBelief();
//	for(int i=0; i<x.size();i++){
//		cout<<x[i]<<" ";
//	}
//	cout<<"\n";
//	x=bel.updateBelief(SenseFound, Listen, true);
//	for(int i=0; i<x.size();i++){
//		cout<<x[i]<<" ";
//	}
//	return 1;
//}
