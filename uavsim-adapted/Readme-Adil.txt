Requires installation of additional libraries

Plib
SimGear


There is now a folder Octave-port which contains cc files to be converted to oct-files using mkoctfile.

In order to create an oct-file you will have to do one of the following.

so you can do one of three alternatives, either 1) link all relevant source files into the oct file using mkoctfile, 2) compile the fdm source files into .o files, and link those into the oct file, or 3) arrange for a fdm library to be built, and link the oct file with that

And to do this I use the following set of commands.

1) I run this in the root directory of uavsim:

g++ -fPIC -c *.cxx

2.a) Then I run this to create libGaTAC.so shared library.

g++ -shared fdm_udp_channel.o multiplayer_udp_channel.o airvehicle.o unmannedC172.o Grid_2D.o Policy.o netSocket.o tiny_xdr.o SGGeodesy.o AlphaVector.o Belief.o -o libGaTAC.so

2.b) The above does not work well. Alternatively I run the following and it works.

ar rvs libGaTAC.a *.o

This creates a static library libGaTAC.a

3.a) mv libGaTAC.so Octave-port/CC-Files/libGaTAC.so //Octave-port/CC-Files/ is the directory containing cc files for our oct-files.

3.b) mv libGaTAC.a Octave-port/CC-Files/

4) And finally I move to the directory containing the cc-files for our oct-files and run this in octave.

mkoctfile getMyFlightData.cc libGaTAC.a 

The above line of code is for the static library and it works. Following are commands for shared libraries and they do not work. But I am just putting them here for your kind perusal. Just in case!

>> mkoctfile getMyFlightData.cc '-L /home/adil/Dropbox/Namal/Research/UAVs-NAMAL/Software/uavsim-adapted/Octave-port/CC-Files' '-l GaTAC'

Or the following in shell (bash):

mkoctfile getMyFlightData.cc -L/home/adil/Dropbox/Namal/Research/UAVs-NAMAL/Software/uavsim-adapted/Octave-port/CC-Files -lGaTAC

Note that running the following does not compile:

 mkoctfile getMyFlightData.cc -L~/Dropbox/Namal
/Research/UAVs-NAMAL/Software/uavsim-adapted/Octave-port/CC-Files -llibGaTAC.so

or even the following does not compile:

mkoctfile getMyFlightData.cc -L~/Dropbox/Namal
/Research/UAVs-NAMAL/Software/uavsim-adapted/Octave-port/CC-Files '-l libGaTAC.so'

So if your shared library file is libGaTAC.so, you should provide its name as GaTAC with the -l option.

You will also have to do: LD_LIBRARY_PATH=`pwd`

where pwd should point to the directory where your libGaTAC.so is lying.

