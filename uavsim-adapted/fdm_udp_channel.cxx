/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>

#include "include/fdm/fdm_udp_channel.hxx"
#include "include/fdm/net_ctrls.hxx"

FDMUDPChannel::FDMUDPChannel(unsigned int l_port, unsigned int s_port,
		in_addr_t addr_to) {
	//Initialize the socket
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
		sock=printErrorMessage("Failed to create socket");//sock gets the value of the error message. Adil.

	//Construct the server sockaddr_in struct
	memset(&server, 0, sizeof(server)); // Clear struct 
	server.sin_family = AF_INET; // Internet/IP 
	server.sin_addr.s_addr = htonl(INADDR_ANY); // Any IP address 
	server.sin_port = htons(l_port); //  server port 

	//Bind the server to the socket
	int serverlen = sizeof(server);
	if (bind(sock, (struct sockaddr *) &server, serverlen) == -1) {
		sock=printErrorMessage("Failed to bind server to socket and port");//sock gets the value of the error message. Adil.
	}

	//Construct the client sockaddr_in struct
	memset(&client, 0, sizeof(client)); // Clear struct 
	client.sin_family = AF_INET; // Internet/IP 
	client.sin_addr.s_addr = addr_to; // client IP address 
	client.sin_port = htons(s_port); // client port 

}

FDMUDPChannel::FDMUDPChannel(unsigned int l_port, unsigned int s_port,
		in_addr_t addr_to, int tcpudpflag) {//Constructor added by Adil to develop TCP connections.
	//Initialize the socket
	if ((sock = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		sock=printErrorMessage("Failed to create socket");//sock gets the value of the error message. Adil.
	

	//Construct the server sockaddr_in struct
	memset(&server, 0, sizeof(server)); // Clear struct 
	server.sin_family = AF_INET; // Internet/IP 
	server.sin_addr.s_addr = htonl(INADDR_ANY); // Any IP address 
	server.sin_port = htons(l_port); //  server port 

	//Bind the server to the socket
	int serverlen = sizeof(server);
	if (bind(sock, (struct sockaddr *) &server, serverlen) == -1) {
		sock=printErrorMessage("Failed to bind server to socket and port\n");//sock gets the value of the error message. Adil.
	}

	//Construct the client sockaddr_in struct
	memset(&client, 0, sizeof(client)); // Clear struct 
	client.sin_family = AF_INET; // Internet/IP 
	client.sin_addr.s_addr = addr_to; // client IP address 
	client.sin_port = htons(s_port); // client port 

	if(connect(sock, (struct sockaddr *) &client, sizeof(client))<0){
		sock=printErrorMessage("Failed to connect.\n");
	}

}

int FDMUDPChannel::sendFGNetFDMDataTCP(FGNetFDM net_fdm){// This is written by Adil.
	char* dataRecvd[4];
	htonFGNetFDM(&net_fdm);
	if (send(sock, (char *)(&net_fdm), sizeof(net_fdm), 0) == -1) {
		return printErrorMessage("Failed to send FGNetFDM data.\n");
	}
	if(recv(sock, dataRecvd, sizeof(dataRecvd),0)<0){
		return printErrorMessage("I think we failed to recive an ACK here.\n");
	}
}

int FDMUDPChannel::sendFGNetFDMDataUDP(FGNetFDM net_fdm){// This is written by Adil.
	char* dataRecvd[4];
	htonFGNetFDM(&net_fdm);
	if (sendto(sock, (char *)(&net_fdm), sizeof(net_fdm), 0, (struct sockaddr *) &client, sizeof(client)) == -1) {
		return printErrorMessage("Failed to send FGNetFDM data.\n");
	}
//	if(recv(sock, dataRecvd, sizeof(dataRecvd),0)<0){
//		return printErrorMessage("I think we failed to recive an ACK here.\n");
//	}
}


int FDMUDPChannel::senddata(FGNetCtrls net_ctrls) {//initially return type was null. To accomodate the error message -1, this change has been made. Adil	 
	htonFGNetCtrls(&net_ctrls);
	//Send the data to the client socket
	//cout<<"senddata" <<net_ctrls.version<<"\n\n\n\n\n\n\n\n\n";
	
	if (sendto(sock, (char *)(&net_ctrls), sizeof(net_ctrls), 0,
			(struct sockaddr *) &client, sizeof(client)) == -1) {
		return printErrorMessage("Failed to send FGNetCtrls data");
	}
}

int FDMUDPChannel::getSocketHandle(){//I added this to get some error codes. Adil
	return sock;
}

FGNetCtrls* FDMUDPChannel::getFGNetCtrlsData(){//Written by Adil
	int received;
	FGNetCtrls *net_ctrls = new FGNetCtrls();
	struct sockaddr_in tmp;
	//Receive the data from bound socket
	unsigned int len = sizeof(tmp);
	if ((received = recvfrom(sock, (char *) (net_ctrls), sizeof(*net_ctrls), 0,
			(struct sockaddr *) &tmp, &len)) ==-1) {
		printErrorMessage("Failed to receive message");
		return NULL;//I added this. Adil
	}
	htonFGNetCtrls(net_ctrls);
	return net_ctrls;
}

FGNetFDM* FDMUDPChannel::getdata() {
	int received;
	FGNetFDM *net_fdm = new FGNetFDM();
	struct sockaddr_in tmp;

	//Receive the data from bound socket
	unsigned int len = sizeof(tmp);
	if ((received = recvfrom(sock, (char *) (net_fdm), sizeof(*net_fdm), 0,
			(struct sockaddr *) &tmp, &len)) ==-1) {
		printErrorMessage("Failed to receive message");
		return NULL;//I added this. Adil
	}

	//Convert from network byte order to host byte order
	net_fdm->version = ntohl(net_fdm->version);

	htond(net_fdm->longitude);
	htond(net_fdm->latitude);
	htond(net_fdm->altitude);
	htonf(net_fdm->agl);
	htonf(net_fdm->phi);
	htonf(net_fdm->theta);
	htonf(net_fdm->psi);
	htonf(net_fdm->alpha);
	htonf(net_fdm->beta);

	htonf(net_fdm->phidot);
	htonf(net_fdm->thetadot);
	htonf(net_fdm->psidot);
	htonf(net_fdm->vcas);
	htonf(net_fdm->climb_rate);
	htonf(net_fdm->v_north);
	htonf(net_fdm->v_east);
	htonf(net_fdm->v_down);
	htonf(net_fdm->v_wind_body_north);
	htonf(net_fdm->v_wind_body_east);
	htonf(net_fdm->v_wind_body_down);

	htonf(net_fdm->A_X_pilot);
	htonf(net_fdm->A_Y_pilot);
	htonf(net_fdm->A_Z_pilot);

	htonf(net_fdm->stall_warning);
	htonf(net_fdm->slip_deg);

	net_fdm->num_engines = htonl(net_fdm->num_engines);
	for (int i = 0; i < net_fdm->num_engines; ++i) {
		net_fdm->eng_state[i] = htonl(net_fdm->eng_state[i]);
		htonf(net_fdm->rpm[i]);
		htonf(net_fdm->fuel_flow[i]);
		htonf(net_fdm->fuel_px[i]);
		htonf(net_fdm->egt[i]);
		htonf(net_fdm->cht[i]);
		htonf(net_fdm->mp_osi[i]);
		htonf(net_fdm->tit[i]);
		htonf(net_fdm->oil_temp[i]);
		htonf(net_fdm->oil_px[i]);
	}

	net_fdm->num_tanks = htonl(net_fdm->num_tanks);
	for (int i = 0; i < net_fdm->num_tanks; ++i) {
		htonf(net_fdm->fuel_quantity[i]);
	}

	net_fdm->num_wheels = htonl(net_fdm->num_wheels);
	for (int i = 0; i < net_fdm->num_wheels; ++i) {
		net_fdm->wow[i] = htonl(net_fdm->wow[i]);
		htonf(net_fdm->gear_pos[i]);
		htonf(net_fdm->gear_steer[i]);
		htonf(net_fdm->gear_compression[i]);
	}

	net_fdm->cur_time = htonl(net_fdm->cur_time);
	net_fdm->warp = ntohl(net_fdm->warp);
	htonf(net_fdm->visibility);

	htonf(net_fdm->elevator);
	htonf(net_fdm->elevator_trim_tab);
	htonf(net_fdm->left_flap);
	htonf(net_fdm->right_flap);
	htonf(net_fdm->left_aileron);
	htonf(net_fdm->right_aileron);
	htonf(net_fdm->rudder);
	htonf(net_fdm->nose_wheel);
	htonf(net_fdm->speedbrake);
	htonf(net_fdm->spoilers);

	return net_fdm;
}

int FDMUDPChannel::printErrorMessage(char const *msg) {
	cerr << "FDMUDPChannel: " << msg << endl;
	return -1;//initially it was exit(0). But that was terminating octave. Adil
}

void FDMUDPChannel::closeSocket() {
	close(sock);// I wonder why was this commented. This should have been open. Adil
	printf("UDP socket has been shut down.\n");
}

void FDMUDPChannel::shutdownSocket(){
	shutdown(sock, SHUT_RDWR);//Added by Adil to shutdown TCP socket.
	printf("TCP socket has been shut down.\n");
}

FDMUDPChannel::~FDMUDPChannel() {
	close(sock);
	//printf("FDMUDPChannel destroyed!\n");
}

void FDMUDPChannel::htonFGNetFDM(FGNetFDM *net_fdm){
	
	//Convert from network byte order to host byte order
	net_fdm->version = htonl(net_fdm->version);

	htond(net_fdm->longitude);
	htond(net_fdm->latitude);
	htond(net_fdm->altitude);
	htonf(net_fdm->agl);
	htonf(net_fdm->phi);
	htonf(net_fdm->theta);
	htonf(net_fdm->psi);
	htonf(net_fdm->alpha);
	htonf(net_fdm->beta);

	htonf(net_fdm->phidot);
	htonf(net_fdm->thetadot);
	htonf(net_fdm->psidot);
	htonf(net_fdm->vcas);
	htonf(net_fdm->climb_rate);
	htonf(net_fdm->v_north);
	htonf(net_fdm->v_east);
	htonf(net_fdm->v_down);
	htonf(net_fdm->v_wind_body_north);
	htonf(net_fdm->v_wind_body_east);
	htonf(net_fdm->v_wind_body_down);

	htonf(net_fdm->A_X_pilot);
	htonf(net_fdm->A_Y_pilot);
	htonf(net_fdm->A_Z_pilot);

	htonf(net_fdm->stall_warning);
	htonf(net_fdm->slip_deg);


	for (int i = 0; i < net_fdm->num_engines; ++i) {
		net_fdm->eng_state[i] = htonl(net_fdm->eng_state[i]);
		htonf(net_fdm->rpm[i]);
		htonf(net_fdm->fuel_flow[i]);
		htonf(net_fdm->fuel_px[i]);
		htonf(net_fdm->egt[i]);
		htonf(net_fdm->cht[i]);
		htonf(net_fdm->mp_osi[i]);
		htonf(net_fdm->tit[i]);
		htonf(net_fdm->oil_temp[i]);
		htonf(net_fdm->oil_px[i]);
	}
	net_fdm->num_engines = htonl(net_fdm->num_engines);


	for (int i = 0; i < net_fdm->num_tanks; ++i) {
		htonf(net_fdm->fuel_quantity[i]);
	}
	net_fdm->num_tanks = htonl(net_fdm->num_tanks);

	for (int i = 0; i < net_fdm->num_wheels; ++i) {
		net_fdm->wow[i] = htonl(net_fdm->wow[i]);
		htonf(net_fdm->gear_pos[i]);
		htonf(net_fdm->gear_steer[i]);
		htonf(net_fdm->gear_compression[i]);
	}
	net_fdm->num_wheels = htonl(net_fdm->num_wheels);

	net_fdm->cur_time = htonl(net_fdm->cur_time);
	net_fdm->warp = ntohl(net_fdm->warp);
	htonf(net_fdm->visibility);

	htonf(net_fdm->elevator);
	htonf(net_fdm->elevator_trim_tab);
	htonf(net_fdm->left_flap);
	htonf(net_fdm->right_flap);
	htonf(net_fdm->left_aileron);
	htonf(net_fdm->right_aileron);
	htonf(net_fdm->rudder);
	htonf(net_fdm->nose_wheel);
	htonf(net_fdm->speedbrake);
	htonf(net_fdm->spoilers);
}

void FDMUDPChannel::htonFGNetCtrls(FGNetCtrls *net_ctrls){
	net_ctrls->version = htonl(net_ctrls->version);
	htond(net_ctrls->aileron);
	htond(net_ctrls->elevator);	
	htond(net_ctrls->rudder);
    	htond(net_ctrls->aileron_trim);
	htond(net_ctrls->elevator_trim);
    	htond(net_ctrls->rudder_trim);
    	htond(net_ctrls->flaps);
    	htond(net_ctrls->spoilers);
    	htond(net_ctrls->speedbrake);
    	
	//Aero Control Faults
	net_ctrls->flaps_power = htonl(net_ctrls->flaps_power);
	net_ctrls->flap_motor_ok = htonl(net_ctrls->flap_motor_ok);
    
	// Engine controls
	net_ctrls->num_engines = htonl(net_ctrls->num_engines);
	for(int i=0; i<net_ctrls->FG_MAX_ENGINES; i++){
	    	net_ctrls->master_bat[i] = htonl(net_ctrls->master_bat[i]);
    		net_ctrls->master_alt[i] = htonl(net_ctrls->master_alt[i]);
    		net_ctrls->magnetos[i] = htonl(net_ctrls->magnetos[i]);
    		net_ctrls->starter_power[i] = htonl(net_ctrls->starter_power[i]);
    		htond(net_ctrls->throttle[i]);
    		htond(net_ctrls->mixture[i]);
    		htond(net_ctrls->condition[i]);
    		net_ctrls->fuel_pump_power[i] = htonl(net_ctrls->fuel_pump_power[i]);
    		htond(net_ctrls->prop_advance[i]);
	}

	for(int i=0; i<4; i++){
    		net_ctrls->feed_tank_to[i] = htonl(net_ctrls->feed_tank_to[i]);
    		net_ctrls->reverse[i] = htonl(net_ctrls->reverse[i]);
    	}
	// Engine faults
	for(int i=0; i<net_ctrls->FG_MAX_ENGINES; i++){
		net_ctrls->engine_ok[i] = htonl(net_ctrls->engine_ok[i]);
    		net_ctrls->mag_left_ok[i] = htonl(net_ctrls->mag_left_ok[i]);
    		net_ctrls->mag_right_ok[i] = htonl(net_ctrls->mag_right_ok[i]);
    		net_ctrls->spark_plugs_ok[i] = htonl(net_ctrls->spark_plugs_ok[i]);
    		net_ctrls->oil_press_status[i] = htonl(net_ctrls->oil_press_status[i]);
    		net_ctrls->fuel_pump_ok[i] = htonl(net_ctrls->fuel_pump_ok[i]);
    	}

	// Fuel management
	net_ctrls->num_tanks = htonl(net_ctrls->num_tanks);
	for(int i=0; i<net_ctrls->FG_MAX_TANKS; i++)
		net_ctrls->fuel_selector[i] = htonl(net_ctrls->fuel_selector[i]); 

	for(int i=0; i<5; i++)
    		net_ctrls->xfer_pump[i] = htonl(net_ctrls->xfer_pump[i]);
		
	net_ctrls->cross_feed = htonl(net_ctrls->cross_feed);

	// Brake controls
	htond(net_ctrls->brake_left);
    	htond(net_ctrls->brake_right);
    	htond(net_ctrls->copilot_brake_left);
    	htond(net_ctrls->copilot_brake_right);
    	htond(net_ctrls->brake_parking);    	
    
    	// Landing Gear
	net_ctrls->gear_handle = htonl(net_ctrls->gear_handle);
    
    	// Switches
	net_ctrls->master_avionics = htonl(net_ctrls->master_avionics);
    	    
        // nav and Comm
	htond(net_ctrls->comm_1);
    	htond(net_ctrls->comm_2);
    	htond(net_ctrls->nav_1);
    	htond(net_ctrls->nav_2);
    	
	// wind and turbulance
	htond(net_ctrls->wind_speed_kt);
	htond(net_ctrls->wind_dir_deg);
    	htond(net_ctrls->turbulence_norm);
    
    	// temp and pressure
	htond(net_ctrls->temp_c);
    	htond(net_ctrls->press_inhg);

	// other information about environment
	htond(net_ctrls->hground);
    	htond(net_ctrls->magvar);
    
    	// hazards
	net_ctrls->icing = htonl(net_ctrls->icing);
    	
    	// simulation control
	net_ctrls->speedup = htonl(net_ctrls->speedup);
	net_ctrls->freeze = htonl(net_ctrls->freeze);
    
}
