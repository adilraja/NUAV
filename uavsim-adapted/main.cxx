/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include <string>
#include <math.h>	
#include <cstdlib>	
#include <signal.h>	

#include "include/airvehicle.hxx"
#include "include/unmannedC172.hxx"

#include "include/Ipomdp/Grid_2D.hxx"

////////////////////Added To control UAV according to optimal policy/////////////////////
#include "include/Ipomdp/Policy.hxx"
#include "include/Ipomdp/AlphaVector.hxx"
#include "include/Ipomdp/Belief.hxx"
#include "include/Ipomdp/IpomdpAction.hxx"	//Just for use in main file
#include "include/Ipomdp/IpomdpObservation.hxx"	//Just for use in main file
///////////////////////////////////////////////////////

void flyPlane(int, char**,AlphaVectors*,Belief*,bool);
void getMyFlightData();
void getMultiplayerData(int, char**);
void printHelp();
void parseCommandlineArgs(int argc, char *argv[]);
void printFlightData(FGNetFDM fdm, FGNetCtrls ctrls);



char *IPAddress, *OutIPAddress=(char*)"127.0.0.1";	//Dummy Address Where agent sends position data to be received by other agents.
char *FDMIncomingPort = NULL;				//Receive FDM data from flightgear
char *CtrlOutgoingPort = NULL;				//Send control data to flightgear
char *MultiplayerIncomingPort = NULL;			//Receive flight data of the other agent. use to form observation// refer to observations in this file
char *MultiplayerOutgoingPort = (char*)"22250";		//Dummy Port
char *CallsignToWatch = NULL;				//Callsign of the other agent.
char *Callsign = (char *)"uga";				//Callsign of this agent
char *PolicyFile = NULL;				//Policy guiding the agent. Look at the format in the folder Policies
bool isComputerPilot = true;				//Autonomously controlled(according to optimal policy)/controlled by a human pilot

const float NORTH_HEADING = 0;				//All values in radians.
const float EAST_HEADING = 1.57;
const float SOUTH_HEADING = 3.14;
const float WEST_HEADING = 4.71;

IpomdpPolicy policy;					//Refer to file IpomdpPolicy.cxx // parser not very robust, maintain exact format, don't insert unnecessary newlines in policy files.

//-----------------------------Important--------------------------------//
Grid2D OpGrid(3, 3, 0.655, -2.137, 0.0020, 0.0002, 320);// No. of rows, No. of Col, Initial Lat(default 0.655), Initial Long(Default -2.137), Grid Size(Default 0.0015), Hover Leeway(Default 0.0002), Time for each action (default 300secs) for sync. Ideally equal to the longest time to perform any action on the slowest machine.
//----------------------------------------------------------------------//



int initial_gridLocation[2] = {0,0};
int final_gridLocation[2] = {2,2};

string AlphaVectorFile;
string BeliefFile;					//This and the one just above are used to select optimal policy.
string InitialGrid; // Format i,j
string FinalGrid; //Necessary as position of static target e.g. Safe sector for Fugitive
bool ObsOnline;	 //If the agent gets observation wrt a static target or moving target(another agent) from a file written by thread 0 read by other thread. Used for our problem formulation where fugitive's position is the intended final location for UAV

bool success=false;
IpomdpObservation random_observation[4]={SenseNorth,SenseSouth,SenseLevel,SenseFound};

double curr_lat;
double curr_long;
int *curr_location_int_arr;

IpomdpObservation obs_received;	//obs_received type casted to enum for our problem formulation, may use as int directly

void Tokenize(const string& str, vector<string>& tokens, const string& delimiters = " ")
{
    // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
   {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
         pos = str.find_first_of(delimiters, lastPos);
   }
}


void getOtherAgentLocation(){
	ifstream readFinalLocation;		//File from which other agent's data is to be read by this thread
	string flocation;
	vector<string> fin_grid_loc;

	fin_grid_loc.clear();	//Being on the safe side

	string filestr="AgentPosition/FinalLocation";
	filestr.append(CallsignToWatch);	//Make sure file exists
	filestr.append(".txt");
	readFinalLocation.open(filestr.c_str());
	while(!readFinalLocation.is_open()){//Keep trying till open
		readFinalLocation.open("include/FinalLocation.txt");
		cout<<"Couldn't open Observation file: Trying Again";
	}
	getline(readFinalLocation,flocation);
	readFinalLocation.close();

	
	if(!flocation.empty()){
		Tokenize(flocation,fin_grid_loc);
		double *OAPos = new double[2];
		OAPos[0]=atof(fin_grid_loc[0].c_str());
		OAPos[1]=atof(fin_grid_loc[1].c_str());
		int* OAGrid=OpGrid.getGridLocation(OAPos);
		final_gridLocation[0]=OAGrid[0];	//Target Location
		final_gridLocation[1]=OAGrid[1];
	}
	flocation.clear();
}

// expects in port, out port and IP address as parameters
int main(int argc, char *argv[]) {
	parseCommandlineArgs(argc, argv);

	policy = IpomdpPolicy(PolicyFile);	//File written or generated in format
	policy.printPolicy();

	
	cout<<endl;
	
	AlphaVectors *alpha = new AlphaVectors(AlphaVectorFile);	//File written or generated in format
	alpha->printAlphaVectors();
	cout<<endl;
	
	Belief bel(BeliefFile);	//File written or generated in format
	bel.printBelief();
	
	srand(time(0));
	
	pid_t pid = fork();
	if (pid == 0) {		//This thread Listens to get flight data from other agents and writes the position of other agents into a file that is read by the other thread
	  getMultiplayerData(argc,argv);
	} else {
		if (isComputerPilot) {		//Controls the respective agent
			flyPlane(argc,argv,alpha,&bel, false);
		} else {
			getMyFlightData();
		}
	}
              	
	return 1;
}

void flyPlane(int argc, char* argv[], AlphaVectors* alpha, Belief* bel, bool isStatic) {
	FGNetFDM *fdm;
	FGNetCtrls *ctrls = new FGNetCtrls();

	
	FDMUDPChannel net(atoi(FDMIncomingPort), atoi(CtrlOutgoingPort), 
			  inet_addr(IPAddress));
	/////To send flight data to the other agent(s) in multiagent settings/////
	MultiplayerUDPChannel mnet(atoi(MultiplayerOutgoingPort),OutIPAddress);
	//////////////////////////////////////////////////////////////////////////

	//Flying the Cessna C-172 aircraft
	UC172Control c172;

	////////////////////To be passed to another function///////////////////
	UC172Control *c172ptr = &c172;
	FDMUDPChannel *netptr;
	///////////////////////////////////////////////////////////////////////

	c172.initGeneralParams(ctrls);
	c172.initC172Params(ctrls);

	//cout<<" Version = "<<ctrls->version<<"\n\n\n\n\n\n\n\n\n";

	c172.setCruisingAGL(200.0);
	c172.setTargetHeading(INIT_HEADING);

	
	//Time action of flying to initial grid
	time_t start,end;
	time(&start);
	
	int ctr=0;
	while (c172.getTakeoffStatus() == false) {
		// Receive FDM information from the client
		fdm = net.getdata();
		
		curr_lat = fdm->latitude;
		curr_long = fdm->longitude;
		
		/*if(ctr%1000==0){	//Uncomment to print flight data continously
			printFlightData(*fdm, *ctrls);
		}*/

		// Send back controller information to the client
		net.senddata(*ctrls);	

		c172.takeoff(*fdm, ctrls);
		mnet.processFDM(*fdm, Callsign);		//Send flight data to other agent(s).

		ctr++;		
	}

	
	//////////////////// To reach starting Location/////////////
	cout<<endl<<"///////////Going to initial grid///////////////";
	cout<<endl;

	double t_lat = OpGrid.getCenterCoordinates(initial_gridLocation[0],initial_gridLocation[1])[0];
	double t_long= OpGrid.getCenterCoordinates(initial_gridLocation[0],initial_gridLocation[1])[1];

	curr_lat = fdm->latitude;
	curr_long = fdm->longitude;
	
	c172.flyFromXToY(argc, argv, &net, ctrls, &mnet, curr_lat, curr_long, t_lat, t_long, Callsign); //Fly to initial position. Don't need to start from runway, may start anywhere even from air. Check FlightGear manual.

	OpGrid.setCurrentGridLocation(initial_gridLocation);	// Since the plane has reached the specified initial grid.

	int* cLoc=OpGrid.getCurrentGridLocation();
	
	time(&end);
	while(difftime(end,start)<320){	//Synchronizing with other agents
		fdm = net.getdata();
		double cur_lat = fdm->latitude;
		double cur_long = fdm->longitude;
		
		double tar_lat = OpGrid.getCenterCoordinates(cLoc[0],cLoc[1])[0] + 0.0002;
		double tar_long = OpGrid.getCenterCoordinates(cLoc[0],cLoc[1])[1] + 0.0002;
	
		c172.flyFromXToY(argc, argv, &net, ctrls, &mnet, cur_lat, cur_long, tar_lat, tar_long, start, 400.00, Callsign);

		fdm = net.getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		tar_lat = OpGrid.getCenterCoordinates(cLoc[0],cLoc[1])[0] - 0.0002;
		tar_long = OpGrid.getCenterCoordinates(cLoc[0],cLoc[1])[1] - 0.0002;
	
		c172.flyFromXToY(argc, argv, &net, ctrls, &mnet, cur_lat, cur_long, tar_lat, tar_long, start, 400.00, Callsign);

		
		net.senddata(*ctrls);
		ctr++;
		time(&end);
		

	}

	int hor=0;
	vector<int> OptPol = alpha->getOptimalPolicy(bel->getBelief());	//All Optimal policies may have >1 policy giving same optimal value for the belief
	//policy.printPolicy();
	int PolIdx = rand()%OptPol.size();	//Randomly pick one optimal policy

	while(hor<policy.getHorizonCount()){
		hor++;
		fdm=net.getdata();
		curr_lat = fdm->latitude;
		curr_long = fdm->longitude;
		
		if(OpGrid.getCurrentGridLocation()[0]==final_gridLocation[0] && OpGrid.getCurrentGridLocation()[1]==final_gridLocation[1]){
			success=true;
			break;
		}
		
		cout<<endl<<endl<<"Acting according to belief";
		
		int act = policy.getCurrentAction(OptPol[PolIdx]);
		
		
		
		//According to our formulation, the observation is received only on Listen//
		//For other formulations, this part of code needs to be changed accordingly//
		switch(act){	//act typecasted to enum for our formulation. May use as int directly
		case MoveNorth: //Or case 0:
			cout<<endl<<"/////////////////////Moving North/////////////////////";

			cout<<endl<<"------------------------Current Grid Location----------------------------"<<endl;
			cout<<"------------------------------"<<OpGrid.getCurrentGridLocation()[0]<<","<<OpGrid.getCurrentGridLocation()[1] <<"-----------------------------------------"<<endl;
			cout<<"-------------------------------------------------------------------------"<<endl;

			time(&start);
			OpGrid.moveNorth(argc, argv, &c172, &net, ctrls, fdm, &mnet, Callsign);	//In file Grid2D.cxx
			time(&end);
			cout<<endl<< "Time Taken to Move North = "<<difftime(end,start);

			if(ObsOnline){
				getOtherAgentLocation();
			}

			obs_received=random_observation[rand()%4];	//Move actions give random obs. or obs_received=rand()%4
			cout<<endl<<"Moved North";
			break;
			
		case MoveSouth: //Or case 1:
			cout<<endl<<"/////////////////////Moving South/////////////////////";

			cout<<endl<<"------------------------Current Grid Location----------------------------"<<endl;
			cout<<"------------------------------"<<OpGrid.getCurrentGridLocation()[0]<<","<<OpGrid.getCurrentGridLocation()[1] <<"-----------------------------------------"<<endl;
			cout<<"-------------------------------------------------------------------------"<<endl;

			time(&start);
			OpGrid.moveSouth(argc, argv, &c172, &net, ctrls, fdm, &mnet, Callsign);	//In file Grid2D.cxx
			time(&end);
			cout<<endl<<"Time Taken to Move South = "<<difftime(end,start);
			if(ObsOnline){
				getOtherAgentLocation();
			}

			obs_received=random_observation[rand()%4];	//Move actions give random obs. or obs_received=rand()%4
			cout<<endl<<"Moved South";
			break;
			
		case MoveEast: //Or case 2:
			cout<<endl<<"/////////////////////Moving East/////////////////////";

			cout<<endl<<"------------------------Current Grid Location----------------------------"<<endl;
			cout<<"------------------------------"<<OpGrid.getCurrentGridLocation()[0]<<","<<OpGrid.getCurrentGridLocation()[1] <<"-----------------------------------------"<<endl;
			cout<<"-------------------------------------------------------------------------"<<endl;

			time(&start);
			OpGrid.moveEast(argc, argv, &c172, &net, ctrls, fdm, &mnet, Callsign);	//In file Grid2D.cxx
			time(&end);
			cout<<endl<<"Time Taken to Move East = "<<difftime(end,start);
			if(ObsOnline){
				getOtherAgentLocation();
			}

			obs_received=random_observation[rand()%4];	//Move actions give random obs. or obs_received=rand()%4
			cout<<endl<<"Moved East";
			break;
			
		case MoveWest: //Or case 3:
			cout<<endl<<"/////////////////////Moving West/////////////////////";

			cout<<endl<<"------------------------Current Grid Location----------------------------"<<endl;
			cout<<"------------------------------"<<OpGrid.getCurrentGridLocation()[0]<<","<<OpGrid.getCurrentGridLocation()[1] <<"-----------------------------------------"<<endl;
			cout<<"-------------------------------------------------------------------------"<<endl;

			time(&start);
			OpGrid.moveWest(argc, argv, &c172, &net, ctrls, fdm, &mnet, Callsign);	//In file Grid2D.cxx
			time(&end);
			cout<<endl<<"Time Taken to Move West = "<<difftime(end,start);
			if(ObsOnline){
				getOtherAgentLocation();
			}

			obs_received=random_observation[rand()%4];	//Move actions give random obs. or obs_received=rand()%4
			cout<<endl<<"Moved West";
			break;

		case Listen: //or case: 4
			cout<<endl<<"/////////////////////Listening/////////////////////";

			cout<<endl<<"------------------------Current Grid Location----------------------------"<<endl;
			cout<<"------------------------------"<<OpGrid.getCurrentGridLocation()[0]<<","<<OpGrid.getCurrentGridLocation()[1] <<"-----------------------------------------"<<endl;
			cout<<"-------------------------------------------------------------------------"<<endl;

			time(&start);
			OpGrid.hover(argc, argv, &c172, &net, ctrls, fdm, &mnet, Callsign);	//In file Grid2D.cxx
			time(&end);
			cout<<endl<<"Time Taken to Listen = "<<difftime(end,start);
			if(ObsOnline){
				getOtherAgentLocation();
			}
			fdm=net.getdata();
	
			curr_lat = fdm->latitude;
			curr_long = fdm->longitude;
			
			if(OpGrid.getCurrentGridLocation()[0]<final_gridLocation[0]){
				obs_received=SenseNorth;	//or obs_received=0;
				cout<<endl<<"Observation Received: Sense North ";	//obs_received type casted to enum for our problem formulation, may use as int directly
			}
			else if(OpGrid.getCurrentGridLocation()[0]>final_gridLocation[0]){
				obs_received=SenseSouth;	//or obs_received=1;
				cout<<endl<<"Observation Received: Sense South ";
			}
			else if(OpGrid.getCurrentGridLocation()[0]==final_gridLocation[0] && OpGrid.getCurrentGridLocation()[1]!=final_gridLocation[1]){
				obs_received=SenseLevel;	//or obs_received=2;
				cout<<endl<<"Observation Received: Sense Level ";
			}

			else{
				obs_received=SenseFound;	//or obs_received=3;
				cout<<endl<<"Observation Received: Sense Found ";
			}
			break;
		}
		policy.next(obs_received, OptPol[PolIdx]);

		cout<<endl<<"Time Step "<<hor<<" complete \n";
		
		
	}
	fdm=net.getdata();
	curr_lat = fdm->latitude;
	curr_long = fdm->longitude;

	if(OpGrid.getCurrentGridLocation()[0]==final_gridLocation[0] && OpGrid.getCurrentGridLocation()[1]==final_gridLocation[1]){
		success=true;
		cout<<endl<<"Mission Successul: May Exit FlightGear and Press Ctrl-C to quit\n";
	}

	else{
		cout<<endl<<"Mission Failed: May Exit FlightGear and Press Ctrl-C to quit\n";
	}
	int counter = 1;
	while (1) {
		// Receive FDM information from the client
		fdm = net.getdata();

		curr_lat = fdm->latitude;
		curr_long = fdm->longitude;
		
		/*if (counter % 1000 == 0) {
			printFlightData(*fdm, *ctrls);
			if(success)
				cout<<endl<<"Mission Accomplished\n";
			else
				cout<<endl<<"Mission Failed\n";
		}*/

		if (counter % 20 == 0) {
			c172.turn_left(0.5);
		}

		c172.fly_straight(*fdm, ctrls);
		
		// Send back controller information to the client
		net.senddata(*ctrls);
		mnet.processFDM(*fdm, Callsign);

		counter++;
	}
}


void getMyFlightData() {
	FGNetFDM *fdm;
	FGNetCtrls *ctrls = new FGNetCtrls();

	FDMUDPChannel net(atoi(FDMIncomingPort), atoi(CtrlOutgoingPort),
			inet_addr(IPAddress));

	while (1) {
		// Receive FDM information from the client
		fdm = net.getdata();
		printFlightData(*fdm, *ctrls);
	}
}

void getMultiplayerData(int argc, char* argv[]) {
	MultiplayerUDPChannel net(atoi(MultiplayerIncomingPort));
	cout << "multi-in socket initialized" << endl;
	int bytes;
	while(1) {
		net.fowardData(CallsignToWatch);
		//signal(SIGINT, terminate);
	}
		
}


void parseCommandlineArgs(int argc, char *argv[]) {
	if ( argc < 15 ) {
		cout << "Invalid number of arguements (" << argc << ")";
		printHelp();
		exit(1);
	}

	cout << "Starting uavsim with options:";
	int count;
	for (count = 1; count < argc; count++) {
		if (!strcmp(argv[count], "-ipaddress")) {
			count++;
			IPAddress = (char*)malloc(strlen(argv[count]));
			strcpy(IPAddress, argv[count]);			
			cout << " IPAddress = " << IPAddress;
		} else if (!strcmp(argv[count], "-out-ipaddress")) {
			count++;
			OutIPAddress = (char*)malloc(strlen(argv[count]));
			strcpy(OutIPAddress, argv[count]);			
			cout << " OutIPAddress = " << OutIPAddress;
		} else if (!strcmp(argv[count], "-ctrl-out")) {
			count++;
			CtrlOutgoingPort = (char*)malloc(strlen(argv[count]));
			strcpy(CtrlOutgoingPort, argv[count]);
			cout << " Control Outgoing Port = " << CtrlOutgoingPort;
		} else if (!strcmp(argv[count], "-fdm-in")) {
			count++;
			FDMIncomingPort = (char*)malloc(strlen(argv[count]));
			strcpy(FDMIncomingPort, argv[count]);
			cout << " FDM Incoming Port = " << FDMIncomingPort;
		} else if (!strcmp(argv[count], "-multi-in")) {
			count++;
			MultiplayerIncomingPort = (char*)malloc(strlen(argv[count]));
			strcpy(MultiplayerIncomingPort, argv[count]);
			cout << " Multiplayer Incoming Port = " << MultiplayerIncomingPort;
		}else if (!strcmp(argv[count], "-multi-outport")) {
			count++;
			MultiplayerOutgoingPort = (char*)malloc(strlen(argv[count]));
			strcpy(MultiplayerOutgoingPort, argv[count]);
			cout << " Multiplayer Outgoing Port = " << MultiplayerOutgoingPort;
		} else if (!strcmp(argv[count], "-callsign-to-watch")) {
			count++;
			CallsignToWatch = (char*)malloc(strlen(argv[count]));
			strcpy(CallsignToWatch, argv[count]);
			cout << " Callsign To Watch = " << CallsignToWatch;
		} else if (!strcmp(argv[count], "-callsign")) {
			count++;
			Callsign = (char*)malloc(strlen(argv[count]));
			strcpy(Callsign, argv[count]);
			cout << " Callsign  = " << Callsign;
		} else if (!strcmp(argv[count], "-policy-file")) {
			count++;
			PolicyFile = (char*)malloc(strlen(argv[count]));
			strcpy(PolicyFile, argv[count]);
			cout << " Policy File = " << PolicyFile;
		} else if (!strcmp(argv[count], "-pilot")) {
			count++;
			if (!strcmp(argv[count], "h")) {
				isComputerPilot = false;
				cout << " pilot = human";
			} else {
				cout << " pilot = computer";
			}
		} else if (!strcmp(argv[count], "-alpha-vector-file")) {
			count++;
			AlphaVectorFile = argv[count];
			cout << " Alpha Vector File = " << AlphaVectorFile;
		} else if (!strcmp(argv[count], "-belief-file")) {
			count++;
			BeliefFile =  argv[count];
			cout << " Belief File = " << BeliefFile;
		} else if (!strcmp(argv[count], "-initial-grid")) {
			count++;
			InitialGrid =  argv[count];
			cout << " Initial Grid = " << InitialGrid;
			vector<string> gridvect;
			Tokenize(InitialGrid, gridvect, ",");
			initial_gridLocation[0]=atoi(gridvect[0].c_str());
			initial_gridLocation[1]=atoi(gridvect[1].c_str());
		} else if (!strcmp(argv[count], "-final-grid")) {
			count++;
			FinalGrid =  argv[count];
			cout << " Final Grid = " << FinalGrid;
			vector<string> gridvect;
			Tokenize(FinalGrid, gridvect, ",");
			final_gridLocation[0]=atoi(gridvect[0].c_str());
			final_gridLocation[1]=atoi(gridvect[1].c_str());
		} else if (!strcmp(argv[count], "-get-obs-online")) {
			count++;
			if(!strcmp(argv[count],"true"))
				ObsOnline=true;
			cout << " Get Observation Online = " << ObsOnline;
		}
		

	}
	cout << endl;
}

void printFlightData(FGNetFDM fdm, FGNetCtrls ctrls) {	//Example to show how to get filght parameters
	fprintf(stdout,
	
	"speed: %-3.3f \
 [agl: %-3.3f pitch: %-3.3f elevator: %-3.3f climb: %-3.3f] \
 [yaw: %-3.3f rudder: %-3.3f roll: %-3.3f aileron: %-3.3f]\n",
	fdm.vcas,
	fdm.agl, fdm.theta, ctrls.elevator, fdm.climb_rate,
	fdm.psi, ctrls.rudder, fdm.phi, ctrls.aileron);
	
		cout << "me:   ";
		
		cout << "Pos(" 
			<< fdm.latitude << "," 
			<< fdm.longitude << "," 
			<< fdm.altitude << ") ";
		
		cout << "Grid(" << OpGrid.getCurrentGridLocation()[0]<<","<<OpGrid.getCurrentGridLocation()[1] << ") "; 
		
		cout << "Orient("
			<< fdm.psi << ","
			<< fdm.theta << ","
			<< fdm.phi << ")"<<endl;
}


void printHelp() {
	cout << endl;
	cout << "\n"
		"arguments are:\n"
		"-ipaddress  IPADDR  ip address of this machine\n"
		"-ctrl-out    PORT    the fdm outgoing port\n"
		"-fdm-in     PORT    the fdm incoming port\n"
		"-multi-in   PORT    the multiplayer incoming port\n"
		"-pilot      [h/c]   h - human pilot, c - computer pilot\n"
		"-callsign-to-watch CALLSIGN the callsign of the plane to observe\n"
		"-print-delay [true/false] whether to print fast or slow"
		"\n"
		"All options are required to run a full UAV.\n"
		"\n";
	exit(0);
}
