#include "include/unmannedC172.hxx"

void UC172Control :: initC172Params(FGNetCtrls* ctrls) {
  // initialize engines
  ctrls->num_engines = 2;
  for (int engineIndex = 0; engineIndex < ctrls->num_engines; engineIndex++){
    ctrls->master_bat[engineIndex] = 1;
    ctrls->master_alt[engineIndex] = 1;
    ctrls->magnetos[engineIndex] = 1;
    ctrls->starter_power[engineIndex] = 1;
    ctrls->throttle[engineIndex] = 0;
    ctrls->mixture[engineIndex]=1;
    ctrls->condition[engineIndex]=1;
    ctrls->fuel_pump_power[engineIndex] = 1;
    ctrls->prop_advance[engineIndex] = 1;
    ctrls->feed_tank_to[engineIndex] = 1;
    ctrls->reverse[engineIndex] = 0;
    
    ctrls->engine_ok[engineIndex] = 1;
    ctrls->engine_ok[engineIndex] = 1;
    ctrls->mag_right_ok[engineIndex] = 1;
    ctrls->mag_left_ok[engineIndex] = 1;
    ctrls->spark_plugs_ok[engineIndex] = 1;
    ctrls->oil_press_status[engineIndex] = 1;
    ctrls->fuel_pump_ok[engineIndex] = 1;
  }

  // initialize fuel tanks
  ctrls->num_tanks = 1;
  for(int tankIndex = 0; tankIndex < ctrls->num_tanks; tankIndex++) {
      ctrls->fuel_selector[tankIndex] = 1;
      ctrls->xfer_pump[tankIndex] = 1;
  }

  // initialize brakes
  ctrls->brake_left = 0;
  ctrls->brake_right = 0;
  ctrls->copilot_brake_left = 0;
  ctrls->copilot_brake_right = 0;
  ctrls->brake_parking = 0;

  // initialize take off status
  takeoffStatus = false;
}

void UC172Control :: takeoff(FGNetFDM fdm, FGNetCtrls *ctrls) {
  // Increase/decrease throttle to stay within the min/max speeds
  if(fdm.vcas < MIN_FLY_SPEED) {
    for(int engineIndex = 0; engineIndex < ctrls->num_engines; engineIndex++) {
      ctrls->throttle[engineIndex] = ctrls->throttle[engineIndex] + 0.03;
    }
  } else if(fdm.vcas > MAX_FLY_SPEED) { //Decrease throttle if needed
    for(int engineIndex = 0; engineIndex < ctrls->num_engines; engineIndex++) {
      ctrls->throttle[engineIndex] = ctrls->throttle[engineIndex] - 0.03;
    }
  }

  // keep the aircraft from turning using the rudder
  if (fdm.agl < ABOVE_GROUND_AGL) {
    if (fdm.vcas < 50) {
      ctrls->rudder = ((INIT_HEADING - fdm.psi) * 8);
    } else {    
      ctrls->rudder = ((INIT_HEADING - fdm.psi) * 4);
    }
  } else {
    ctrls->rudder = ((INIT_HEADING - fdm.psi) * 20);
  }
  
  // keep the aircraft from banking using the ailerons      
  if(fdm.agl > ABOVE_GROUND_AGL) {
    ctrls->aileron = getAileronAdjustment_FlyStraight(fdm, ctrls);
  }

  // adjust the elevators to keep the plane ascending
  if (fdm.agl > 20.0) {
    ctrls->elevator = (fdm.climb_rate - OPTIMAL_CLIMB_RATE) / 100;
  }
  
  // the aircrafts has taken off
  if(fdm.agl >= cruisingAGL) {
    takeoffStatus = true;
  }
}
   
void UC172Control :: fly_straight(FGNetFDM fdm, FGNetCtrls* ctrls) {
  //Keep throttle between the max and min range
  if(fdm.vcas < MIN_FLY_SPEED) {
    for(int engineIndex = 0; engineIndex < ctrls->num_engines; engineIndex++) {
      ctrls->throttle[engineIndex] = ctrls->throttle[engineIndex] + 0.01;
    }
  } else {
    if(fdm.vcas > MAX_FLY_SPEED) {
      for(int engineIndex = 0; engineIndex < ctrls->num_engines; engineIndex++) {
        ctrls->throttle[engineIndex] = ctrls->throttle[engineIndex] - 0.01;
      }
    }
  }

  ctrls->rudder = 0.0;
  
  ctrls->aileron = getAileronAdjustment_FlyStraight(fdm, ctrls);

  ctrls->elevator = getElevatorAdjustment_FlyStraight(fdm, ctrls);
}

double UC172Control :: getAileronAdjustment_FlyStraight(FGNetFDM fdm, FGNetCtrls* ctrls) {

  /** Find the targetRoll **/
  double MAX_TARGET_ROLL = 0.349066; // in radians (= 20 degrees)
  // HEADING_ERROR_FOR_MAX_ROLL: if our heading error is greater than this many
  //  radians, we will perform a max roll.
  double HEADING_ERROR_FOR_MAX_ROLL = 0.349066; // in radians (= 20 degrees)
  double targetRoll; // the roll amount we want to adjust our heading
                                                
  double headingError = targetHeading - fdm.psi;
  // normalize headingError so it has the range [-pi radians, pi radians] (a half circle)
  if (headingError > HALF_RADIAN_CIRCLE) {
    headingError -= FULL_RADIAN_CIRCLE;
  } else if (headingError <= -HALF_RADIAN_CIRCLE) {
    headingError += FULL_RADIAN_CIRCLE;
  }
  
  if (fabs(headingError) > HEADING_ERROR_FOR_MAX_ROLL) {
    if (headingError < 0) {
      targetRoll = -MAX_TARGET_ROLL;
    } else {
      targetRoll = MAX_TARGET_ROLL;
    }
  } else {
    // We create a line from the points (-HEADING_ERROR_FOR_MAX_ROLL, -MAX_TARGET_ROLL) to
    //  (HEADING_ERROR_FOR_MAX_ROLL, MAX_TARGET_ROLL). We then have y = m*x + b where y is
    //  our target roll, m is the slope of the line, x is our current heading error, and b
    //  is a constant. Now we can solve for y to get our targetRoll.
    double slope = (MAX_TARGET_ROLL + MAX_TARGET_ROLL) / (HEADING_ERROR_FOR_MAX_ROLL + HEADING_ERROR_FOR_MAX_ROLL);
    double b = MAX_TARGET_ROLL - slope * HEADING_ERROR_FOR_MAX_ROLL;

    targetRoll = slope * headingError + b;
  }

  /** Find the aileronValue **/
  // ROLL_ERROR_FOR_SMOOTH_ROLL: if our roll error is greater than this many
  //  radians, we will adjust our ailerons by a maximum amount.
  double ROLL_ERROR_FOR_SMOOTH_ROLL = 0.174533; // in radians (= 10 degrees)
  double MAX_AILERON = 0.2; // the largest amount (+/-) we want to adjust the ailerons by.

  double aileronValue = 0.0;  
  double rollError = targetRoll - fdm.phi;

  if (fabs(rollError) > ROLL_ERROR_FOR_SMOOTH_ROLL) {
    if (rollError < 0) {
      aileronValue = -MAX_AILERON;
    } else {
      aileronValue = MAX_AILERON;
    }
  } else {
    // We create a line from the points (-ROLL_ERROR_FOR_SMOOTH_ROLL, -MAX_AILERON) to
    //  (ROLL_ERROR_FOR_SMOOTH_ROLL, MAX_AILERON). We then have y = m*x + b where y is
    //  our aileron value, m is the slope of the line, x is our current roll error, and b
    //  is a constant. Now we can solve for y to get our aileron value.
    double slope = (MAX_AILERON + MAX_AILERON) / (ROLL_ERROR_FOR_SMOOTH_ROLL + ROLL_ERROR_FOR_SMOOTH_ROLL);
    double b = MAX_AILERON - slope * ROLL_ERROR_FOR_SMOOTH_ROLL;

    aileronValue = slope * rollError + b;
  }

  return aileronValue;
}

double UC172Control :: getElevatorAdjustment_FlyStraight(FGNetFDM fdm, FGNetCtrls* ctrls) {

  double aglError = cruisingAGL - fdm.agl;

  // if the current agl is within a few feet of our goal, don't
  //  correct the plane as harshly.
  if (fabs(aglError) < TARGET_AGL_ERROR_ALLOWANCE) {
    aglError = aglError / 4.0;
  }
  
  double targetClimbRate = aglError * 8.0;

  double maxClimb = OPTIMAL_CLIMB_RATE;
  double speed = fdm.vcas;

  // do not exceed maximum or minimum climb rate
  if (targetClimbRate > maxClimb ) {
      targetClimbRate = maxClimb;
  } else if (targetClimbRate < -maxClimb) {
      targetClimbRate = -maxClimb;
  }

  double climbRateError = fdm.climb_rate - targetClimbRate;

  /** Calculate Adjustment **/
  double totalAdjustment = climbRateError / 35.0;
  
  if (totalAdjustment > 0.6) {
    totalAdjustment = 0.6;
  }
  else if (totalAdjustment < -0.2) {
    totalAdjustment = -0.2;
  }
  
  return totalAdjustment;
}

/* turn_left(radiansTurnAmount)
 * Calling turn_left will change the heading by the specified radians.
 * Unlike fly_straight and takeoff, this is only to be called once.
 * radiansTurnAmount: must be within [-pi radians, pi radians] ([~-3.14, ~3.14])
 */
void UC172Control :: turn_left(float radiansTurnAmount) {
  float newHeading = targetHeading - radiansTurnAmount;
  if (newHeading < 0) {
    newHeading += FULL_RADIAN_CIRCLE;
  }
  targetHeading = newHeading;
}

/* turn_right(radiansTurnAmount)
 * Calling turn_right will change the heading by the specified radians.
 * Unlike fly_straight and takeoff, this is only to be called once.
 * radiansTurnAmount: must be within [-pi radians, pi radians] ([~-3.14, ~3.14])
 */
void UC172Control :: turn_right(float radiansTurnAmount) {
  float newHeading = targetHeading + radiansTurnAmount;
  if (newHeading > FULL_RADIAN_CIRCLE) {
    newHeading -= FULL_RADIAN_CIRCLE;
  }
  targetHeading = newHeading;
}

void UC172Control :: descend_by(float aglAdjustmentAmount) {
  cruisingAGL -= aglAdjustmentAmount;
}

void UC172Control :: ascend_by(float aglAdjustmentAmount) {
  cruisingAGL += aglAdjustmentAmount;
}

void UC172Control :: stop(FGNetFDM fdm, FGNetCtrls* ctrls) {
}

void UC172Control :: setTakeoffStatus(bool status) {
  takeoffStatus = status;
}

bool UC172Control :: getTakeoffStatus() {
  return takeoffStatus;
}

void UC172Control :: setCruisingAGL(float agl) {
  cruisingAGL = agl;
}

float UC172Control :: getCruisingAGL() {
  return cruisingAGL;
}

void UC172Control :: setTargetHeading(float heading) {
  targetHeading = heading;
}

float UC172Control :: getTargetHeading() {
  return targetHeading;
}

  
UC172Control :: ~UC172Control() {
  //delete ctrls;
}
