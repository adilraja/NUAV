In order to run NUAV please follow these steps:
1) Run all the commands in Software-Installation/Installation_Commands.txt. You will only have to run them once in order to have NUAV set up on your system.
2) After this, run the commands in uavsim-adapted/Readme-Adil.txt. You may not have to run all of them depending on your OS. This basically is about compiling and linking for creation of oct-files required to connect NUAV with FGFS. Compilation has already been done on Ubuntu. But if you are using any other OS, you may have to do the linking again.
3) Start FGFS. Run fg-local-start-nuav from the folder NUAV/uavsim-adapted
4) Run a command from OctaveCode/commands.txt e.g.:
[all_results, chromosome, NET] = nuav_testbed(150, 100, 50, 1, 10, 30, 0.25, [17 4 3 4], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17], "computeFitnessForNavigation", "127.0.0.1", 22222, 22223, 22224);

If you use this code in your research, please cite the following in your articles:

Habib, Saleh, et al. "NUAV-a testbed for developing autonomous Unmanned Aerial Vehicles." Communication, Computing and Digital Systems (C-CODE), International Conference on. IEEE, 2017.

@inproceedings{habib2017nuav,
  title={NUAV-- A Testbed For Developing Autonomous Unmanned Aerial Vehicles},
  author={Habib, Saleh and Malik, Mahgul and Rahman, Shams Ur and Raja, Muhammad Adil},
  booktitle={Communication, Computing and Digital Systems (C-CODE), International Conference on},
  pages={185--192},
  year={2017},
  organization={IEEE}
}

http://ieeexplore.ieee.org/document/7918926/

AND

@INPROCEEDINGS{8289450, 
author={M. A. Raja and S. U. Rahman}, 
booktitle={2017 International Multi-topic Conference (INMIC)}, 
title={A tutorial on simulating unmanned aerial vehicles}, 
year={2017}, 
volume={}, 
number={}, 
pages={1-6}, 
keywords={aerospace simulation;aircraft control;autonomous aerial vehicles;learning (artificial intelligence);tutorial;flight simulator;machine learning algorithm;UAV simulation;unmanned aerial vehicles simulation;Aircraft;Atmospheric modeling;Matlab;Open source software;Aerodynamics;Force;Evolutionary Computation;Genetic Algorithms;Machine Learning}, 
doi={10.1109/INMIC.2017.8289450}, 
ISSN={}, 
month={Nov},}

If you use the deep learning neural networks toolbox (provided with the code) in your research please cite [Prediction as a candidate for learning deep hierarchical models of data](http://www2.imm.dtu.dk/pubdb/views/publication_details.php?id=6284)

```
@MASTERSTHESIS\{IMM2012-06284,
    author       = "R. B. Palm",
    title        = "Prediction as a candidate for learning deep hierarchical models of data",
    year         = "2012",
}
```

Please contact the following if you have any problems:

salehchampion119@gmail.com, mahgulmalik95@gmail.com, shams0333@gmail.com, adilraja@gmail.com



