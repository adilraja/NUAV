## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} ann_tree (@var{input1}, @var{input2})
## This function creates a tree of ANNs
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-06-20

function NET = ann_tree (level=0, architecture=[3 4 5])

if(level>0)%Random architectures for not-root networks
  architecture=[intrand(2, 5, 1, intrand(2, 3, 1, 1)) 1];%use intrand function from GPLab toolbox
endif
NET=nnsetup(architecture);%Create the structure for this network.
NET.depth=level;

if(level<6)
  for(i=1:architecture(1))
    NET.child{i}=ann_tree(level+1);%Specify the depth for the next level of neural network
  endfor
endif
endfunction
