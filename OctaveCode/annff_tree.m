## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} annff_tree (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-06-20

function NET = annff_tree (NET, x, y)

%If you have hit a neural network at a leaf node, just evaluate that and come out of the loop.

if(~isfield(NET, "child"))
%Choose a random set of input variables, the size of which is equal to the 
%number of neurons in the input layer of any terminal neural network
  NET.input_indices=[intrand(1, length(x), 1, NET.size(1))];%use intrand function from GPLab toolbox
  %x contains only those input indices that are chosen in the previous step
  NET=nnff(NET, x(NET.input_indices), y);%y is dummy, 
  return;
else%Otherwise invoke this function for the non-terminal child nodes
  for(i=1:length(NET.child))
    NET.child{i}=annff_tree(NET.child{i}, x, y);
  endfor
endif
input=[];
for(i=1:length(NET.child))
  input(i)=NET.child{i}.a{NET.child{i}.n};  
end
NET=nnff(NET,input, y);
  
endfunction
