## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} nuavPlots (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-08-03

function [retval] = nuavPlots (net_fdm_for_one_sim, net_ctrls_for_one_sim, sim_time)

altitude=[];
longitude=[];
latitude=[];

phi=[];%Roll
psi=[];%Yaw
theta=[];%Pitch

vcas=[];
climb_rate=[];

loop_size=length(net_fdm_for_one_sim);
time_t=0.25:0.25:sim_time;

for(i=1:length(net_fdm_for_one_sim))
  net_fdm_one=net_fdm_for_one_sim(i);
  phi=[phi; net_fdm_one.phi];
  psi=[psi; net_fdm_one.psi];
  theta=[theta; net_fdm_one.theta];
  altitude=[altitude; net_fdm_one.altitude];
  longitude=[longitude; net_fdm_one.longitude];
  latitude=[latitude; net_fdm_one.latitude];
  climb_rate=[climb_rate; net_fdm_one.climb_rate];
  vcas=[vcas; net_fdm_one.vcas];
endfor

phi=phi*180/3.141592654;
psi=psi*180/3.141592654-180;
theta=theta*180/3.141592654;
altitude1=altitude/25;%Normalize

disp(max(theta));

plot(time_t, phi(1:length(time_t)), "linewidth", 1);
hold on;
%plot(psi, '--g');
%hold on;
plot(time_t, theta(1:length(time_t)), '-.r', "linewidth", 1.5);
%hold on;
plot(time_t, altitude1(1:length(time_t)), '--g', "linewidth", 1.5);

hold off;


grid on;

cfh(1)=gcf();
xlabel("Time (seconds)");
ylabel("Degrees");
legend("Roll","Pitch","Altitude");

figure;

plot3(longitude(1:length(time_t)), latitude(1:length(time_t)), altitude(1:length(time_t)));
hold on;
%quiver3(longitude(40), latitude(40), altitude(40), longitude(41), latitude(41), altitude(41), 'r');
xlabel("Longitude (degrees)");
ylabel("Latitude (degrees)");
zlabel("Altitude (ft)");

grid on;

disp(min([longitude latitude altitude]));
disp(max([longitude latitude altitude]));

cfh(2)=gcf();

figure;

plot(time_t, climb_rate(1:length(time_t)), "linewidth", 1);
xlabel("Time (seconds)");
ylabel("Climb Rate (feet per second)");

grid on;

cfh(3)=gcf();

figure;

plot(time_t, vcas(1:length(time_t)), "linewidth", 1);
xlabel("Time (seconds)");
ylabel("Calibrated Air Speed (knots)");

grid on;

cfh(4)=gcf();

retval=cfh;

%plot(Euclidean_orientation);

endfunction
