## Copyright (C) 2016 FGFS
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} evaluateGAPopWithFGFS (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: FGFS <fgfs@fgfs-Precision-WorkStation-T3500>
## Created: 2016-08-02

function [test_results] = evaluateGAPopWithFGFS (pop, network, genome_size, simulation_time, interval_length, FGIncomingPort, FGOutgoingPort, fg_ip_address)
pop_size=length(pop(:,1));

net_ctrls_for_all_sims=[];
net_fdm_for_all_sims=[];
for(i=1:pop_size)
  [net_ctrls_for_one_sim, net_fdm_for_one_sim] =evaluateANNWithFGFS (network, pop(i,1:genome_size), simulation_time, interval_length, FGIncomingPort, FGOutgoingPort, fg_ip_address);
  net_ctrls_for_all_sims=[net_ctrls_for_all_sims; net_ctrls_for_one_sim];
  net_fdm_for_all_sims=[net_fdm_for_all_sims; net_fdm_for_one_sim];
endfor

test_results.NET=network;
test_results.chromosome=pop;
test_results.net_fdm_for_all_sims =net_fdm_for_all_sims ;
test_results.net_ctrls_for_all_sims =net_ctrls_for_all_sims;

results_file=strcat("TestResults-", datestr(now(), 30), ".mat");

save(results_file, "test_results" );

endfunction
