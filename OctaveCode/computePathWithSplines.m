## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} computePathWithSplines (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-08-28

function [xq, yq, cf] = computePathWithSplines (path)
t = [0 1 2 3 4 5];
points = [0.72 0.03; 1 0.03; 1 0.065; 0.72 0.065; 0.72 0.1; 1 0.1];
x = points(:,1);
y = points(:,2);

plot(x,y,x,y,'o');
axis([0.6 1.1 0.02 0.11]);
xlabel("Latitude (deg)");
ylabel("Longitude (deg)");
grid on;
cf(1)=gcf();

tq = 0:0.1:5;
slope0 = 0;
slopeF = 0;
xq = spline(t,[slope0; x; slopeF],tq);
yq = spline(t,[slope0; y; slopeF],tq);

figure;
plot(x,y,'o',xq,yq,':.');
axis([0.6 1.1 0.02 0.11]);
xlabel("Latitude (deg)");
ylabel("Longitude (deg)");
grid on;
%title('y vs. x');
cf(2)=gcf();

endfunction
