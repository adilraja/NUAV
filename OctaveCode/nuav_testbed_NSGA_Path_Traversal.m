## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} nuav_testbed (@var{pop}, @var{gen}, @var{architecture})
##
## This is the main file for NUAV testbed. Basically what it does is that it integrates 
## FlightGear with a machine learning algorithm. To this end, it has the ability to fetch and send data to FLightGear.
## Moreover, it can manipulate that data using a machine learning algorithm. In a very basic scheme what we have done
## is to create a neural network structure using a deep learning neural network toolbox.
## The toolbox can be fetched from here, http://www.mathworks.com/matlabcentral/fileexchange/38310-deep-learning-toolbox 
## The weights of the structure are adapted using NSGA-II algorithm.
## The algorithm performs a static, online training of the control laws for a 
## fixed wing UAV. However, the code can be adapted for any type of a UAV, 
## for any scenario using any combination of ML algorithms. Our goal is to make this function more plug and play.
## 
##Inputs: Since the function calls NSGA-2, it should be provided with:
##  pop: population size of the GA.
##  gen: number of generations for which the NSGA-2 will run.
## Epochs: It is some measure of time intervals for which a single simulation involving a plane has to be run. These are basically time slices.
## For a simulation lasting 30 seconds, for example, epochs could be 30/1=30, or 30/5=6
## This basically means that for a simulation lasting 30 seconds either the state shall have to be sampled 30 times, or 6 times respectively.
##  architecture: Specify the architecture of the network
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-06-16

function [all_results, chromosome, NET] = nuav_testbed_NSGA_Path_Traversal (init_pop_size=200, final_pop_size=20, gen=200, start_simulation_time=1, intermediate_simulation_time=10, end_simulation_time=30, interval_length=0.015, architecture=[17 4 3 4], fg_ip_address="192.168.0.28", FGIncomingPort=22222, FGOutgoingPort=22223, FGTelnetPort=22224, cost_function='computeFitnessForNavigation', start_conds, target_vals)

%First things go first. So first of all we create a neural network structure using the deep learning toolbox.
%But before doing that please add the path to the deep learning neural network toolbox using 
%the following commands on your octave command prompt.
% octave:1> addpath(genpath("/home/mike/mfiles"))
%octave:2> savepath

%Specify an architecture for the network. This should be an array containing the number of neurons in each of the layers.
%For example, architecture=[4 3 5] has 4 neurons in the input layer, 3 in the hidden layer
%and 5 in the output layer. So,
if(nargin<4)
  architecture=[17 4 3 4];
endif
 
%Before we do anything else lets create a command through which we are going to reset the potition of our plane everytime we want to test a solution.

%python_script=cstrcat("python set_fgfs_properties_through_telnet.py ", fg_ip_address," ", num2str(FGTelnetPort)," ", num2str(altitude_ft), " ", num2str(latitude), " ", num2str(longitude));

epochs=round(start_simulation_time/interval_length);

%The number of neurons in the input layer should be typically equal to the number of input parameters
%(or variables) and the number of neurons in the output layer should be equal to the 
% number of neurons in the output layer.

%Now lets create the neural network
NET=nnsetup(architecture);

%One thing that we really need to know is that how many weights and biases are there in this network structure.
%We need to know this because we are going to initialize NSGA-II accordingly.

numweights = nnnumweights(NET);

%Once this has been done, its weighths now have to be tuned using NSGA-II
%So lets call NSGA-II now. Some code snippets have been taken from the nsga_2.m written by Arvind Shshadri and requires attribution to the following:

%%  Copyright (c) 2009, Aravind Seshadri
%  All rights reserved.

%% Make sure pop and gen are integers
pop = round(init_pop_size);
current_pop_size=round(init_pop_size);
gen = round(gen);
%interval_length=round(interval_length);
%% Objective Function
% The objective function description contains information about the
% objective function. M is the dimension of the objective space, V is the
% dimension of decision variable space, min_range and max_range are the
% range for the variables in the decision variable space. User has to
% define the objective functions using the decision variables. Make sure to
% edit the function 'evaluate_objective' to suit your needs.
V=numweights;%specify the same as the following function will prompt you for 
%number of decision variables
[M, V, min_range, max_range] = objective_description_function(4, numweights,-5*ones(numweights,1), 5*ones(numweights,1));

%% Initialize the population
% Population is initialized with random values which are within the
% specified range. Each chromosome consists of the decision variables. Also
% the value of the objective functions, rank and crowding distance
% information is also added to the chromosome vector but only the elements
% of the vector which has the decision variables are operated upon to
% perform the genetic operations like corssover and mutation.
chromosome = initialize_variables(pop, M, V, min_range, max_range);

%%This is the most subtle piece of code in this function. This basically does all the evaluation
%Initially the following loop was in initialize_variables and also in 
%genetic_operator. We have commented it out from those functions. 
%The reason being that we do not want it to be evaluated there. 
%We want them to be evaluated here as fetching data from FlightGear can
%be conveniently done over here. And it should be done here as well.
%This is the testbed that acts as an interface between FlightGear and ML.
%So all the necessary data processing should be done here.
K=V+M;

net_ctrls_for_all_sims_all_gens=[];%Initialize this array to contain nothing. We are going to use this to store values for simulation results for all thee runs for statistical analysis latter.

net_fdm_for_all_sims_all_gens=[];%Initialize this array to contain nothing. We are going to use this to store values for simulation results for all thee runs for statistical analysis latter.


net_ctrls_for_all_sims_one_gen=[];%Initialize this array to contain nothing. We are going to use this to store values for simulation results for all thee runs for statistical analysis latter.

net_fdm_for_all_sims_one_gen=[];%Initialize this array to contain nothing. We are going to use this to store values for simulation results for all thee runs for statistical analysis latter.

net_fdm=getMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);

%We are also calling here getMyFlightData to get the current state of FGFS. We would use this state as a reference starting state for 
%the rest of the simulation.

net_fdm_ref=net_fdm;

%One last thing: Set the start conditions for a flight.
start_conds=start_conditions;
%And the target values to be achieved
target_vals=target_values;

%We are also calling here sendMyFlightData oct-file because we want to initialize the struct netCtrls that is going to hold values for control related properties of FGFS.

netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);


%Similarly call here initGeneralParam.m
netCtrls=initGeneralParams(netCtrls);

%And now call initC172Params.m
netCtrls=initC172Params(netCtrls);

%This will load lat and lon in xq and yq.
load SamplePath;

fitness=5000*ones(1,4);


for(i=1:pop)
%First of all we assign values to the weigths of the network.
  NET=nnassignweights(NET, chromosome(i,1:V));

%Reset the properties of FGFS. We especially need to reset the altitude, longitude latitude values so as to place our plane under test on the initial position.

net_fdm=setFGNetFDMvars(FGIncomingPort, FGOutgoingPort, FGIncomingPort-1, fg_ip_address, net_fdm_ref, start_conds);
%sendFGNetFDMData(FGIncomingPort,FGIncomingPort-1, fg_ip_address, net_fdm, 0);
%[script_output, script_text]=system(python_script);
%disp(script_text);


%We are also calling here sendMyFlightData oct-file because we want to initialize the struct netCtrls that is going to hold values for control related properties of FGFS.

netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);


%Similarly call here initGeneralParam.m
netCtrls=initGeneralParams(netCtrls);

%And now call initC172Params.m
netCtrls=initC172Params(netCtrls);
 
%Now fetch input variables from FlightGear. We do this by calling our getMyFlightData oct-file.
%In case of static learning of the model, this data shall have to be sent in a loop
%again and again iteratively. In case of adaptive optimization, weigths shall have to be updated as soon as 
%a new snapshot of the state of FlightGear is fetched. Since we are using NSGA-II,
%we shall be using static learning merely because of the fact that it is a very compute intensive algorithm.
%
net_ctrls_for_one_sim=[];%Initialize this array to contain nothing. We are going to use this to store values for all simulation steps for statistical analysis latter.

net_fdm_for_one_sim=[];%Initialize this array to contain nothing. We are going to use this to store values for all simulation steps for statistical analysis latter.

path_step_counter=1;

for(j=1:epochs)
   net_fdm2=getMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);
%net_fdm is an octave struct, we should fetch relevant values from it.
%Check if you have received a valid struct in net_fdm. This can only happen if getMyFlightData returns successfully.
%And only then assign it to net_fdm. This implies that if it is not the case then we would be using the previous values.
if(isstruct(net_fdm2))
  net_fdm=net_fdm2;
endif

inputData=[net_fdm.altitude net_fdm.longitude net_fdm.latitude net_fdm.agl net_fdm.phi net_fdm.theta net_fdm.psi net_fdm.alpha net_fdm.beta net_fdm.phidot net_fdm.thetadot net_fdm.psidot net_fdm.vcas net_fdm.climb_rate net_fdm.v_north net_fdm.v_east net_fdm.v_down net_fdm.v_wind_body_north net_fdm.v_wind_body_east net_fdm.v_wind_body_down net_fdm.A_X_pilot net_fdm.A_Y_pilot net_fdm.A_Z_pilot];%So these are a total of 17 input variables. They are explained in getMyFlightData.cc or elsewhere on the property tree of FGFS. They are basically related to position, orientation and velocities in various directions of the aircraft under test.

%Now we evaluate this network. We provide dummy values for the target 'y' as we do not require them.
%But we have to evaluate it again and again so as to see 
   NET = nnff(NET, inputData, ones(1,architecture(end)));
%Get the output of this network
    output=NET.a{NET.n};
%We assume that the first four outputs of this network are throttle, aileron, rudder and elevator values in this order. So we assign them to the netCtrls struct before sending them over. Any additional values you may desire can be obtained by increasing the number of neurons to the output layer of the network and assiging them to the corresponding values of the netCtrls struct before sending them over.

netCtrls.throttle(1)=confineValuesBetweenMinMax(output(1), 0.01, 1);
netCtrls.aileron=confineValuesBetweenMinMax(output(2), -1, 1);
netCtrls.rudder=confineValuesBetweenMinMax(output(3), -1, 1);
netCtrls.elevator=confineValuesBetweenMinMax(output(4), -1, 1);

%netCtrls.throttle
%output
%return;
%Send this output back to Flightgear FDM
    netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address, netCtrls, 0);%sendMyFlightData is an oct-file that sends control data back to FlightGear.
    %Now is the time to evaluate the chromosomes of NSGA-II
    if(mod(j,3)==0)
      target_vals=target_values(xq(path_step_counter), yq(path_step_counter));
      fitness = evaluate_objective(chromosome(i,:), M, V, net_fdm, cost_function, netCtrls, target_vals, path_step_counter);
      chromosome(i,V + 1: K)=fitness;
    endif
  %If the current point on the path has been reached, then move to the next point on the path.
  if(fitness(2)<0.01)
    path_step_counter+=1;
  endif
  
     %Pause the simulation for the specified length of interval.
  pause(interval_length);
net_ctrls_for_one_sim=[net_ctrls_for_one_sim netCtrls];
net_fdm_for_one_sim=[net_fdm_for_one_sim net_fdm];




%disp("Running this simulation for the initial generation"), disp(i), disp("th individual and"), disp(j), disp("th interval"); 
  endfor
	net_ctrls_for_all_sims_one_gen=[net_ctrls_for_all_sims_one_gen net_ctrls_for_one_sim];
	net_fdm_for_all_sims_one_gen=[net_fdm_for_all_sims_one_gen net_fdm_for_one_sim];
endfor
net_ctrls_for_all_sims_all_gens=[net_ctrls_for_all_sims_all_gens net_ctrls_for_all_sims_one_gen];
net_fdm_for_all_sims_all_gens=[net_fdm_for_all_sims_all_gens net_fdm_for_all_sims_one_gen];

%% Sort the initialized population
% Sort the population using non-domination-sort. This returns two columns
% for each individual which are the rank and the crowding distance
% corresponding to their position in the front they belong. At this stage
% the rank and the crowding distance for each chromosome is added to the
% chromosome vector for easy of computation.
chromosome = non_domination_sort_mod(chromosome, M, V);

%% Start the evolution process
% The following are performed in each generation
% * Select the parents which are fit for reproduction
% * Perfrom crossover and Mutation operator on the selected parents
% * Perform Selection from the parents and the offsprings
% * Replace the unfit individuals with the fit individuals to maintain a
%   constant population size.
current_simulation_time=start_simulation_time;%Initialize the current_sim_time to start_sim_time
epochs=abs(round(current_simulation_time/interval_length));

for i = 1 : gen
    % Select the parents
    % Parents are selected for reproduction to generate offspring. The
    % original NSGA-II uses a binary tournament selection based on the
    % crowded-comparision operator. The arguments are 
    % pool - size of the mating pool. It is common to have this to be half the
    %        population size.
    % tour - Tournament size. Original NSGA-II uses a binary tournament
    %        selection, but to see the effect of tournament size this is kept
    %        arbitary, to be choosen by the user.
    pool = round(pop/2);
    tour = 2;
    %Increase the simulation time upto a certain number of generations and keep it there for a while for next few generations.
    %After that start increasing it again.
    if(current_simulation_time<intermediate_simulation_time && i<=40)
      current_simulation_time+=1;
      epochs=abs(round(current_simulation_time/interval_length));
    elseif(current_simulation_time<end_simulation_time && i>40)
      current_simulation_time+=1;
      epochs=abs(round(current_simulation_time/interval_length));
    endif%Code added by Adil
    
  %Compute the epochs according to the current simulation time

    % Selection process
    % A binary tournament selection is employed in NSGA-II. In a binary
    % tournament selection process two individuals are selected at random
    % and their fitness is compared. The individual with better fitness is
    % selcted as a parent. Tournament selection is carried out until the
    % pool size is filled. Basically a pool size is the number of parents
    % to be selected. The input arguments to the function
    % tournament_selection are chromosome, pool, tour. The function uses
    % only the information from last two elements in the chromosome vector.
    % The last element has the crowding distance information while the
    % penultimate element has the rank information. Selection is based on
    % rank and if individuals with same rank are encountered, crowding
    % distance is compared. A lower rank and higher crowding distance is
    % the selection criteria.
    parent_chromosome = tournament_selection(chromosome, pool, tour);
      

    % Perfrom crossover and Mutation operator
    % The original NSGA-II algorithm uses Simulated Binary Crossover (SBX) and
    % Polynomial  mutation. Crossover probability pc = 0.9 and mutation
    % probability is pm = 1/n, where n is the number of decision variables.
    % Both real-coded GA and binary-coded GA are implemented in the original
    % algorithm, while in this program only the real-coded GA is considered.
    % The distribution indeices for crossover and mutation operators as mu = 20
    % and mum = 20 respectively.
    mu = 20;
    mum = 20;
    offspring_chromosome = ...
        genetic_operator(parent_chromosome, ...
        M, V, mu, mum, min_range, max_range);
        
       
%Now this loop is basically the same as the first loop in this function.
%The only difference is that here the offspring_chromosome is evaluated. 
child_pop_size=rows(offspring_chromosome);

net_ctrls_for_all_sims_one_gen=[];%Initialize this array to contain nothing. We are going to use this to store values for simulation results for all thee runs for statistical analysis latter.

net_fdm_for_all_sims_one_gen=[];%Initialize this array to contain nothing. We are going to use this to store values for simulation results for all thee runs for statistical analysis latter.


    for(j=1:child_pop_size)

%First of all we reset some propertites of the FGFS to put it back to the initial position. 

net_fdm=setFGNetFDMvars(FGIncomingPort, FGOutgoingPort, FGIncomingPort-1, fg_ip_address, net_fdm_ref, start_conds);
%sendFGNetFDMData(FGIncomingPort,FGIncomingPort-1, fg_ip_address, net_fdm, 0);

%[script_output, script_text]=system(python_script);
%disp(script_text);

%Then we assign values to the weigths of the network.
	NET=nnassignweights(NET, offspring_chromosome(j,1:V));

%We are also calling here sendMyFlightData oct-file because we want to initialize the struct netCtrls that is going to hold values for control related properties of FGFS.

%netCtrls2=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);

%Similarly call here initGeneralParam.m
%netCtrls2=initGeneralParams(netCtrls2);

%And now call initC172Params.m
%netCtrls2=initC172Params(netCtrls2);
  
%Now fetch input variables from FlightGear. We do this by calling our getMyFlightData oct-file.
%In case of static learning of the model, this data shall have to be sent in a loop
%again and again iteratively. In case of adaptive optimization, weigths shall have to be updated as soon as 
%a new snapshot of the state of FlightGear is fetched. Since we are using NSGA-II,
%we shall be using static learning merely because of the fact that it is a very compute intensive algorithm.
%

net_ctrls_for_one_sim=[];%Initialize this array to contain nothing. We are going to use this to store values for all simulation steps for statistical analysis latter.

net_fdm_for_one_sim=[];%Initialize this array to contain nothing. We are going to use this to store values for all simulation steps for statistical analysis latter.

disp('epochs is: '), disp(epochs);
disp('interval_length is: '), disp(interval_length);

path_step_counter=1;
    for(k=1:epochs)
    printf("We are here: %d, %d, %d\n", i, j, k);

    net_fdm2=getMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);
%net_fdm is an octave struct, we should fetch relevant values from it.

%Check if you have received a valid struct in net_fdm. This can only happen if getMyFlightData returns successfully.
%And only then assign it to net_fdm. This implies that if it is not the case then we would be using the previous values.
if(isstruct(net_fdm2))
  net_fdm=net_fdm2;
endif
inputData=[net_fdm.altitude net_fdm.longitude net_fdm.latitude net_fdm.agl net_fdm.phi net_fdm.theta net_fdm.psi net_fdm.alpha net_fdm.beta net_fdm.phidot net_fdm.thetadot net_fdm.psidot net_fdm.vcas net_fdm.climb_rate net_fdm.v_north net_fdm.v_east net_fdm.v_down net_fdm.v_wind_body_north net_fdm.v_wind_body_east net_fdm.v_wind_body_down net_fdm.A_X_pilot net_fdm.A_Y_pilot net_fdm.A_Z_pilot];%So these are a total of 23 input variables. They are explained in getMyFlightData.cc or elsewhere on the property tree of FGFS. They are basically related to position, orientation and velocities in various directions of the aircraft under test.

%Now we evaluate this network. We provide dummy values for the target 'y' as we do not require them.
%But we have to evaluate it again and again so as to see 
      NET = nnff(NET, inputData, ones(1,architecture(end)));
%Get the output of this network
      output=NET.a{NET.n};
%We assume that the first four outputs of this network are throttle, aileron, rudder and elevator values in this order. So we assign them to the netCtrls struct before sending them over. Any additional values you may desire can be obtained by increasing the number of neurons to the output layer of the network and assiging them to the corresponding values of the netCtrls struct before sending them over.
%netCtrls2=setfield(netCtrls2, {1}, "throttle", {1}, double(confineValuesBetweenMinMax(output(1), 0, 100)));
%netCtrls2=setfield(netCtrls2, "aileron", 22);
%netCtrls2.aileron=double(confineValuesBetweenMinMax(output(2), -1, 1));
%netCtrls.throttle(1)=confineValuesBetweenMinMax(output(1), -1, 1);
%netCtrls=copyNetCtrlsVals(netCtrls,"aileron", 1);

netCtrls.throttle(1)=confineValuesBetweenMinMax(output(1), 0.01, 1);
netCtrls.aileron=confineValuesBetweenMinMax(output(2), -1, 1);
netCtrls.rudder=confineValuesBetweenMinMax(output(3), -1, 1);
netCtrls.elevator=confineValuesBetweenMinMax(output(4), -1, 1);

if(~isstruct(netCtrls))
  disp('Here lies the error.');
  printf("We are here: %d, %d, %d\n", i, j, k);
  break;
endif

%Send this output back to Flightgear FDM
    netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address, netCtrls, 0);%sendMyFlightData is an oct-file that sends control data back to FlightGear.
    if(~isstruct(netCtrls))
      disp('sendMyFlightData causes error');
      break;
    endif
      %Pause the simulation for the specified length of interval.
     pause(interval_length);
     net_ctrls_for_one_sim=[net_ctrls_for_one_sim netCtrls];
     net_fdm_for_one_sim=[net_fdm_for_one_sim net_fdm];
     
    % disp("Running this simulation for the"), disp(i), disp("th generation"), disp(j), disp("th individual and"), disp(k), disp("th interval");
    
      %Now is the time to evaluate the chromosomes of NSGA-II
    if(mod(k,3)==0)
      target_vals=target_values(xq(path_step_counter), yq(path_step_counter));
      offspring_chromosome(j, V+1: K) = evaluate_objective(offspring_chromosome(i,:), M, V, net_fdm, cost_function, netCtrls, target_vals, path_step_counter);
     % chromosome(i,V + 1: K)=fitness;
    endif
  %If the current point on the path has been reached, then move to the next point on the path.
  if(fitness(2)<0.01)
    path_step_counter+=1;
  endif
  if(net_fdm.agl<500)%i.e. if the plane is falling and about to crash, lift it up
  net_fdm=setFGNetFDMvars(FGIncomingPort, FGOutgoingPort, FGIncomingPort-1, fg_ip_address, net_fdm_ref, start_conds);
endif
  
      endfor
     net_ctrls_for_all_sims_one_gen=[net_ctrls_for_all_sims_one_gen net_ctrls_for_one_sim];
	net_fdm_for_all_sims_one_gen=[net_fdm_for_all_sims_one_gen net_fdm_for_one_sim];
    endfor

%net_ctrls_for_all_sims_all_gens=[net_ctrls_for_all_sims_all_gens net_ctrls_for_all_sims_one_gen];
%net_fdm_for_all_sims_all_gens=[net_fdm_for_all_sims_all_gens net_fdm_for_all_sims_one_gen];


    % Intermediate population
    % Intermediate population is the combined population of parents and
    % offsprings of the current generation. The population size is two
    % times the initial population.
    
    [main_pop,temp] = size(chromosome);
    [offspring_pop,temp] = size(offspring_chromosome);
    % temp is a dummy variable.
    clear temp
    % intermediate_chromosome is a concatenation of current population and
    % the offspring population.
    intermediate_chromosome(1:main_pop,:) = chromosome;
    intermediate_chromosome(main_pop + 1 : main_pop + offspring_pop,1 : M+V) = ...
        offspring_chromosome;

    % Non-domination-sort of intermediate population
    % The intermediate population is sorted again based on non-domination sort
    % before the replacement operator is performed on the intermediate
    % population.
    intermediate_chromosome = ...
        non_domination_sort_mod(intermediate_chromosome, M, V);
    % Perform Selection
    % Once the intermediate population is sorted only the best solution is
    % selected based on it rank and crowding distance. Each front is filled in
    % ascending order until the addition of population size is reached. The
    % last front is included in the population based on the individuals with
    % least crowding distance
    chromosome = replace_chromosome(intermediate_chromosome, M, V, pop);
    
    
    %decrement pop by 2
    if(current_pop_size>final_pop_size)
      current_pop_size-=2;
      pop=current_pop_size;
    endif
    
    chromosome = chromosome(1:pop,:);%Only keep the best pop individuals. Adil
    if ~mod(i,100)
        clc
        fprintf('%d generations completed\n',i);
    end
endfor

%% Result
% Save the result in ASCII text format.
%save solution.txt chromosome -ASCII


%% Visualize
% The following is used to visualize the result if objective space
% dimension is visualizable.
if M == 2
    plot(chromosome(:,V + 1),chromosome(:,V + 2),'*');
elseif M ==3
    plot3(chromosome(:,V + 1),chromosome(:,V + 2),chromosome(:,V + 3),'*');
endif

all_results.net_ctrls_for_all_sims_all_gens=net_ctrls_for_all_sims_all_gens;%Return all net_ctrls data
all_results.net_fdm_for_all_sims_all_gens=net_fdm_for_all_sims_all_gens;%Return all net_fdm data
all_results.chromosome=chromosome;%Return final population
all_results.NET=NET;

simulation_results=all_results;

results_file=strcat("SimulationResults-", datestr(now(), 30), ".mat");

save(results_file, "simulation_results" );
return;
endfunction
