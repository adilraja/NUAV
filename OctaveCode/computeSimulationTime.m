## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} computeSimulationTime (@var{input1}, @var{input2})
## Computes the simulation time of a GA based flight simulator
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-28

function [retval, tot_time_per_gen] = computeSimulationTime (init_pop_size, fin_pop_size, gen, sim_time_start, sim_time_intermediate, sim_time_fin)
tot_time=0;
tot_time_per_gen=zeros(gen, 1);
cur_pop_size=init_pop_size;
sim_time_cur=sim_time_start;
for(i=1:gen)
  tot_time+=cur_pop_size*sim_time_cur;
  tot_time_per_gen(i)+=cur_pop_size*sim_time_cur;
  if(cur_pop_size>fin_pop_size)
    cur_pop_size-=2;
  endif
  if(sim_time_cur<sim_time_intermediate &&i <=40)
    sim_time_cur+=1;
  elseif(sim_time_cur<sim_time_fin && i>40)
    sim_time_cur+=1;
  endif
endfor
retval=tot_time/3600;
tot_time_per_gen=tot_time_per_gen/3600;

endfunction
