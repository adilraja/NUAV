## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} evaluateANNWithFGFS (@var{network}, @var{weights})
## This function evaluates a neural network model trained using nuav_testbed
## Inputs: - 
## network :- A neural network structure.
## weights: - A weight vector for weights of the network
## Outputs: -
## Outputs can be any user specified measures.
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-06-19

function [net_ctrls_for_one_sim, net_fdm_for_one_sim] =evaluateANNWithFGFS (network, weights, sim_time, interval_length, FGIncomingPort, FGOutgoingPort, fg_ip_address)
%One thing that we really need to know is that how many weights and biases are there in this network structure.
%We need to know this because we are going to initialize NSGA-II accordingly.

NET=network;
numweights = nnnumweights(NET);

%Then we assign values to the weigths of the network.
NET=nnassignweights(NET, weights(1:numweights));

%Now fetch input variables from FlightGear. We do this by calling our getMyFlightData oct-file.
%In case of static learning of the model, this data shall have to be sent in a loop
%again and again iteratively. In case of adaptive optimization, weigths shall have to be updated as soon as 
%a new snapshot of the state of FlightGear is fetched. Since we are using NSGA-II,
%we shall be using static learning merely because of the fact that it is a very compute intensive algorithm.
%

net_fdm=getMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);

%We are also calling here getMyFlightData to get the current state of FGFS. We would use this state as a reference starting state for 
%the rest of the simulation.

net_fdm_ref=net_fdm;

%One last thing: Set the start conditions for a flight.
start_conds=start_conditions;

%Reset the properties of FGFS. We especially need to reset the altitude, longitude latitude values so as to place our plane under test on the initial position.

net_fdm=setFGNetFDMvars(FGIncomingPort, FGOutgoingPort, FGIncomingPort-1, fg_ip_address, net_fdm_ref, start_conds);


%We are also calling here sendMyFlightData oct-file because we want to initialize the struct netCtrls that is going to hold values for control related properties of FGFS.

netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);


%Similarly call here initGeneralParam.m
netCtrls=initGeneralParams(netCtrls);

%Compute epochs
epochs=sim_time/interval_length;

%And now call initC172Params.m
netCtrls=initC172Params(netCtrls);

architecture=[17 4 3 4];

net_ctrls_for_one_sim=[];
net_fdm_for_one_sim=[];
  for(i=1:epochs)
  net_fdm=getMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);
%net_fdm is an octave struct, we should fetch relevant values from it.
inputData=[net_fdm.altitude net_fdm.longitude net_fdm.latitude net_fdm.agl net_fdm.phi net_fdm.theta net_fdm.psi net_fdm.alpha net_fdm.beta net_fdm.phidot net_fdm.thetadot net_fdm.psidot net_fdm.vcas net_fdm.climb_rate net_fdm.v_north net_fdm.v_east net_fdm.v_down];%So these are a total of 17 input variables. They are explained in getMyFlightData.cc or elsewhere on the property tree of FGFS. They are basically related to position, orientation and velocities in various directions of the aircraft under test.

%Now we evaluate this network. We provide dummy values for the target 'y' as we do not require them.
%But we have to evaluate it again and again so as to see 
   NET = nnff(NET, inputData, ones(1,architecture(end)));
%Get the output of this network
    output=NET.a{NET.n};
%We assume that the first four outputs of this network are throttle, aileron, rudder and elevator values in this order. So we assign them to the netCtrls struct before sending them over. Any additional values you may desire can be obtained by increasing the number of neurons to the output layer of the network and assiging them to the corresponding values of the netCtrls struct before sending them over.

netCtrls.throttle(1)=confineValuesBetweenMinMax(output(1), 0, 100);
netCtrls.aileron=confineValuesBetweenMinMax(output(2), -1, 1);
netCtrls.rudder=confineValuesBetweenMinMax(output(3), -1, 1);
netCtrls.elevator=confineValuesBetweenMinMax(output(4), -1, 1);

%netCtrls.throttle
%output
%return;
%Send this output back to Flightgear FDM
    netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address, netCtrls, 0);%sendMyFlightData is an oct-file that sends control data back to FlightGear.
     %Pause the simulation for the specified length of interval.
  pause(interval_length);
net_ctrls_for_one_sim=[net_ctrls_for_one_sim netCtrls];
net_fdm_for_one_sim=[net_fdm_for_one_sim net_fdm];

  endfor

endfunction
