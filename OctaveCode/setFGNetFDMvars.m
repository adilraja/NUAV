## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} setFGNetFDMvars (@var{input1}, @var{input2})
## This function resets FGNetFDM variables.
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-27

function [retval] = setFGNetFDMvars (port1, port2, port3, ip_address, net_fdm, start_conds)
  
 % net_fdm=getMyFlightData(port1, port2, ip_address);
  net_fdm.altitude=start_conds.altitude_ft;
 net_fdm.longitude=start_conds.longitude;
  net_fdm.latitude=start_conds.latitude;
  net_fdm.psi=start_conds.psi;
  net_fdm.theta=start_conds.theta;
  net_fdm.phi=start_conds.phi;
  net_fdm.fuel_quantity=start_conds.fuel_quantity;
  net_fdm=sendFGNetFDMData(port1, port3, ip_address, net_fdm, 0);
  retval=net_fdm;
endfunction
