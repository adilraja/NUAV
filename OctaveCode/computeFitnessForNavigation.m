## Copyright (C) 2016 FGFS
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} computeFitnessForNavigation (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: FGFS <fgfs@fgfs-Precision-WorkStation-T3500>
## Created: 2016-08-29

function [retval] = computeFitnessForNavigation (net_fdm, net_ctrls, target_vals, waypoint_num)
f=[];

%Compute the absolute distance between the current plane altitude and the target altitude.
f(1)=computeFitness(net_fdm.altitude, target_vals.altitude, 1)/target_vals.altitude;

%Compute the absolute difference between the current climb rate and the optimal climb rate.
%f(2)=computeFitness(net_fdm.climb_rate, target_vals.optimal_climb_rate, 1);

%Compute the distance between the current plane position and the target position.
%Uses the distance function from the mapping package

f(2)=deg2km(distance([net_fdm.longitude net_fdm.latitude], [target_vals.longitude target_vals.latitude]));

%Compute the absolute difference between the current flying speed and the max flying speed.
f(3)=computeFitness(net_fdm.vcas, target_vals.max_fly_speed, 1)/target_vals.max_fly_speed;

if(exist('waypoint_num', 'var'))
  f(4)=(60-waypoint_num)/60;%Lower is better
endif

retval=f;
endfunction
