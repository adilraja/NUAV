## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} saveFigures (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-08-04

function [retval] = saveFigures (fh, filename, idx)

file_name_jpg=strcat("jpg/", filename, "-ind-", num2str(idx), ".jpg");
print(fh, file_name_jpg, '-djpg');

file_name_pdf=strcat("pdf/", filename, "-ind-", num2str(idx), ".pdf");
print(fh, file_name_pdf, '-dpdf');

file_name_eps=strcat("eps/", filename, "-ind-", num2str(idx), ".eps");
print(fh, file_name_eps, '-deps');

file_name_fig=strcat("fig/", filename, "-ind-", num2str(idx), ".fig");
print(fh, file_name_fig, '-dfig');

endfunction
