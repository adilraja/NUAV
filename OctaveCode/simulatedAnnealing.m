## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} simulatedAnnealing (@var{input1}, @var{input2})
## Trans a neural network using the Simulated annealing algorithm.
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-08-12

function [retval] = simulatedAnnealing (NET, net_fdm, net_ctrls, target_values, num_weights, LB=-5, UB=5, max_iterations=100, acceptance_probability=0.2)

%Get the number of weights in the network if they have not been provided already

if(nargin<=3)
  num_weights=nnnumweights(NET);
endif

T=1.0;
T_min=0.00001;
alpha=0.9;

current_solution=rand(1, num_weights)*(UB-LB)+LB;
f=computeFitnessForSmoothingFlight(net_fdm, net_ctrls, target_vals);
f=sum(f);

while(T>T_min)
  for(i=1:max_iterations)
    new_solution=current_solution+rand(1, num_weights)-0.5;
    f=computeFitnessForSmoothingFlight(net_fdm, net_ctrls, target_vals);
  endfor
endwhile

endfunction
