import sys
import telnetlib

PROMPT = ':~$'
fg = telnetlib.Telnet();
fg.open(sys.argv[1],sys.argv[2],5);
# longitude and latitude should be of the same airport otherwise it will hang in the air . Reason It had to download the scenery from teragear
fg.write("set /position[0]/altitude-ft %s\r\n" % sys.argv[3]);
print fg.read_until('(double)')
fg.write("set /position[0]/latitude-deg %s\r\n" % sys.argv[4]);
print fg.read_until('(double)')
fg.write("set /position[0]/longitude-deg %s\r\n" % sys.argv[5]);
print fg.read_until('(double)')
fg.write("set /orientation/pitch-deg %s\r\n" % sys.argv[6]);
print fg.read_until('(double)')
fg.write("set /orientation/roll-deg %s\r\n" % sys.argv[7]);
print fg.read_until('(double)')
fg.write("set /orientation/yaw-deg %s\r\n" % sys.argv[8]);
print fg.read_until('(string)')
fg.write("quit\r\n");
print fg.read_until('host')

fg.close();
