## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} computeFitnessForTakeoff (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-27

function [retval] = computeFitnessForTakeoff (net_fdm, net_ctrls, target_vals)
f=[];

%Compute the Euclidean distance between the current plane position and the target position.
f(1)=computeFitness([net_fdm.altitude net_fdm.longitude net_fdm.latitude], [target_vals.altitude target_vals.longitude target_vals.latitude], 2);

%Compute the absolute difference between the current flying speed and the target speed.
f(2)=computeFitness(net_fdm.vcas, target_vals.max_fly_speed, 1);
%Compute the absolute difference between the current limb rate and the optimal climb rate.
f(3)=computeFitness(net_fdm.climb_rate, target_vals.optimal_climb_rate, 1);

retval=f;
endfunction
