## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} nuav_testbed (@var{pop}, @var{gen}, @var{architecture})
##
## This is the main file for NUAV testbed. Basically what it does is that it integrates 
## FlightGear with a machine learning algorithm. To this end, it has the ability to fetch and send data to FLightGear.
## Moreover, it can manipulate that data using a machine learning algorithm. In a very basic scheme what we have done
## is to create a neural network structure using a deep learning neural network toolbox.
## The toolbox can be fetched from here, http://www.mathworks.com/matlabcentral/fileexchange/38310-deep-learning-toolbox 
## The weights of the structure are adapted using the simulated annealing algorithm.
## The algorithm performs a static, online training of the control laws for a 
## fixed wing UAV. However, the code can be adapted for any type of a UAV, 
## for any scenario using any combination of ML algorithms. Our goal is to make this function more plug and play.
## 
##Inputs: Since the function calls NSGA-2, it should be provided with:
##  pop: population size of the GA.
##  gen: number of generations for which the NSGA-2 will run.
## Epochs: It is some measure of time intervals for which a single simulation involving a plane has to be run. These are basically time slices.
## For a simulation lasting 30 seconds, for example, epochs could be 30/1=30, or 30/5=6
## This basically means that for a simulation lasting 30 seconds either the state shall have to be sampled 30 times, or 6 times respectively.
##  architecture: Specify the architecture of the network
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-06-16

function [all_results, current_solution, NET] = nuav_testbed_simulated_annealing (start_simulation_time=1, intermediate_simulation_time=10, simulation_time=300, interval_length=0.015, architecture=[17 4 3 4], inputVariableIndices=[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23], cost_function="computeFitnessForSmoothingFlight", LB=-5, UB=5, fg_ip_address="192.168.0.28", FGIncomingPort=22222, FGOutgoingPort=22223, FGTelnetPort=22224, start_conds, target_vals, current_solution, T)

%First things go first. So first of all we create a neural network structure using the deep learning toolbox.
%But before doing that please add the path to the deep learning neural network toolbox using 
%the following commands on your octave command prompt.
% octave:1> addpath(genpath("/home/mike/mfiles"))
%octave:2> savepath

%Specify an architecture for the network. This should be an array containing the number of neurons in each of the layers.
%For example, architecture=[4 3 5] has 4 neurons in the input layer, 3 in the hidden layer
%and 5 in the output layer. So,
if(nargin<4)
  architecture=[17 4 3 4];
endif
%Check for the existence of other important variables
if(exist("architecture", "var")==0)
  architecture=[17 4 3 4];
else
  architecture(1)=columns(inputVariableIndices);
endif
if(exist("start_conds", "var")==0)
  start_conds=start_conditions;
endif

if(exist("target_vals", "var")==0)
  target_vals=target_values(start_conds.latitude, start_conds.longitude);
endif
 
%Before we do anything else lets create a command through which we are going to reset the potition of our plane everytime we want to test a solution.

%python_script=cstrcat("python set_fgfs_properties_through_telnet.py ", fg_ip_address," ", num2str(FGTelnetPort)," ", num2str(altitude_ft), " ", num2str(latitude), " ", num2str(longitude));
current_simulation_time=start_simulation_time;
epochs=round(current_simulation_time/interval_length);

%The number of neurons in the input layer should be typically equal to the number of input parameters
%(or variables) and the number of neurons in the output layer should be equal to the 
% number of neurons in the output layer.

%Now lets create the neural network
NET=nnsetup(architecture);

%One thing that we really need to know is that how many weights and biases are there in this network structure.
%We need to know this because we are going to initialize NSGA-II accordingly.

numweights = nnnumweights(NET);

%Once this has been done, its weighths now have to be tuned using NSGA-II
%So lets call NSGA-II now. Some code snippets have been taken from the nsga_2.m written by Arvind Shshadri and requires attribution to the following:

%%  Copyright (c) 2009, Aravind Seshadri
%  All rights reserved.



%interval_length=round(interval_length);

net_fdm_for_all_sims_one_gen=[];%Initialize this array to contain nothing. We are going to use this to store values for simulation results for all thee runs for statistical analysis latter.

net_fdm=getMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);

%We are also calling here getMyFlightData to get the current state of FGFS. We would use this state as a reference starting state for 
%the rest of the simulation.

net_fdm_ref=net_fdm;

%One last thing: Set the start conditions for a flight.
%start_conds=start_conditions;
%And the target values to be achieved
%target_vals=target_values;

%We are also calling here sendMyFlightData oct-file because we want to initialize the struct netCtrls that is going to hold values for control related properties of FGFS.

netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);


%Similarly call here initGeneralParam.m
netCtrls=initGeneralParams(netCtrls);

%And now call initC172Params.m
netCtrls=initC172Params(netCtrls);


if(exist("T", "var")==0)
  T=1.0;
endif

T_min=0.00001;
alpha=0.95;
if(exist("current_solution", "var")==0)
  current_solution=rand(1, numweights)*(UB-LB)+LB;
endif

f=feval(cost_function, net_fdm, netCtrls, target_vals);
old_cost=mean(f);

%First of all we assign values to the weigths of the network.
  NET=nnassignweights(NET, current_solution);

%Reset the properties of FGFS. We especially need to reset the altitude, longitude latitude values so as to place our plane under test on the initial position.

net_fdm=setFGNetFDMvars(FGIncomingPort, FGOutgoingPort, FGIncomingPort-1, fg_ip_address, net_fdm_ref, start_conds);
%sendFGNetFDMData(FGIncomingPort,FGIncomingPort-1, fg_ip_address, net_fdm, 0);
%[script_output, script_text]=system(python_script);
%disp(script_text);


%We are also calling here sendMyFlightData oct-file because we want to initialize the struct netCtrls that is going to hold values for control related properties of FGFS.

netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);


%Similarly call here initGeneralParam.m
netCtrls=initGeneralParams(netCtrls);

%And now call initC172Params.m
netCtrls=initC172Params(netCtrls);
 
%Now fetch input variables from FlightGear. We do this by calling our getMyFlightData oct-file.
%In case of static learning of the model, this data shall have to be sent in a loop
%again and again iteratively. In case of adaptive optimization, weigths shall have to be updated as soon as 
%a new snapshot of the state of FlightGear is fetched. Since we are using NSGA-II,
%we shall be using static learning merely because of the fact that it is a very compute intensive algorithm.
%
%net_ctrls_for_one_sim=[];%Initialize this array to contain nothing. We are going to use this to store values for all simulation steps for statistical analysis latter.

%net_fdm_for_one_sim=[];%Initialize this array to contain nothing. We are going to use this to store values for all simulation steps for statistical analysis latter.

net_fdm=getMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);
%net_fdm is an octave struct, we should fetch relevant values from it.
%Check if you have received a valid struct in net_fdm. This can only happen if getMyFlightData returns successfully.
%And only then assign it to net_fdm. This implies that if it is not the case then we would be using the previous values.
%if(isstruct(net_fdm2))
%  net_fdm=net_fdm2;
%endif

inputData=[net_fdm.altitude net_fdm.longitude net_fdm.latitude net_fdm.agl net_fdm.phi net_fdm.theta net_fdm.psi net_fdm.alpha net_fdm.beta net_fdm.phidot net_fdm.thetadot net_fdm.psidot net_fdm.vcas net_fdm.climb_rate net_fdm.v_north net_fdm.v_east net_fdm.v_down net_fdm.v_wind_body_north net_fdm.v_wind_body_east net_fdm.v_wind_body_down net_fdm.A_X_pilot net_fdm.A_Y_pilot net_fdm.A_Z_pilot];%So these are a total of 17 input variables. They are explained in getMyFlightData.cc or elsewhere on the property tree of FGFS. They are basically related to position, orientation and velocities in various directions of the aircraft under test.

%Now we evaluate this network. We provide dummy values for the target 'y' as we do not require them.
%But we have to evaluate it again and again so as to see 
   NET = nnff(NET, inputData(inputVariableIndices), ones(1,architecture(end)));
%Get the output of this network
    output=NET.a{NET.n};
%We assume that the first four outputs of this network are throttle, aileron, rudder and elevator values in this order. So we assign them to the netCtrls struct before sending them over. Any additional values you may desire can be obtained by increasing the number of neurons to the output layer of the network and assiging them to the corresponding values of the netCtrls struct before sending them over.
netCtrls.throttle(1)=confineValuesBetweenMinMax(output(1), 0.01, 1);
netCtrls.aileron=confineValuesBetweenMinMax(output(2)*2-1, -1, 1);
netCtrls.rudder=confineValuesBetweenMinMax(output(3)*2-1, -1, 1);
netCtrls.elevator=confineValuesBetweenMinMax(output(4)*2-1, -1, 1);

%Send this output back to Flightgear FDM
    netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address, netCtrls, 0);%sendMyFlightData is an oct-file that sends control data back to FlightGear.
%Wait for a while before entering the main loop
    pause(interval_length);
%Get the net_fdm data once again    
    net_fdm=getMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);

%The following arrays hold results
net_ctrls_for_one_sim=[];
net_fdm_for_one_sim=[];
%Calculate the number of times the following outer loop will run
i=0;   


 
while(T>T_min)
  i+=1;
  T*=alpha;

endwhile  

%compute epochs, the outer while loop will run for i times
epochs=epochs-i;
epochs=epochs/i;
epochs=round(i);
%set T back to its original value
T=1;

loop_count=0;
%This will load lat and lon in xq and yq.
load SamplePath;

while(T>T_min)
%Reset the properties of FGFS. We especially need to reset the altitude, longitude latitude values so as to place our plane under test on the initial position.

net_fdm=setFGNetFDMvars(FGIncomingPort, FGOutgoingPort, FGIncomingPort-1, fg_ip_address, net_fdm_ref, start_conds);
%sendFGNetFDMData(FGIncomingPort,FGIncomingPort-1, fg_ip_address, net_fdm, 0);
%[script_output, script_text]=system(python_script);
%disp(script_text);


%We are also calling here sendMyFlightData oct-file because we want to initialize the struct netCtrls that is going to hold values for control related properties of FGFS.

netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);

%This keeps count of the current point on the path
path_step_counter=50;


%Similarly call here initGeneralParam.m
netCtrls=initGeneralParams(netCtrls);

%And now call initC172Params.m
netCtrls=initC172Params(netCtrls);
epochs=round(current_simulation_time/interval_length);

  for(j=1:epochs)
  
  %Generate a new solution.
  
  new_solution=rand(1, numweights)*(UB-LB)+LB;
  
  %Assign this to network as weights
   NET=nnassignweights(NET, new_solution);
   
 %Fetch the inputData from net_fdm to be fed to the network.
   
 inputData=[net_fdm.altitude net_fdm.longitude net_fdm.latitude net_fdm.agl net_fdm.phi net_fdm.theta net_fdm.psi net_fdm.alpha net_fdm.beta net_fdm.phidot net_fdm.thetadot net_fdm.psidot net_fdm.vcas net_fdm.climb_rate net_fdm.v_north net_fdm.v_east net_fdm.v_down net_fdm.v_wind_body_north net_fdm.v_wind_body_east net_fdm.v_wind_body_down net_fdm.A_X_pilot net_fdm.A_Y_pilot net_fdm.A_Z_pilot];%So these are a total of 17 input variables. They are explained in getMyFlightData.cc or elsewhere on the property tree of FGFS. They are basically related to position, orientation and velocities in various directions of the aircraft under test.

   
%Now we evaluate this network. We provide dummy values for the target 'y' as we do not require them.
%But we have to evaluate it again and again so as to see 
   NET = nnff(NET, inputData(inputVariableIndices), ones(1,architecture(end)));
%Get the output of this network
    output=NET.a{NET.n};
%We assume that the first four outputs of this network are throttle, aileron, rudder and elevator values in this order. So we assign them to the netCtrls struct before sending them over. Any additional values you may desire can be obtained by increasing the number of neurons to the output layer of the network and assiging them to the corresponding values of the netCtrls struct before sending them over.
netCtrls.throttle(1)=confineValuesBetweenMinMax(output(1), 0.01, 1);
netCtrls.aileron=confineValuesBetweenMinMax(output(2)*2-1, -1, 1);
netCtrls.rudder=confineValuesBetweenMinMax(output(3)*2-1, -1, 1);
netCtrls.elevator=confineValuesBetweenMinMax(output(4)*2-1, -1, 1);

%Send this output back to Flightgear FDM
    netCtrls=sendMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address, netCtrls, 0);%sendMyFlightData is an oct-file that sends control data back to FlightGear.
 %Wait for a while and then fetch the data
  pause(interval_length);
%Now get the net_fdm data 
net_fdm2=getMyFlightData(FGIncomingPort, FGOutgoingPort, fg_ip_address);
%net_fdm is an octave struct, we should fetch relevant values from it.
%Check if you have received a valid struct in net_fdm. This can only happen if getMyFlightData returns successfully.
%And only then assign it to net_fdm. This implies that if it is not the case then we would be using the previous values.
if(isstruct(net_fdm2))
  net_fdm=net_fdm2;
endif    

    
   
%Specify the current target longitude and latitude values.
target_vals=target_values(xq(path_step_counter), yq(path_step_counter));

 %Now compute the new_cost.
f=feval(cost_function, net_fdm, netCtrls, target_vals);

%Check to see if the current target point on the path has been reached.
%If so, increment the path counter to the next point.
%f(2) contains the distance in km.
%if(f(2)<0.01)
%  path_step_counter+=1;
%endif
new_cost=mean(f);
ap = acceptance_probability_for_simulated_annealing(old_cost, new_cost, T);
if(ap > rand(1,1))
  current_solution = new_solution;
  old_cost = new_cost;
endif 
if(net_fdm.agl<100)%i.e. if the plane is falling and about to crash, lift it up
  net_fdm=setFGNetFDMvars(FGIncomingPort, FGOutgoingPort, FGIncomingPort-1, fg_ip_address, net_fdm_ref, start_conds);
endif

%Pause the simulation for the specified length of interval.
 
net_ctrls_for_one_sim=[net_ctrls_for_one_sim netCtrls];
net_fdm_for_one_sim=[net_fdm_for_one_sim net_fdm];

%disp("Running this simulation for the initial generation"), disp(i), disp("th individual and"), disp(j), disp("th interval"); 
  endfor
  T=T*alpha;
  if(current_simulation_time<intermediate_simulation_time)
    current_simulation_time+=0.125;
  endif
  if(loop_count>80 && current_simulation_time<simulation_time)
    current_simulation_time+=1;
  endif
  
  loop_count+=1;

endwhile

%% Result
% Save the result in ASCII text format.
%save solution.txt chromosome -ASCII


%% Visualize
% The following is used to visualize the result if objective space
% dimension is visualizable.
%if M == 2
 %   plot(chromosome(:,V + 1),chromosome(:,V + 2),'*');
%elseif M ==3
 %   plot3(chromosome(:,V + 1),chromosome(:,V + 2),chromosome(:,V + 3),'*');
%endif

all_results.net_ctrls_for_one_sim=net_ctrls_for_one_sim;%Return all net_ctrls data
all_results.net_fdm_for_one_sim=net_fdm_for_one_sim;%Return all net_fdm data
all_results.current_solution=current_solution;%Return final population
all_results.NET=NET;

simulation_results=all_results;

%results_file=strcat("SimulationResults-Simulated-Annealing-", datestr(now(), 30), ".mat");

%save(results_file, "simulation_results" );
return;
endfunction
