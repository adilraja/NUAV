## Copyright (C) 2016 FGFS
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} nuav_testbed_in_a_loop (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: FGFS <fgfs@fgfs-Precision-WorkStation-T3500>
## Created: 2016-08-24

function [retval] = nuav_testbed_in_a_loop (loop_count, start_simulation_time=1, intermediate_simulation_time=10, simulation_time=300, interval_length=0.015, architecture=[17 4 3 4], inputVariableIndices=[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23], cost_function="computeFitnessForSmoothingFlight", LB=-5, UB=5, fg_ip_address="192.168.0.28", FGIncomingPort=22222, FGOutgoingPort=22223, FGTelnetPort=22224, start_conds, target_vals, current_solution, T)

all_results_of_the_loop=[];

%Check for the existence of other important variables
if(exist("architecture", "var")==0)
  architecture=[17 4 3 4];
endif
if(exist("start_conds", "var")==0)
  start_conds=start_conditions;
endif

if(exist("target_vals", "var")==0)
  target_vals=target_values;
endif

if(exist("T", "var")==0)
  T=1.0;
endif

%if(exist("current_solution", "var")==0)
 % current_solution=rand(1, numweights)*(UB-LB)+LB;
%endif

for(i=1:loop_count)
  [all_results, current_solution, NET] = nuav_testbed_simulated_annealing (start_simulation_time, intermediate_simulation_time, simulation_time, interval_length, architecture, inputVariableIndices, cost_function, LB, UB, fg_ip_address, FGIncomingPort, FGOutgoingPort, FGTelnetPort);
  all_results_of_the_loop=[all_results_of_the_loop all_results];
endfor

retval=all_results_of_the_loop;

results_file=strcat("SimulationResults-Simulated-Annealing-", datestr(now(), 30), ".mat");

save(results_file, "retval" );

endfunction
