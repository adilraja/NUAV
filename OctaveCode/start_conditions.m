## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} start_conditions (@var{input1}, @var{input2})
## The initial conditions for a flight
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-27

function [retval] = start_conditions ()
start_conds.altitude_ft=2500;%The engine stops at 3500 ft
start_conds.longitude=0.03;
start_conds.latitude=0.720;

%Final position lon=0.1, lat=1.0, tot_distance=19.95 miles http://andrew.hedges.name/experiments/haversine/
start_conds.psi=0.78540;%Yaw radians, or true heading
start_conds.phi=0;
start_conds.theta=0;
start_conds.fuel_quantity=[120 120];
retval=start_conds;
endfunction
