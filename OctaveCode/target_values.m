## Copyright (C) 2016 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} target_vals (@var{input1}, @var{input2})
##Specify target values for a flight
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2016-07-21

function [retval] = target_values(target_lat, target_lon)
target_vals.altitude=2500;
if(exist("target_lat", "var")==1)
  target_vals.latitude=target_lat;
endif
if(exist("target_lon", "var")==1)
  target_vals.longitude=target_lon;
endif
target_vals.max_fly_speed=80;
target_vals.optimal_climb_rate=12;%ft/sec
target_vals.phi=0;
target_vals.theta=0;
target_vals.psi=0;
retval=target_vals;
endfunction
