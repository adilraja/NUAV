1. Installing Octave 4.0.0

sudo apt-add-repository ppa:octave/stable
sudo apt-get update
sudo apt-get install octave

To set read-write permissions issue following commands.

cd /home/mahgul/.config/octave/
sudo chown mahgul qt-settings
(In place of the word "mahgul" you must place your user name)


2. Installing OpenGL
(PLIB requires OpenGl working library. Therefore, before installing PLIB we install OpenGl.)

sudo apt-get install freeglut3 freeglut3-dev
sudo apt-get install binutils-gold
sudo apt-get install libqt4-opengl-dev
sudo apt-get install libxmu-dev libxmu6
sudo apt-get install build-essential libgl1-mesa-dev
sudo apt-get install libglew-dev libsdl2-dev libsdl2-image-dev libglm-dev libfreetype6-dev

(Check your OpenGL installation)
sudo apt install mesa-utils
glxinfo | grep OpenGL


3. Installing Plib		
(Download PLIB - Version 1.8.5 from http://plib.sourceforge.net/download.html)
Install PLIB by simply issuing following commands.

tar xzf plib-1.8.5.tar.gz
cd plib-1.8.5
./configure
sudo make install


4. Installing GitLab
sudo apt-get install git
sudo apt-get install gitlab

You can either clone or pull the data from the repository. The only difference between them is git clone gets the local copy of an existing repository to work on and git pull (or git fetch + git merge) is how you update that local copy with new commits from the remote repository. (StackOverflow)

~ Before cloning, we need to generate SSH key. Copy paste SSH Key on our profile.

ssh-keygen -t rsa -C "mahgul2012@namal.edu.pk"
cd /home/mahgul/.ssh
chmod 700 ~/.ssh
cat ~/.ssh/id_rsa.pub
git clone git@gitlab.com:adilraja/UAVs-NAMAL.git

Cloning sometimes give read error which can also be solved using following commands
First, turn off compression: 
git config --global core.compression 0

Next, let's do a partial clone to truncate the amount of info coming down:
git clone --depth 1 git@gitlab.com:adilraja/UAVs-NAMAL.git

When that works, go into the new directory and retrieve the rest of the clone:
git fetch --unshallow 

Now, do a regular pull:
git pull --all


~ Otherwise to pull we issue the following commands

git init
git remote add origin https://gitlab.com/adilraja/UAVs-NAMAL
git fetch --all
git pull origin master


5. Installing Dropbox
32-bit:
cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86" | tar xzf -

64-bit:
cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -

~/.dropbox-dist/dropboxd


6. Installing Simgear

git clone git://git.code.sf.net/p/flightgear/simgear simgear
cd simgear

Issue the following command before compiling simgear. Otherwise, it will give you CMake - configuration error.

sudo apt-get install libboost-dev libcurl4-openssl-dev libdbus-1-dev libfltk1.3-dev  libgtkglext1-dev  libjpeg62-turbo-dev   libopenal-dev libopenscenegraph-dev librsvg2-dev libxml2-dev libvlc-dev

sudo apt-get install cmake
 
cmake .
make
sudo make install

Alternatively, Simgear can be installed as follows:

sudo apt-get update -y
sudo apt-get install -y libsimgear-dev




7. Installing FlightGear from a PPA

sudo add-apt-repository ppa:saiarcot895/flightgear
sudo apt-get update
sudo apt-get install flightgear
