/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include "include/Ipomdp/AlphaVector.hxx"
using namespace std;

AlphaVectors::AlphaVectors(){}

AlphaVectors::AlphaVectors(string filename){
	ifstream inputfile(filename.c_str());
	string act;
	string vect, newline;

	int no_of_states;
	if(inputfile.is_open()){
			int ctr=0;
			while(! inputfile.eof()){
				if(ctr%3==0){
					getline(inputfile, act);
				}
				else if(ctr%3==1){
					getline(inputfile, vect);
					vector <string> tokens;
					Tokenize(vect, tokens);
					if(ctr==1){
						no_of_states=tokens.size();
					}
					else if(no_of_states != tokens.size()){
						cerr<<"\nError: Mismatch in sizes of alphavectors";
						exit(1);
					}
					alphavectors av;
					av.act=atoi(act.c_str());
					
					for(int i=0; i<tokens.size(); i++){
						av.intercepts.push_back(atof(tokens[i].c_str()));
					}
					alphvects.push_back(av);
				}
				else{
					getline(inputfile, newline);
					//cout<<"\nnew line";
				}
				ctr++;
			}
			nodecount = ctr/3;
			inputfile.close();
	}
	else{
		cout<<"couldn't open alpha vector file";
		exit(0);
	}

}
AlphaVectors::~AlphaVectors(){}


vector<int> AlphaVectors::getOptimalAction (vector<double> pbelief){
	double max_expected_reward=numeric_limits<double>::min();
	double expected_reward;
	vector<int> optimal_action;
	if(pbelief.size()!=alphvects[0].intercepts.size()){
		cerr<<"\nError AlphaVector::getOptimalAction: Incorrect Belief size";
		exit(1);
	}
	for(int i=0; i<alphvects.size(); i++){
		expected_reward = 0;
		for(int j=0; j<pbelief.size(); j++){
			expected_reward+=pbelief[j]*alphvects[i].intercepts[j];
		}
		if(expected_reward>max_expected_reward){
			max_expected_reward=expected_reward;
		}
	}
	for(int i=0; i<alphvects.size(); i++){
		expected_reward = 0;
		for(int j=0; j<pbelief.size(); j++){
			expected_reward+=pbelief[j]*alphvects[i].intercepts[j];
		}
		if(expected_reward==max_expected_reward){
			optimal_action.push_back(alphvects[i].act);
		}
	}
	return(optimal_action);
}


//No Rounding up of values here. Need to be taken care of in the alpha vector file//
vector<int> AlphaVectors::getOptimalPolicy (vector<double> belief){
	double max_expected_reward=numeric_limits<double>::min();
	double expected_reward;
	vector<int> optimal_policy;
	if(belief.size()!=alphvects[0].intercepts.size()){
		cerr<<"\nError AlphaVectors::getOptimalPolicy: Incorrect Belief size";
		exit(1);
	}
	for(int i=0; i<alphvects.size(); i++){
		expected_reward = 0;
		for(int j=0; j<belief.size(); j++){
			expected_reward+=belief[j]*alphvects[i].intercepts[j];
		}
		if(expected_reward>max_expected_reward){
			max_expected_reward=expected_reward;
			
		}
	}
	for(int i=0; i<alphvects.size(); i++){
		expected_reward = 0;
		for(int j=0; j<belief.size(); j++){
			expected_reward+=belief[j]*alphvects[i].intercepts[j];
		}
		if(expected_reward==max_expected_reward){
			optimal_policy.push_back(i);
		}
	}
	return(optimal_policy);
}


void AlphaVectors::printAlphaVectors(){
	cout<<"\nNodeCount = "<<nodecount;
	for(int ctr=0; ctr<nodecount; ctr++){
		cout<<"\n\n"<<alphvects[ctr].act<<"\n";
		for(int i=0; i<alphvects[ctr].intercepts.size();i++){
			cout<<" "<<alphvects[ctr].intercepts[i]<<",";
		}
	}
}



