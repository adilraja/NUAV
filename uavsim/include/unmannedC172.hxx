/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#ifndef _UNMANNED_C172_
#define _UNMANNED_C172_

#include "common.hxx"
#include <time.h>

#include "airvehicle.hxx"

#include "fdm/fdm_udp_channel.hxx"
#include "Multiplayer/multiplayer_udp_channel.hxx"

#include "fdm/net_ctrls.hxx"
#include "fdm/net_fdm.hxx"

#define MAX_FLY_SPEED 80 //Speed in knots
#define MIN_FLY_SPEED 75 
#define INIT_HEADING 5.2
#define ABOVE_GROUND_AGL 1.45
#define TARGET_AGL_ERROR_ALLOWANCE 7.0
#define OPTIMAL_CLIMB_RATE 12.0 // the optimal climb rate (feet/second)
#define FULL_RADIAN_CIRCLE 6.28318531
#define HALF_RADIAN_CIRCLE 3.14159265

class UC172Control : public AVControl {

private:
  bool takeoffStatus;
  float cruisingAGL;
  float targetHeading;

  double elevatorErrorAccumulation;
  double elevatorErrorAccumulationTotal;

  double getAileronAdjustment_FlyStraight(FGNetFDM, FGNetCtrls*);
  double getElevatorAdjustment_FlyStraight(FGNetFDM, FGNetCtrls*);  
  
public:
  
  ~UC172Control();

  void initC172Params(FGNetCtrls*);

  void takeoff(FGNetFDM, FGNetCtrls*);
  void fly_straight(FGNetFDM, FGNetCtrls*);
  void turn_left(float);
  void turn_right(float);
  void descend_by(float);
  void ascend_by(float);
  void stop(FGNetFDM, FGNetCtrls*);

  void setTakeoffStatus(bool);
  bool getTakeoffStatus();
  void setCruisingAGL(float);
  float getCruisingAGL();
  void setTargetHeading(float);
  float getTargetHeading();

  void flyFromXToY(int, char **, FDMUDPChannel*, FGNetCtrls *, MultiplayerUDPChannel*, double, double, double, double, char*);

  void flyFromXToY(int, char **, FDMUDPChannel*, FGNetCtrls *,  MultiplayerUDPChannel*, double, double, double, double, time_t, double, char*);

  double getNewHeading(double,double,double,double);
};


#endif
