/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#ifndef POLICY_HXX_
#define POLICY_HXX_


#include "../common.hxx"


#include <fstream>
#include <vector>

struct Policy {
	int horizon;
	int currentObservation;
	int currentAction;
	vector<Policy> nextPolicies;
};

class IpomdpPolicy {
	
private:
	
	int horizonCount;
	int observationCount;
	vector<Policy> totalPolicy;
	vector<Policy> currentPolicy; 

	int numPolicy;

	Policy readyPolicy(fstream &file, int horizonIndex);
	
	void printIndividualPolicy(Policy policy);
	
public :
	IpomdpPolicy();
	IpomdpPolicy(string);
	~IpomdpPolicy();
 
	void next(int observation,int polNum);
	int getCurrentAction(int polNum);
	void printPolicy();
	int getHorizonCount();	
};

#endif /*POLICY_HXX_*/
