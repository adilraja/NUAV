/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include "include/airvehicle.hxx"


AVControl :: AVControl() {}

void AVControl :: initGeneralParams(FGNetCtrls *ctrls) {
  //ctrls = new FGNetCtrls();

  ctrls->version = 27;

  ctrls->aileron = 0;
  ctrls->elevator = 0;
  ctrls->rudder = 0;
  ctrls->aileron_trim = 0;
  ctrls->elevator_trim = 0;
  ctrls->rudder_trim = 0;
  ctrls->flaps = 0;
  ctrls->spoilers = 0;
  ctrls->speedbrake = 0;

  ctrls->flaps_power=1;
  ctrls->flap_motor_ok=1;

  ctrls->gear_handle=0;
  ctrls->master_avionics=1;
  
  ctrls->cross_feed = 1;

  ctrls->comm_1 = 0;
  ctrls->comm_2 = 0;
  ctrls->nav_1 = 0;
  ctrls->nav_2 = 0;

  ctrls->wind_speed_kt = 0;
  ctrls->wind_dir_deg = 0;
  ctrls->turbulence_norm = 0;
  
  ctrls->temp_c = 25;
  ctrls->press_inhg = 29.92;
  
  ctrls->hground = 0;
  ctrls->magvar = 0;
  
  ctrls->icing = 0;
  
  ctrls->speedup = 0x01;
  ctrls->freeze = 0;
}


FGNetCtrls* AVControl :: getCtrlParams()
{
  //return ctrls;
}


AVControl :: ~AVControl()
{
  //delete ctrls;
}
