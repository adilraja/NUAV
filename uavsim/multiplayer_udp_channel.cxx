/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include "include/Multiplayer/multiplayer_udp_channel.hxx"
#include "include/Multiplayer/tiny_xdr.hxx"
#include <simgear/math/SGGeodesy.hxx>
#include <simgear/math/SGMath.hxx>
#include <simgear/props/props.hxx>


#include<fstream>

#define MAX_PACKET_SIZE 1200
#define MAX_TEXT_SIZE 128


MultiplayerUDPChannel::MultiplayerUDPChannel(unsigned int l_port) {
	//Initialize the socket
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
		printErrorMessage((char*)"Failed to create socket");

	//Construct the server sockaddr_in struct
	memset(&server, 0, sizeof(server)); // Clear struct 
	server.sin_family = AF_INET; // Internet/IP 
	server.sin_addr.s_addr = htonl(INADDR_ANY); // Any IP address
	server.sin_port = htons(l_port); //  server port 

	//Bind the server to the socket
	int serverlen = sizeof(server);
	if (bind(sock, (struct sockaddr *) &server, serverlen) == -1) {
		cout<<l_port<<endl;
		printErrorMessage((char*)"Failed to bind server to socket and port");
	}
}

//Added For Sending Multiplayer Data//

MultiplayerUDPChannel::MultiplayerUDPChannel(unsigned int s_port, char* addr_to) {	
	
  mSocket = new netSocket();
  if (!mSocket->open(false)) {
		cout<<s_port<<" "<<(char*)addr_to<<endl;
                printErrorMessage((char*)"Failed to create data socket"); 
  }
  mServer.set(addr_to,s_port);//Hardcode if necessary//

}

void MultiplayerUDPChannel :: processFDM(FGNetFDM fdm, char* callsign)
{
    FGExternalMotionData motionInfo;
    
    // The current simulation time we need to update for,
    // note that the simulation time is updated before calling all the
    // update methods. Thus it contains the time intervals *end* time.
    // The FDM is already run, so the states belong to that time.
    motionInfo.time = 0.0;

    // The typical lag will be the reciprocal of the output frequency
    //double hz = get_hz();
    //if (hz != 0) // I guess we can test a double for exact zero in this case
    //  motionInfo.lag = 1/get_hz();
    //else
    //  motionInfo.lag = 0.1; //??

    motionInfo.lag = 0.1;

    // These are for now converted from lat/lon/alt and euler angles.
    // But this should change in FGInterface ...
    double lon = fdm.longitude;
    double lat = fdm.latitude;
    // first the aprioriate structure for the geodetic one
    SGGeod geod = SGGeod::fromRadFt(lon, lat, fdm.altitude);
    // Convert to cartesion coordinate
    motionInfo.position = SGVec3d::fromGeod(geod);
    //motionInfo.position = SGVec3d(geod.getLongitudeRad(),geod.getLatitudeRad(),geod.getElevationFt());	
    
    // The quaternion rotating from the earth centered frame to the
    // horizontal local frame
    SGQuatf qEc2Hl = SGQuatf::fromLonLatRad((float)lon, (float)lat);
    // The orientation wrt the horizontal local frame
    float heading = fdm.psi;
    float pitch = fdm.theta;
    float roll = fdm.phi;
    SGQuatf hlOr = SGQuatf::fromYawPitchRoll(heading, pitch, roll);
    // The orientation of the vehicle wrt the earth centered frame
    motionInfo.orientation = qEc2Hl*hlOr;

      motionInfo.linearVel = SGVec3f::zeros();
      motionInfo.angularVel = SGVec3f::zeros();
      motionInfo.linearAccel = SGVec3f::zeros();
      motionInfo.angularAccel = SGVec3f::zeros();
 
    // now send the properties
    PropertyMap::iterator it;
    for (it = mPropertyMap.begin(); it != mPropertyMap.end(); ++it) {
      FGPropertyData* pData = new FGPropertyData;
      //pData->id = it->first;
      pData->id = 0;
      //pData->type = it->second->getType();
      pData->type = SGPropertyNode::FLOAT;
      pData->float_value = 0.0;
      
      
      motionInfo.properties.push_back(pData);
    }

    SendMyPosition(motionInfo, callsign);
    
    // Now remove the data
    std::vector<FGPropertyData*>::const_iterator propIt;
    std::vector<FGPropertyData*>::const_iterator propItEnd;
    propIt = motionInfo.properties.begin();
    propItEnd = motionInfo.properties.end();


    while (propIt != propItEnd)
    {
      delete *propIt;
      propIt++;
    }    
  }

//////////////////////////////////////////////////////////////////////
//
//  Description: Sends the position data for the local position.
//
//////////////////////////////////////////////////////////////////////
void
MultiplayerUDPChannel::SendMyPosition(const FGExternalMotionData& motionInfo, char* callsign)
{

  T_PositionMsg PosMsg;
  
  PosMsg.time = XDR_encode_double (motionInfo.time);
  PosMsg.lag = XDR_encode_double (motionInfo.lag);
  for (unsigned i = 0 ; i < 3; ++i)
    PosMsg.position[i] = XDR_encode_double (motionInfo.position(i));
  SGVec3f angleAxis;
  motionInfo.orientation.getAngleAxis(angleAxis);
  for (unsigned i = 0 ; i < 3; ++i)
    PosMsg.orientation[i] = XDR_encode_float (angleAxis(i));
  for (unsigned i = 0 ; i < 3; ++i)
    PosMsg.linearVel[i] = XDR_encode_float (motionInfo.linearVel(i));
  for (unsigned i = 0 ; i < 3; ++i)
    PosMsg.angularVel[i] = XDR_encode_float (motionInfo.angularVel(i));
  for (unsigned i = 0 ; i < 3; ++i)
    PosMsg.linearAccel[i] = XDR_encode_float (motionInfo.linearAccel(i));
  for (unsigned i = 0 ; i < 3; ++i)
    PosMsg.angularAccel[i] = XDR_encode_float (motionInfo.angularAccel(i));

  char Msg[MAX_PACKET_SIZE];
  memcpy(Msg + sizeof(T_MsgHdr), &PosMsg, sizeof(T_PositionMsg));
  
  char* ptr = Msg + sizeof(T_MsgHdr) + sizeof(T_PositionMsg);
  std::vector<FGPropertyData*>::const_iterator it;
  it = motionInfo.properties.begin();

  while (it != motionInfo.properties.end()
         && ptr < (Msg + MAX_PACKET_SIZE - sizeof(xdr_data_t))) {
             
    // First elements is the ID
    xdr_data_t xdr = XDR_encode_uint32((*it)->id);
    memcpy(ptr, &xdr, sizeof(xdr_data_t));
    ptr += sizeof(xdr_data_t);
    
    // The actual data representation depends on the type
    switch ((*it)->type) {
      case SGPropertyNode::INT:        
      case SGPropertyNode::BOOL:        
      case SGPropertyNode::LONG:        
        xdr = XDR_encode_uint32((*it)->int_value);
        memcpy(ptr, &xdr, sizeof(xdr_data_t));
        ptr += sizeof(xdr_data_t);
        
        break;
      case SGPropertyNode::FLOAT:
      case SGPropertyNode::DOUBLE:
        xdr = XDR_encode_float((*it)->float_value);;
        memcpy(ptr, &xdr, sizeof(xdr_data_t));
        ptr += sizeof(xdr_data_t);
        
        break;
      case SGPropertyNode::STRING:
      case SGPropertyNode::UNSPECIFIED:
        {
          // String is complicated. It consists of
          // The length of the string
          // The string itself
          // Padding to the nearest 4-bytes.        
          const char* lcharptr = (*it)->string_value;
          
          if (lcharptr != 0)
          {
            // Add the length         
            ////cout << "String length: " << strlen(lcharptr) << "\n";
            uint32_t len = strlen(lcharptr);
            
            xdr = XDR_encode_uint32(len);
            memcpy(ptr, &xdr, sizeof(xdr_data_t));
            ptr += sizeof(xdr_data_t);
            
            if (len != 0)
            {

              // Now the text itself          
              int lcount = 0;
              while ((*lcharptr != '\0') && (lcount < MAX_TEXT_SIZE)) 
              {
                xdr = XDR_encode_int8(*lcharptr);
                memcpy(ptr, &xdr, sizeof(xdr_data_t));
                ptr += sizeof(xdr_data_t);
                lcharptr++;
                lcount++;          
              }

              

              // Now pad if required
              while ((lcount % 4) != 0)
              {
                xdr = XDR_encode_int8(0);
                memcpy(ptr, &xdr, sizeof(xdr_data_t));
                ptr += sizeof(xdr_data_t);
                lcount++;          
                //cout << "0";
              }
              
              //cout << "\n";
            }
          }
          else
          {
            // Nothing to encode
            xdr = XDR_encode_uint32(0);
            memcpy(ptr, &xdr, sizeof(xdr_data_t));
            ptr += sizeof(xdr_data_t);
            
          }
           
        }
        break;
        
      default:
        
        xdr = XDR_encode_float((*it)->float_value);;
        memcpy(ptr, &xdr, sizeof(xdr_data_t));
        ptr += sizeof(xdr_data_t);
        
        break;
    }
        
    ++it;
  }

  T_MsgHdr MsgHdr;
  fillMsgHdr(callsign, &MsgHdr, POS_DATA_ID, ptr - Msg);
  memcpy(Msg, &MsgHdr, sizeof(T_MsgHdr));
  
   
  mSocket->sendto(Msg, ptr - Msg, 0, &mServer);

}
//////////////////////////////////////////////////////////////////////


////////////////////////////////////////

void MultiplayerUDPChannel::fowardData(char* callsignToWatch) {
	//////////////////////////////////////////////////
	//  Read the receive socket and process any data
	//////////////////////////////////////////////////

	int ctr=0; 
	int bytes;
	do {	
		char Msg[1200];
		//////////////////////////////////////////////////
		//  Although the recv call asks for 
		//  MAX_PACKET_SIZE of data, the number of bytes
		//  returned will only be that of the next
		//  packet waiting to be processed.
		//////////////////////////////////////////////////
		struct sockaddr_in tmp;
		unsigned int len = sizeof(tmp);
		bytes = recvfrom(sock, Msg, sizeof(Msg), 0, (struct sockaddr *) &tmp, &len);
		
		
		//////////////////////////////////////////////////
		//  no Data received
		//////////////////////////////////////////////////	      
		if (bytes <= 0) {
			if (errno != EAGAIN && errno != 0) // MSVC output "NoError" otherwise
				printErrorMessage((char*)"FGMultiplayMgr::MP_ProcessData");
			//break;
		}
		if (bytes <= sizeof(T_MsgHdr)) {
			printErrorMessage((char*)"FGMultiplayMgr::MP_ProcessData - received message with insufficient data");
			//break;
		}
		//////////////////////////////////////////////////
		//  Read header
		//////////////////////////////////////////////////
		T_MsgHdr* MsgHdr = (T_MsgHdr *)Msg;
		MsgHdr->Magic       = XDR_decode_uint32 (MsgHdr->Magic);
		MsgHdr->Version     = XDR_decode_uint32 (MsgHdr->Version);
		MsgHdr->MsgId       = XDR_decode_uint32 (MsgHdr->MsgId);
		MsgHdr->MsgLen      = XDR_decode_uint32 (MsgHdr->MsgLen);
		MsgHdr->ReplyPort   = XDR_decode_uint32 (MsgHdr->ReplyPort);
	
		if (MsgHdr->Magic != MSG_MAGIC) {
			printErrorMessage((char*)"FGMultiplayMgr::MP_ProcessData - message has invalid magic number!");
		}
		if (MsgHdr->Version != PROTO_VER) {
			printErrorMessage((char*)"FGMultiplayMgr::MP_ProcessData - message has invalid protocoll number!");
		}
		if (MsgHdr->MsgLen != bytes) {
			printErrorMessage((char*)"FGMultiplayMgr::MP_ProcessData - message has invalid length!");
		}
		//////////////////////////////////////////////////
		//  Process messages
		//////////////////////////////////////////////////
		switch (MsgHdr->MsgId) {
		case POS_DATA_ID:
		        if (!strcmp(MsgHdr->Callsign, callsignToWatch)) {
				if(ctr%100==0){
					processPositionMessage(Msg,true);//Writes to file
				}
				else{
					processPositionMessage(Msg,false);
				}
			}
			break;
		case UNUSABLE_POS_DATA_ID:
		case OLD_OLD_POS_DATA_ID:
		case OLD_PROP_MSG_ID:
		case OLD_POS_DATA_ID:
			break;
		default:
                	  cout << "FGMultiplayMgr::MP_ProcessData - Unknown message Id received: " 
				 << MsgHdr->MsgId << "\n";
			break;
		}
		ctr++; 
	} while (bytes > 0);
}

void MultiplayerUDPChannel::processPositionMessage(const char *Msg) {
	T_MsgHdr* header = (T_MsgHdr *)Msg;
	T_PositionMsg* message = (T_PositionMsg *)(Msg + sizeof(T_MsgHdr));
	
	FGExternalMotionData motionInfo;
	
	motionInfo.time = XDR_decode_double(message->time);
	motionInfo.lag = XDR_decode_double(message->lag);
	for (unsigned i = 0; i < 3; ++i)
		motionInfo.position(i) = XDR_decode_double(message->position[i]);
		
	SGVec3f angleAxis;
	for (unsigned i = 0; i < 3; ++i)
		angleAxis(i) = XDR_decode_float(message->orientation[i]);
	motionInfo.orientation = SGQuatf::fromAngleAxis(angleAxis);
	for (unsigned i = 0; i < 3; ++i)
		motionInfo.linearVel(i) = XDR_decode_float(message->linearVel[i]);
	for (unsigned i = 0; i < 3; ++i)
		motionInfo.angularVel(i) = XDR_decode_float(message->angularVel[i]);
	for (unsigned i = 0; i < 3; ++i)
		motionInfo.linearAccel(i) = XDR_decode_float(message->linearAccel[i]);
	for (unsigned i = 0; i < 3; ++i)
		motionInfo.angularAccel(i) = XDR_decode_float(message->angularAccel[i]);
	
	
	SGGeod pos = SGGeod::fromCart(motionInfo.position);
	
	
	
	SGQuatf qEc2Hl = SGQuatf::fromLonLatRad((float)pos.getLongitudeRad(), (float)pos.getLatitudeRad());
	
	SGQuatf hlOr = conj(qEc2Hl)*motionInfo.orientation;
	float hDeg, pDeg, rDeg;
	hlOr.getEulerRad(hDeg, pDeg, rDeg);
	
	
	
}

void MultiplayerUDPChannel::SendTextMessage(const string &MsgText) {
	multiplayerSocket = new netSocket();
	if (!multiplayerSocket->open(false)) {
		cout << "SendTextMessage: Couldn't create socket to send chat message!";
	}
	multiplayerSocket->setBlocking(false);
	multiplayerSocket->setBroadcast(true);
	
	multiplayerServer.set("mpserver02.flightgear.org", 5000);
	
	T_MsgHdr MsgHdr;
	fillMsgHdr(&MsgHdr, CHAT_MSG_ID);
	//////////////////////////////////////////////////
	// Divide the text string into blocks that fit
	// in the message and send the blocks.
	//////////////////////////////////////////////////
	unsigned iNextBlockPosition = 0;
	T_ChatMsg ChatMsg;

	char Msg[sizeof(T_MsgHdr) + sizeof(T_ChatMsg)];
	while (iNextBlockPosition < MsgText.length()) {
		strncpy(ChatMsg.Text, MsgText.substr(iNextBlockPosition, MAX_CHAT_MSG_LEN - 1).c_str(), 
		MAX_CHAT_MSG_LEN);
		ChatMsg.Text[MAX_CHAT_MSG_LEN - 1] = '\0';
		memcpy(Msg, &MsgHdr, sizeof(T_MsgHdr));
		memcpy(Msg + sizeof(T_MsgHdr), &ChatMsg, sizeof(T_ChatMsg));
		multiplayerSocket->sendto(Msg, sizeof(T_MsgHdr) + sizeof(T_ChatMsg), 0, &multiplayerServer);
		iNextBlockPosition += MAX_CHAT_MSG_LEN - 1;
	}
	
	cout << "Sent chat message\n";
}

void MultiplayerUDPChannel::fillMsgHdr(T_MsgHdr *MsgHdr, int MsgId, unsigned _len) {
	uint32_t len;
	switch (MsgId) {
	case CHAT_MSG_ID:
		len = sizeof(T_MsgHdr) + sizeof(T_ChatMsg);
		break;
	case POS_DATA_ID:
		len = _len;
		break;
	default:
		len = sizeof(T_MsgHdr);
		break;
	}
	MsgHdr->Magic = XDR_encode_uint32(MSG_MAGIC);
	MsgHdr->Version = XDR_encode_uint32(PROTO_VER);
	MsgHdr->MsgId = XDR_encode_uint32(MsgId);
	MsgHdr->MsgLen = XDR_encode_uint32(len);
	MsgHdr->ReplyAddress = 0; // Are obsolete, keep them for the server for
	MsgHdr->ReplyPort = 0; // now
	
	strncpy(MsgHdr->Callsign, "uga4", MAX_CALLSIGN_LEN);
	MsgHdr->Callsign[MAX_CALLSIGN_LEN - 1] = '\0';
}

void MultiplayerUDPChannel::fillMsgHdr(char* callsign, T_MsgHdr *MsgHdr, int MsgId, unsigned _len) {
	uint32_t len;
	switch (MsgId) {
	case CHAT_MSG_ID:
		len = sizeof(T_MsgHdr) + sizeof(T_ChatMsg);
		break;
	case POS_DATA_ID:
		len = _len;
		break;
	default:
		len = sizeof(T_MsgHdr);
		break;
	}
	MsgHdr->Magic = XDR_encode_uint32(MSG_MAGIC);
	MsgHdr->Version = XDR_encode_uint32(PROTO_VER);
	MsgHdr->MsgId = XDR_encode_uint32(MsgId);
	MsgHdr->MsgLen = XDR_encode_uint32(len);
	MsgHdr->ReplyAddress = 0; // Are obsolete, keep them for the server for
	MsgHdr->ReplyPort = 0; // now
	//cout<<"FillMsgHdr "<<callsign<<endl;
	strncpy(MsgHdr->Callsign, callsign, MAX_CALLSIGN_LEN);
	MsgHdr->Callsign[MAX_CALLSIGN_LEN - 1] = '\0';
}

void MultiplayerUDPChannel::printErrorMessage(char *msg) {
	cerr << "MultiplayerUDPChannel: " << msg << endl;
	//exit(1);
}

void MultiplayerUDPChannel::closeSocket() {
	close(sock);
}

MultiplayerUDPChannel::~MultiplayerUDPChannel() {
	close(sock);
}


///////////////////////////////////////////
void MultiplayerUDPChannel::processPositionMessage(const char *Msg, bool writeToFile) {
	T_MsgHdr* header = (T_MsgHdr *)Msg;
	T_PositionMsg* message = (T_PositionMsg *)(Msg + sizeof(T_MsgHdr));
	
	FGExternalMotionData motionInfo;
	
	motionInfo.time = XDR_decode_double(message->time);
	motionInfo.lag = XDR_decode_double(message->lag);
	for (unsigned i = 0; i < 3; ++i)
		motionInfo.position(i) = XDR_decode_double(message->position[i]);
		
	SGVec3f angleAxis;
	for (unsigned i = 0; i < 3; ++i)
		angleAxis(i) = XDR_decode_float(message->orientation[i]);
	motionInfo.orientation = SGQuatf::fromAngleAxis(angleAxis);
	for (unsigned i = 0; i < 3; ++i)
		motionInfo.linearVel(i) = XDR_decode_float(message->linearVel[i]);
	for (unsigned i = 0; i < 3; ++i)
		motionInfo.angularVel(i) = XDR_decode_float(message->angularVel[i]);
	for (unsigned i = 0; i < 3; ++i)
		motionInfo.linearAccel(i) = XDR_decode_float(message->linearAccel[i]);
	for (unsigned i = 0; i < 3; ++i)
		motionInfo.angularAccel(i) = XDR_decode_float(message->angularAccel[i]);
	
	
	
	SGGeod pos = SGGeod::fromCart(motionInfo.position);
	
		
	

	if(writeToFile){
		string filestr="AgentPosition/FinalLocation";
		filestr.append(header->Callsign);
		
		filestr.append(".txt");
		finalLocationFile.open(filestr.c_str(),ios::out); 
		finalLocationFile<<pos.getLatitudeRad()<<" "<<pos.getLongitudeRad();
		finalLocationFile.close();
	}
	///////////////////

	SGQuatf qEc2Hl = SGQuatf::fromLonLatRad((float)pos.getLongitudeRad(), (float)pos.getLatitudeRad());
	
	SGQuatf hlOr = conj(qEc2Hl)*motionInfo.orientation;
	float hDeg, pDeg, rDeg;
	hlOr.getEulerRad(hDeg, pDeg, rDeg);
}


////////////////////////////////////////////
