################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../SGGeodesy.o \
../main.o 

CXX_SRCS += \
../GridLocation.cxx \
../IpomdpPolicy.cxx \
../airvehicle.cxx \
../fdm_udp_channel.cxx \
../main.cxx \
../multiplayer_udp_channel.cxx \
../netSocket.cxx \
../tiny_xdr.cxx \
../unmannedC172.cxx 

OBJS += \
./GridLocation.o \
./IpomdpPolicy.o \
./airvehicle.o \
./fdm_udp_channel.o \
./main.o \
./multiplayer_udp_channel.o \
./netSocket.o \
./tiny_xdr.o \
./unmannedC172.o 

CXX_DEPS += \
./GridLocation.d \
./IpomdpPolicy.d \
./airvehicle.d \
./fdm_udp_channel.d \
./main.d \
./multiplayer_udp_channel.d \
./netSocket.d \
./tiny_xdr.d \
./unmannedC172.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cxx
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


