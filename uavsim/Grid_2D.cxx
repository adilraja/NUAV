/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include "include/Ipomdp/Grid_2D.hxx"

Grid2D :: Grid2D(int NoRows, int NoCols, double InitLat=0.655, double InitLon=-2.137, double gridSize =0.0015, double hvr_leeway=0.0002, double act_time = 300){
	no_of_rows=NoRows;
	no_of_cols=NoCols;
	init_lat=InitLat;
	init_lon=InitLon;
	grid_size=gridSize;
	hover_leeway=hvr_leeway;
	action_time=act_time;

	for(int i=0; i<NoRows; i++){
		double lat= InitLat+gridSize/2+i*gridSize;
		center_lats.push_back(lat);
	}
	for(int i=0; i<NoCols; i++){
		double lon= InitLon+gridSize/2+i*gridSize;
		center_lons.push_back(lon);
	}
}

int* Grid2D :: getGridSize(){
	int* siz = new int[2];
	siz[0]=no_of_rows;
	siz[1]=no_of_cols;
	return siz;

}

int* Grid2D :: getCurrentGridLocation(){	//Returns from history
	int* loc = new int[2];
	loc[0]=current_row;
	loc[1]=current_col;
	return loc;
}


double* Grid2D :: getCenterCoordinates(int row, int col){
	double* center = new double[2];
	center[0] = center_lats[row];
	center[1] = center_lons[col];
	return center;
}

int* Grid2D :: getGridLocation(double* coord){
	int* gridPos = new int[2];
	gridPos[0]=(int)floor(((coord[0]-init_lat)/grid_size));
	if(gridPos[0]>no_of_rows)
		gridPos[0]=-1;
	gridPos[1]=(int)floor(((coord[1]-init_lon)/grid_size));
	if(gridPos[1]>no_of_rows)
		gridPos[1]=-1;
	return gridPos;
}


void Grid2D :: moveNorth(int argc, char *argv[], UC172Control* c172ptr, FDMUDPChannel *netptr, FGNetCtrls *ctrls, FGNetFDM *fdm, MultiplayerUDPChannel *mnetptr, char* callsign){
	
	fdm = netptr->getdata();
	double cur_lat = fdm->latitude;
	double cur_long = fdm->longitude;
	
	if((current_row < no_of_rows-1) && (current_row > -1))
		current_row++;

	double* tar_coord = getCenterCoordinates(current_row, current_col);
	
	time_t start,end;
	time(&start);
	c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], callsign);
	
	tar_coord[0] -= hover_leeway;	//For a good reason//
	tar_coord[1] -= hover_leeway;

	time(&end);
	while(difftime(end,start)<action_time){
		fdm = netptr->getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		tar_coord[0] += (2*hover_leeway);
		tar_coord[1] += (2*hover_leeway);
		
		c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], start, action_time, callsign);

		fdm = netptr->getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		time(&end);
		if(difftime(end,start)>=action_time)
			break;

		tar_coord[0] -= (2*hover_leeway);
		tar_coord[1] -= (2*hover_leeway);
		
		c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], start, action_time, callsign);
		
		netptr->senddata(*ctrls);
		time(&end);
	}
}

void Grid2D :: moveSouth(int argc, char *argv[], UC172Control* c172ptr, FDMUDPChannel *netptr, FGNetCtrls *ctrls, FGNetFDM *fdm, MultiplayerUDPChannel *mnetptr, char* callsign){
	
	fdm = netptr->getdata();
	double cur_lat = fdm->latitude;
	double cur_long = fdm->longitude;
	
	if(current_row < no_of_rows && current_row > 0)
		current_row--;

	double* tar_coord = getCenterCoordinates(current_row, current_col);
	
	time_t start,end;
	time(&start);
	c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], callsign);	
	tar_coord[0] -= hover_leeway;
	tar_coord[1] -= hover_leeway;
	time(&end);
	while(difftime(end,start)<action_time){
		fdm = netptr->getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		tar_coord[0] += (2*hover_leeway);
		tar_coord[1] += (2*hover_leeway);
		
		c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], start, action_time, callsign);

		fdm = netptr->getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		time(&end);
		if(difftime(end,start)>=action_time)
			break;

		tar_coord[0] -= (2*hover_leeway);
		tar_coord[1] -= (2*hover_leeway);
		
		c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], start, action_time, callsign);
		
		netptr->senddata(*ctrls);
		time(&end);
	}
	}

void Grid2D :: moveEast(int argc, char *argv[], UC172Control* c172ptr, FDMUDPChannel *netptr, FGNetCtrls *ctrls, FGNetFDM *fdm, MultiplayerUDPChannel *mnetptr, char* callsign){
	fdm = netptr->getdata();
	double cur_lat = fdm->latitude;
	double cur_long = fdm->longitude;
	

	if(current_col < no_of_cols-1 && current_col>-1)
		current_col++;

	double* tar_coord = getCenterCoordinates(current_row, current_col);
	
	time_t start,end;
	time(&start);
	c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], callsign);	
	tar_coord[0] -= hover_leeway;
	tar_coord[1] -= hover_leeway;
	time(&end);
	while(difftime(end,start)<action_time){
		fdm = netptr->getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		tar_coord[0] += (2*hover_leeway);
		tar_coord[1] += (2*hover_leeway);
		
		c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], start, action_time, callsign);

		fdm = netptr->getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		time(&end);
		if(difftime(end,start)>=action_time)
			break;

		tar_coord[0] -= (2*hover_leeway);
		tar_coord[1] -= (2*hover_leeway);
		
		c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], start, action_time, callsign);
		
		netptr->senddata(*ctrls);
		time(&end);
	}
	
}

void Grid2D :: moveWest(int argc, char *argv[], UC172Control* c172ptr, FDMUDPChannel *netptr, FGNetCtrls *ctrls, FGNetFDM *fdm, MultiplayerUDPChannel *mnetptr, char* callsign){
	fdm = netptr->getdata();
	double cur_lat = fdm->latitude;
	double cur_long = fdm->longitude;
	
	//cout<<"/////////////////Moving West/////////////////";

	if((current_col<no_of_cols) && (current_col>0))
		current_col--;

	
	double* tar_coord = getCenterCoordinates(current_row, current_col);
	
	time_t start,end;
	time(&start);
	c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], callsign);	
	tar_coord[0] -= hover_leeway;
	tar_coord[1] -= hover_leeway;
	time(&end);
	while(difftime(end,start)<action_time){
		fdm = netptr->getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		tar_coord[0] += (2*hover_leeway);
		tar_coord[1] += (2*hover_leeway);
		
		c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], start, action_time, callsign);

		fdm = netptr->getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		time(&end);
		if(difftime(end,start)>=action_time)
			break;

		tar_coord[0] -= (2*hover_leeway);
		tar_coord[1] -= (2*hover_leeway);
		
		c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], start, action_time, callsign);
		
		netptr->senddata(*ctrls);
		time(&end);
	}
	
}

void Grid2D :: hover(int argc, char *argv[], UC172Control* c172ptr, FDMUDPChannel *netptr, FGNetCtrls *ctrls, FGNetFDM * fdm, MultiplayerUDPChannel *mnetptr, char* callsign){////////////change to accomodate parameter netptr///////
	
	fdm = netptr->getdata();
	double cur_lat;
	double cur_long;
	double* tar_coord = getCenterCoordinates(current_row, current_col);
	
	tar_coord[0] -= hover_leeway;
	tar_coord[1] -= hover_leeway;

	time_t start,end;
	time(&start);
	do{
		fdm = netptr->getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		tar_coord[0] += (2*hover_leeway);
		tar_coord[1] += (2*hover_leeway);
		
		c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], start, action_time, callsign);

		fdm = netptr->getdata();
		cur_lat = fdm->latitude;
		cur_long = fdm->longitude;
		
		time(&end);
		if(difftime(end,start)>=action_time)
			break;

		tar_coord[0] -= (2*hover_leeway);
		tar_coord[1] -= (2*hover_leeway);
		
		c172ptr->flyFromXToY(argc, argv, netptr, ctrls, mnetptr, cur_lat, cur_long, tar_coord[0], tar_coord[1], start, action_time, callsign);
		
		netptr->senddata(*ctrls);
		time(&end);
	}while(difftime(end,start)<action_time);
	
}

