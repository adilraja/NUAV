/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"Copyright (C) 2010 THINC Lab @ The University of Georgia"

This file is part of GaTAC.

GaTAC is free software: you can redistribute it and/or modify

it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GaTAC is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License

along with GaTAC. If not, see <http://www.gnu.org/licenses/>.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include "include/Ipomdp/Policy.hxx"

IpomdpPolicy :: IpomdpPolicy() {}

IpomdpPolicy :: IpomdpPolicy(string filename) {
    char line[2000];
    fstream file(filename.c_str(), ios::in);
    
    
    // read in the number of horizons
    file.getline(line,2000);
    sscanf(line, "%*s %d", &horizonCount);
    
    // read in the number of observations
    file.getline(line,2000);
    sscanf(line, "%*s %d", &observationCount);
    
    numPolicy=0;
    while(file.getline(line,2000)!=NULL){  
      // set our total policy horizons (the root policy)
      Policy tPolicy;
      tPolicy.horizon = horizonCount;
    
      
      sscanf(line, "%*s %*d %*s %*s %*s %d,", &tPolicy.currentAction);

      // for each observation, read in the subsequent policies
      for (int obervationIndex = 0; obervationIndex < observationCount; obervationIndex++) {
    	  Policy newPolicy = readyPolicy(file, (horizonCount - 1));
    	  tPolicy.nextPolicies.push_back(newPolicy);		
      }

      totalPolicy.push_back(tPolicy);
 
      // set our current policy to the root policy node (totalPolicy of same index) //changes By Ekhlas.
      currentPolicy.push_back(totalPolicy[numPolicy]);
      numPolicy++;
    }
    file.close();
    
}

Policy IpomdpPolicy :: readyPolicy(fstream &file, int horizonIndex) {
	Policy newPolicy;
	newPolicy.horizon = horizonIndex;
	
	char line[2000];
	file.getline(line,2000);
	sscanf(line, " %*s %*s %d %*s %*s %d", &newPolicy.currentObservation, &newPolicy.currentAction);
	
	if (horizonIndex == 1) {
		return newPolicy;
	} else {
		for (int obervationIndex = 0; obervationIndex < observationCount; obervationIndex++) {
			newPolicy.nextPolicies.push_back(readyPolicy(file, horizonIndex - 1));
		}
	}
	
	return newPolicy;
}

void IpomdpPolicy :: next(int observation,int polNum) {
	for (int i = 0; i < currentPolicy[polNum].nextPolicies.size(); i++) {
		if (currentPolicy[polNum].nextPolicies[i].currentObservation == observation) {
			currentPolicy[polNum] = currentPolicy[polNum].nextPolicies[i];
			break;
		}
	}
}

int IpomdpPolicy :: getCurrentAction(int polNum) {
	return currentPolicy[polNum].currentAction;
}

IpomdpPolicy :: ~IpomdpPolicy() {}

void IpomdpPolicy :: printPolicy() {
    for(int i=0; i < numPolicy; i++)
	printIndividualPolicy(totalPolicy[i]);
}

void IpomdpPolicy :: printIndividualPolicy(Policy policy) {
        for (int i = horizonCount - policy.horizon; i > 0; i--) { 
	  cout << "\t";
	}		
	if (policy.horizon == horizonCount) 
	  cout << "action: " << policy.currentAction << endl;
	else{
	  cout << "observation: " << policy.currentObservation 
	       << " -> " << "action: " << policy.currentAction << endl;
	}
	for (int i = 0; i < policy.nextPolicies.size(); i++) {
	  printIndividualPolicy(policy.nextPolicies[i]);
	}       
}

int IpomdpPolicy :: getHorizonCount(){
	return horizonCount;
}
